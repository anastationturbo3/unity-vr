# Unity VRTTRPG

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:8dd4a4c717e559b4a3c15474fad7a8ab?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:8dd4a4c717e559b4a3c15474fad7a8ab?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:8dd4a4c717e559b4a3c15474fad7a8ab?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/anastationturbo3/unity-vr.git
git branch -M main
git push -uf origin main
```

## The Gist
This is awork-in-progress thesis project designed in Unity that simulates a TTRPG environment in VR. Its main goal is testing immersion using TTRPGs as a base, comparing different applications (and real life experience) to a virtual environment (suitable for play at any distance)
The application is designed in [Unity](https://unity.com/) and utilizes its [XR library](https://docs.unity3d.com/Manual/XR.html) for VR development, as well as [Photon Pun](https://www.photonengine.com/pun) for online capabilities.