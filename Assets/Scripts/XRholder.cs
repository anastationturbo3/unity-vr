using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XRholder : MonoBehaviour {
    public static XRholder instance = null;
    [HideInInspector] public GameObject vrRig;

    void Awake(){
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        vrRig = this.gameObject;
    }

    public GameObject vrHead;
    public GameObject vrLeftArm;
    public GameObject vrRightArm;
}
