using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class SnapSet : MonoBehaviour {
    private XRSocketInteractor Holder;

    void Awake(){
        Holder = GetComponent<XRSocketInteractor>();
    }

    void Update(){
        if(Holder.selectTarget != null){
            if(Holder.selectTarget.transform.Find("AttachSnap") != null)
                Holder.selectTarget.GetComponent<XRGrabNetworkInteractable>().attachTransform = Holder.selectTarget.transform.Find("AttachSnap");
        }
    }
}
