using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class ObjectList : MonoBehaviour {
    private PhotonView photonView;
    public List<GameObject> Objects;
    public List<MaterialMap> MaterialMappingTool;

    void Awake(){
        photonView = GetComponent<PhotonView>();
    }

    public void MaterialChange(string color){
        if(!TransitionManager.instance.InTheNetwork) MaterialChangeRPC(color);
        else photonView.RPC("MaterialChangeRPC", RpcTarget.All, color);
    }

    [PunRPC]
    public void MaterialChangeRPC(string color){
        for(int i=0; i<MaterialMappingTool.Count; i++){
            if(MaterialMappingTool[i].color == color){
                Material mat = MaterialMappingTool[i].mat;
                foreach(GameObject obj in Objects) obj.GetComponent<MeshRenderer>().material = mat;
            }
        }
    }
}
