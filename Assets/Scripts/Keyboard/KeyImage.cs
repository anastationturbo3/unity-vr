using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyImage : MonoBehaviour {
    public string ButtonType;
    public GameObject ImageInactive;
    public GameObject ImageActive;

    void FixedUpdate(){
        switch(ButtonType){
            case "Caps":
                if(KeyboardManager.instance.CapsOn == false){ ImageInactive.SetActive(true); ImageActive.SetActive(false);}
                else{ ImageInactive.SetActive(false); ImageActive.SetActive(true);}
                break;

            case "Shift":
                if(KeyboardManager.instance.ShiftOn == false){ ImageInactive.SetActive(true); ImageActive.SetActive(false);}
                else{ ImageInactive.SetActive(false); ImageActive.SetActive(true);}
                break;
        }   
    }
}
