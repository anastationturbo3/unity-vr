using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Key : MonoBehaviour {
    public char key;
    public char altKey;
    public bool capsChange;
    public TMPro.TextMeshProUGUI letter;

    public void Write(){
        if((capsChange == true && KeyboardManager.instance.CapsOn == false && KeyboardManager.instance.ShiftOn == false) ||
           (capsChange == true && KeyboardManager.instance.CapsOn == true && KeyboardManager.instance.ShiftOn == true) ||
           (capsChange == false && KeyboardManager.instance.ShiftOn == false))
            NameWriter.instance.WriteChar(key);
        else NameWriter.instance.WriteChar(altKey);

        if(KeyboardManager.instance.ShiftOn) KeyboardManager.instance.DisableShift();
    }

    void FixedUpdate(){
        if(letter != null){
            if((capsChange == true && KeyboardManager.instance.CapsOn == false && KeyboardManager.instance.ShiftOn == false) ||
            (capsChange == true && KeyboardManager.instance.CapsOn == true && KeyboardManager.instance.ShiftOn == true) ||
            (capsChange == false && KeyboardManager.instance.ShiftOn == false))
                letter.SetText(key.ToString());  
            else letter.SetText(altKey.ToString());
        }
    }
}
