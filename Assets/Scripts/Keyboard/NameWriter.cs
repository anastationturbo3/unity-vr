using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NameWriter : MonoBehaviour {
    public static NameWriter instance = null;
    public string Name;
    public TMPro.TextMeshProUGUI nameHolder;
    public GameObject NameBackground;
    public int maxSize;
    
    private int cursorPos;
    private bool Writing;

    void Awake(){
        DontDestroyOnLoad(this.gameObject);
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        Writing = false;
    }

    public void UpdateNameText(){
        nameHolder.SetText(Name);
    }

    public void WriteChar(char c){
        if(Writing && Name.Length < maxSize){
            Name += c;
            UpdateNameText();
        }
    }

    public void Backspace(){
        if(Writing && Name.Length > 0){
            Name = Name.Remove(Name.Length-1);
            UpdateNameText();
        }
    }

    public void ToggleWriting(){
        Writing = !Writing;
        if(!Writing) TransitionManager.instance.UpdateName(Name);
    }

    public void SetWriting(bool t){
        Writing = t;
        if(!Writing) TransitionManager.instance.UpdateName(Name);
    }

    void FixedUpdate(){
        if(Writing){
            NameBackground.SetActive(true);
            nameHolder.color = new Color(0f,0f,0f,1f);
        }
        else {
            NameBackground.SetActive(false);
            nameHolder.color = new Color(1f,1f,1f,1f);
        }
    }
}
