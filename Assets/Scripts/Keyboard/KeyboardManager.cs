using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardManager : MonoBehaviour {
    public static KeyboardManager instance = null;

    public bool CapsOn;
    public bool ShiftOn;

    void Awake(){
        DontDestroyOnLoad(this.gameObject);
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    public void TurnCaps(){
        CapsOn = !CapsOn;
    }

    public void TurnShift(){
        ShiftOn = !ShiftOn;
    }

    public void DisableShift(){
        ShiftOn = false;
    }

    public void Backspace(){
        NameWriter.instance.Backspace();
    }
}
