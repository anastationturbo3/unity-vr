using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class GrabSetLeft : MonoBehaviour {
    private XRRayInteractor Holder;

    void Awake(){
        Holder = GetComponent<XRRayInteractor>();
    }

    void Update(){
        if(Holder.selectTarget != null){
            if(Holder.selectTarget.transform.Find("AttachLeft") != null)
                Holder.selectTarget.GetComponent<XRGrabNetworkInteractable>().attachTransform = Holder.selectTarget.transform.Find("AttachLeft");
        }
    }
}
