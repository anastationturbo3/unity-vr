using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using Photon.Pun;

public class XRSocketNetworkInteractor : XRSocketInteractor {
    /*
    protected override void OnSelectEntered(SelectEnterEventArgs args){
        PhotonView objectPhotonView = args.interactorObject.transform.GetComponent<PhotonView>();
        if(objectPhotonView != null){
            if(!objectPhotonView.IsMine)
                interactionManager.SelectExit(this, this.selectTarget);
        }
    }
    */

    public override bool CanSelect(IXRSelectInteractable interactable){
        PhotonView objectPhotonView = interactable.transform.GetComponent<PhotonView>();
        if(objectPhotonView != null){
            if(objectPhotonView.IsMine)
                return base.CanSelect(interactable);
        }
        return false;
    }
}
