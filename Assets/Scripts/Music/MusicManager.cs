using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;

public class MusicManager : MonoBehaviour{
    public static MusicManager instance = null;
    public AudioSource music_audioSrc;

    [HideInInspector] public float CurrentVolume;
    [HideInInspector] public float SetVolume {
        get {
            return CurrentVolume;
        }
        set {
            if(value == CurrentVolume) return;
            CurrentVolume = value;
            music_audioSrc.volume = CurrentVolume;
        } 
    }

    void Awake(){
        DontDestroyOnLoad(this.gameObject);
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        music_audioSrc.loop = true;
        CurrentVolume = PlayerPrefs.GetFloat("MusicVolume");
        music_audioSrc.volume = CurrentVolume;
    }

    public void ChangeAudioSource(AudioSource new_audioSrc){
        music_audioSrc = new_audioSrc;
        music_audioSrc.volume = CurrentVolume;
    }

    public void PlayMusic(AudioClip music){
        music_audioSrc.volume = CurrentVolume;
        if(music_audioSrc.isPlaying && music_audioSrc.clip == music) return;
        if(music_audioSrc.isPlaying) music_audioSrc.Stop();
        music_audioSrc.clip = music;
        music_audioSrc.Play();
    }

    public void StopMusic(){
        music_audioSrc.Stop();
    }

    public void PauseUnpauseMusic(){
        if(music_audioSrc.isPlaying) music_audioSrc.Pause();
        else music_audioSrc.UnPause();
    }
}