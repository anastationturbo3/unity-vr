using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class MusicSynchronizer : MonoBehaviour {
    private PhotonView photonView;
    [HideInInspector] public int current_selected;
    public MusicPlayer musicPlayer;

    void Awake(){
        photonView = GetComponent<PhotonView>();
    }

    public void PlayMusic(int i) {
        photonView.RPC("PlayMusicRPC", RpcTarget.All, i);
    }

    [PunRPC]
    public void PlayMusicRPC(int i) {
        current_selected = i;
        if(MusicManager.instance != null){
            musicPlayer.SetSong(i);
            MusicManager.instance.PlayMusic(musicPlayer.GetSelectedSong());
        }
    }

    public void AskForCurrentMusic(){
        photonView.RPC("AskForCurrentMusicRPC", RpcTarget.All);
    }

    [PunRPC]
    public void AskForCurrentMusicRPC() {
        if(TransitionManager.instance.isGM) PlayMusic(current_selected);
    }
}
