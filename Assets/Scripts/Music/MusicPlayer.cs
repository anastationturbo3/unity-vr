using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour{
    public bool initial_music;
    public MusicSynchronizer synchronizer;

    public List<AudioClip> Songs;
    public int current_selected;

    void Awake(){
        if(initial_music) StartCoroutine(StartMusic());
    }

    IEnumerator StartMusic(){
        yield return new WaitUntil(() => MusicManager.instance != null);
        PlayCurrentSong();
    }

    public AudioClip GetSelectedSong(){
        return Songs[current_selected];
    }

    public void SetSong(int i){
        current_selected = i;
    }

    public void NextSong(){
        current_selected = (current_selected + 1) % Songs.Count;
    }

    public void PreviousSong(){
        current_selected -= 1;
        if(current_selected < 0) current_selected = Songs.Count - 1;
    }

    public void PlayCurrentSong(){
        MusicManager.instance.music_audioSrc = this.GetComponent<AudioSource>();
        if(synchronizer == null) MusicManager.instance.PlayMusic(GetSelectedSong());
        else{
            if(TransitionManager.instance.isGM){
                synchronizer.PlayMusic(current_selected);
            }
            else{
                synchronizer.AskForCurrentMusic();
            }
        }
    }

    public void PlayThisSong(int i){
        SetSong(i);
        PlayCurrentSong();
    }

    public void PlayNextSong(){
        NextSong();
        PlayCurrentSong();
    }

    public void PlayPreviousSong(){
        PreviousSong();
        PlayCurrentSong();
    }
}
