using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.XR.Interaction.Toolkit;

public class PlayerManager : MonoBehaviour {
    private PhotonView photonView;

    public GameObject AvatarHead;
    public GameObject AvatarTorso;
    public GameObject AvatarPants;
    public GameObject AvatarHandRight;
    public GameObject AvatarHandLeft;

    public Nameplate Nameplate;

    [Header("Rig Bones")]
    public Transform BoneHead;
    public Transform BoneTorso;
    public Transform BoneArmRight;
    public Transform BoneForearmRight;
    public Transform BoneArmLeft;
    public Transform BoneForearmLeft;
    public Transform BoneWaist;
    [HideInInspector] public Dictionary<string,Transform> AvatarBones;

    void Awake(){
        photonView = GetComponent<PhotonView>();
    
        AvatarBones = new Dictionary<string, Transform>();
        AvatarBones.Add("Head", BoneHead);
        AvatarBones.Add("Torso", BoneTorso);
        AvatarBones.Add("Arm Right", BoneArmRight);
        AvatarBones.Add("Forearm Right", BoneForearmRight);
        AvatarBones.Add("Arm Left", BoneArmLeft);
        AvatarBones.Add("Forearm Left", BoneForearmLeft);
        AvatarBones.Add("Waist", BoneWaist);

        AvatarRPC();
    }

    public void AvatarRPC() {
        if(photonView.IsMine){
            int[] AvatarComponents = {TransitionManager.instance.ComponentChoices["Head"],
                                      TransitionManager.instance.ComponentChoices["Torso"],
                                      TransitionManager.instance.ComponentChoices["Pants"]};

            int[] AvatarMaterials = {TransitionManager.instance.MaterialChoices["Skin"],
                                     TransitionManager.instance.MaterialChoices["Hair 1"],
                                     TransitionManager.instance.MaterialChoices["Hair 2"],
                                     TransitionManager.instance.MaterialChoices["Shirt"],
                                     TransitionManager.instance.MaterialChoices["Pants"]};

            int[] ClothingComponents = {TransitionManager.instance.ClothingChoices["Hat"],
                                        TransitionManager.instance.ClothingChoices["Armor"],
                                        TransitionManager.instance.ClothingChoices["Waist"]};

            int[] ClothingMaterials = {TransitionManager.instance.MaterialChoices["Hat"],
                                       TransitionManager.instance.MaterialChoices["Armor"],
                                       TransitionManager.instance.MaterialChoices["Waist"]};

            photonView.RPC("InitiateAvatar", RpcTarget.All, AvatarComponents, AvatarMaterials, ClothingComponents, ClothingMaterials);
            photonView.RPC("SetNameplate", RpcTarget.All, TransitionManager.instance.CharacterName);
        }
    }

    [PunRPC]
    public void InitiateAvatar(int[] AvatarOptions, int[] MaterialOptions, int[] ClothingComponentOptions, int[] ClothingMaterialOptions){
        Dictionary<string,int> Chosen_AvatarMaterialOptions = new Dictionary<string, int>();
        Chosen_AvatarMaterialOptions.Add("Skin", MaterialOptions[0]);
        Chosen_AvatarMaterialOptions.Add("Hair 1", MaterialOptions[1]);
        Chosen_AvatarMaterialOptions.Add("Hair 2", MaterialOptions[2]);
        Chosen_AvatarMaterialOptions.Add("Shirt", MaterialOptions[3]);
        Chosen_AvatarMaterialOptions.Add("Pants", MaterialOptions[4]);

        AvatarHead.GetComponent<SkinnedMeshRenderer>().sharedMesh = AvatarChoices.instance.AvatarComponentOptions["Head"][AvatarOptions[0]].Mesh;
        string[] material_keys = AvatarChoices.instance.AvatarComponentOptions["Head"][AvatarOptions[0]].Materials;
        Material[] materials = new Material[material_keys.Length];
        for(int i=0; i<material_keys.Length; i++) materials[i] = AvatarChoices.instance.AvatarMaterialOptions[material_keys[i]][Chosen_AvatarMaterialOptions[material_keys[i]]];
        AvatarHead.GetComponent<SkinnedMeshRenderer>().sharedMaterials = materials;

        AvatarTorso.GetComponent<SkinnedMeshRenderer>().sharedMesh = AvatarChoices.instance.AvatarComponentOptions["Torso"][AvatarOptions[1]].Mesh;
        material_keys = AvatarChoices.instance.AvatarComponentOptions["Torso"][AvatarOptions[1]].Materials;
        materials = new Material[material_keys.Length];
        for(int i=0; i<material_keys.Length; i++) materials[i] = AvatarChoices.instance.AvatarMaterialOptions[material_keys[i]][Chosen_AvatarMaterialOptions[material_keys[i]]];
        AvatarTorso.GetComponent<SkinnedMeshRenderer>().sharedMaterials = materials;

        AvatarPants.GetComponent<SkinnedMeshRenderer>().sharedMesh = AvatarChoices.instance.AvatarComponentOptions["Pants"][AvatarOptions[2]].Mesh;
        material_keys = AvatarChoices.instance.AvatarComponentOptions["Pants"][AvatarOptions[2]].Materials;
        materials = new Material[material_keys.Length];
        for(int i=0; i<material_keys.Length; i++) materials[i] = AvatarChoices.instance.AvatarMaterialOptions[material_keys[i]][Chosen_AvatarMaterialOptions[material_keys[i]]];
        AvatarPants.GetComponent<SkinnedMeshRenderer>().sharedMaterials = materials;

        materials = new Material[1];
        materials[0] = AvatarChoices.instance.AvatarMaterialOptions["Skin"][Chosen_AvatarMaterialOptions["Skin"]];
        AvatarHandRight.GetComponent<SkinnedMeshRenderer>().sharedMaterials = materials;
        AvatarHandLeft.GetComponent<SkinnedMeshRenderer>().sharedMaterials = materials;

        //~~

        string[] Clothings = {"Hat", "Armor", "Waist"};
        for(int i=0; i<ClothingComponentOptions.Length; i++){
            if(ClothingComponentOptions[i] >= 0){
                string target = Clothings[i];
                int option = ClothingComponentOptions[i];
                for(int j=0; j<AvatarChoices.instance.ClothingOptions[target][option].Clothings.Length; j++){
                    GameObject ClothingComponent = Instantiate(AvatarChoices.instance.ClothingOptions[target][option].Clothings[j].Object);
                    ClothingComponent.transform.SetParent(AvatarBones[AvatarChoices.instance.ClothingOptions[target][option].Clothings[j].RootBone]);
                    ClothingComponent.transform.localPosition = AvatarChoices.instance.ClothingOptions[target][option].Clothings[j].OffsetPosition;
                    ClothingComponent.transform.localRotation = Quaternion.Euler(AvatarChoices.instance.ClothingOptions[target][option].Clothings[j].OffsetRotation);
                    ClothingComponent.transform.localScale = AvatarChoices.instance.ClothingOptions[target][option].Clothings[j].OffsetScale;

                    ClothingComponent.GetComponent<MeshRenderer>().sharedMaterial = AvatarChoices.instance.AvatarMaterialOptions[AvatarChoices.instance.ClothingOptions[target][option].Clothings[j].Material][ClothingMaterialOptions[i]];
                }
            }
        }
    }

    [PunRPC]
    public void SetNameplate(string Name){
        if(!photonView.IsMine) Nameplate.NameText.SetText(Name);
        else Nameplate.gameObject.SetActive(false);
    }
}
