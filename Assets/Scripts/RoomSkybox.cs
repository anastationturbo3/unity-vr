using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEditor;

public class RoomSkybox : MonoBehaviour {
    public Material Skybox;
    public Color AmbientColor;
    void OnEnable() {
        StartCoroutine(CameraSkybox());
        RenderSettings.ambientLight = AmbientColor;
    }

    IEnumerator CameraSkybox(){
        yield return new WaitUntil(() => XRholder.instance != null);
        XRholder.instance.vrHead.GetComponent<Skybox>().material = Skybox;
    }
}
