using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
using UnityEngine.XR.Interaction.Toolkit;

public class DiceWheel : MonoBehaviour {
    public GameObject Wheel;
    public GameObject Anchor;
    public List<GameObject> WheelButtons;

    public XRRayInteractor RayInteractor;
    public XRRayInteractor UIRayInteractor;
    public ActionBasedController ActionController;
    public XRController XRController;

    private InputActionProperty handInput_action;
    private InputHelpers.Button handInput_device;
    private bool action_mode;

    private float m_ButtonPressPoint = 0.5f;
    private bool pressed_down;

    private bool lockMode;

    void Awake(){
        Wheel.SetActive(false);

        if(ActionController != null){
            handInput_action = ActionController.uiPressAction;
            action_mode = true;
        }
        else if(XRController != null){
            handInput_device = XRController.uiPressUsage;
            action_mode = false;
        }

        pressed_down = false;
        lockMode = false;
    }

    void LateUpdate(){
        transform.position = Anchor.transform.position;
        transform.rotation = Quaternion.Euler(Anchor.transform.rotation.eulerAngles.x, Anchor.transform.rotation.eulerAngles.y, 0);
    }

    // the input stuff

    void Update() {
        if(action_mode){
            // Wheel Activation Action
            if(IsPressed(handInput_action.action)){
                if(pressed_down == false){
                    Pressed();
                    pressed_down = true;
                }
            }
            else{
                if(pressed_down == true){
                    Released();
                    pressed_down = false;
                }
            }
        }
        else{
            // Wheel Activation Device
            if(IsPressedDevice(handInput_device)){
                if(pressed_down == false){
                    Pressed();
                    pressed_down = true;
                }
            }
            else{
                if(pressed_down == true){
                    Released();
                    pressed_down = false;
                }
            }
        }

        // Option Hightlight
        if(pressed_down && Wheel.activeSelf){
            int cur_option = GetOption();
            for(int i=0; i<WheelButtons.Count; i++){
                if(i == cur_option) WheelButtons[i].GetComponent<OptionHighlight>().highlight = true;
                else WheelButtons[i].GetComponent<OptionHighlight>().highlight = false;
            }
        }
    }

    private bool IsPressed(InputAction action){
        if (action == null)
            return false;

#if INPUT_SYSTEM_1_1_OR_NEWER || INPUT_SYSTEM_1_1_PREVIEW // 1.1.0-preview.2 or newer, including pre-release
            return action.phase == InputActionPhase.Performed;
#else
        if (action.activeControl is ButtonControl buttonControl)
            return buttonControl.isPressed;

        if (action.activeControl is AxisControl)
            return action.ReadValue<float>() >= m_ButtonPressPoint;

        return action.triggered || action.phase == InputActionPhase.Performed;
#endif
    }

    // wheel stuff

    private void Pressed(){
        UnityEngine.EventSystems.RaycastResult raycastResult;
        RaycastHit raycastHit;
        if(RayInteractor.selectTarget == null && !UIRayInteractor.TryGetCurrentUIRaycastResult(out raycastResult) && !UIRayInteractor.TryGetCurrent3DRaycastHit(out raycastHit)){
            Wheel.SetActive(true);
        }
    }

    private void Released() {
        if(Wheel.activeSelf) {
            Wheel.SetActive(false);
            int cur_option = GetOption();
            DiceRoller Roller = DiceHandOption.instance.GetComponent<DiceRoller>();

            if(lockMode) {
                if(cur_option == 0) {
                    lockMode = false;
                    Roller.ThrowDice(false, TransitionManager.instance.CharacterName + " rolled:");
                    WheelButtons[0].transform.Find("Lock Text").GetComponent<TMPro.TextMeshProUGUI>().text = "lock";
                }
                if(cur_option == 15) Roller.AddDie("d3"); //eh.
                if(cur_option == 1) Roller.AddDie("d4");
                if(cur_option == 2) Roller.AddDie("d6");
                if(cur_option == 3) Roller.AddDie("d8");
                if(cur_option == 4) Roller.AddDie("d10");
                if(cur_option == 5) Roller.AddDie("d12");
                if(cur_option == 6) Roller.AddDie("d20");
                if(cur_option == 7) {
                    lockMode = false;
                    Roller.CancelThrow();
                    WheelButtons[0].transform.Find("Lock Text").GetComponent<TMPro.TextMeshProUGUI>().text = "lock";
                }
            }
            else {
                if(cur_option == 0) {
                    lockMode = true;
                    WheelButtons[0].transform.Find("Lock Text").GetComponent<TMPro.TextMeshProUGUI>().text = "roll";
                }
                if(cur_option == 1) Roller.RollSingle(new dieOption(null, "d4", "black"), TransitionManager.instance.CharacterName + " rolled:");
                if(cur_option == 2) Roller.RollSingle(new dieOption(null, "d6", "black"), TransitionManager.instance.CharacterName + " rolled:");
                if(cur_option == 3) Roller.RollSingle(new dieOption(null, "d8", "black"), TransitionManager.instance.CharacterName + " rolled:");
                if(cur_option == 4) Roller.RollSingle(new dieOption(null, "d10", "blue"), TransitionManager.instance.CharacterName + " rolled:");
                if(cur_option == 5) Roller.RollSingle(new dieOption(null, "d12", "blue"), TransitionManager.instance.CharacterName + " rolled:");
                if(cur_option == 6) Roller.RollSingle(new dieOption(null, "d20", "blue"), TransitionManager.instance.CharacterName + " rolled:");
                if(cur_option == 7) {
                    Roller.ClearDice();
                }
            }
        }
    }

    private int GetOption(){
        if(DiceHandOption.instance.CurrentHand == 1){
            float AnchorRotation = Anchor.transform.eulerAngles.z;
            if(AnchorRotation > 286 && AnchorRotation < 314) return 0;
            if(AnchorRotation > 316 && AnchorRotation < 344) return 1;
            if((AnchorRotation >= 0 && AnchorRotation < 14) || (AnchorRotation > 346 && AnchorRotation <= 360)) return 2;
            if(AnchorRotation > 16 && AnchorRotation < 44) return 3;
            if(AnchorRotation > 46 && AnchorRotation < 74) return 4;
            if(AnchorRotation > 76 && AnchorRotation < 104) return 5;
            if(AnchorRotation > 106 && AnchorRotation < 134) return 6;
            if(AnchorRotation > 136 && AnchorRotation < 164) return 7;
        }
        else if(DiceHandOption.instance.CurrentHand == 0){
            float AnchorRotation = Anchor.transform.eulerAngles.z;
            if(AnchorRotation > 46 && AnchorRotation < 74) return 0;
            if(AnchorRotation > 16 && AnchorRotation < 44) return 1;
            if((AnchorRotation > 346 && AnchorRotation <= 360) || (AnchorRotation >= 0 && AnchorRotation < 14)) return 2;
            if(AnchorRotation > 316 && AnchorRotation < 344) return 3;
            if(AnchorRotation > 286 && AnchorRotation < 314) return 4;
            if(AnchorRotation > 256 && AnchorRotation < 284) return 5;
            if(AnchorRotation > 226 && AnchorRotation < 254) return 6;
            if(AnchorRotation > 196 && AnchorRotation < 224) return 7;
        }
        return -1;
    }

    // device stuff

    private bool IsPressedDevice(InputHelpers.Button button){
        XRController.inputDevice.IsPressed(button, out var pressed, XRController.axisToPressThreshold);
        return pressed;
    }
}
