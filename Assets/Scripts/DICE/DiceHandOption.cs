using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class DiceHandOption : MonoBehaviour {
    public static DiceHandOption instance = null;
    public GameObject DiceWheelRight;
    public GameObject DiceWheelLeft;

    public int CurrentHand;
    public int SetDiceHand {
        get {
            return CurrentHand;
        }
        set {
            if(value == CurrentHand) return;
            CurrentHand = value;

            if(CurrentHand == 0){
                if(DiceWheelRight != null) DiceWheelRight.SetActive(false);
                if(DiceWheelLeft != null) DiceWheelLeft.SetActive(true);
                DiceRoller.instance.DiceSphereSwitchHand();
            } 
            else if(CurrentHand == 1){
                if(DiceWheelRight != null) DiceWheelRight.SetActive(true);
                if(DiceWheelLeft != null) DiceWheelLeft.SetActive(false);
                DiceRoller.instance.DiceSphereSwitchHand();
            }
        }    
    }

    void Awake(){
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        SetDiceHand = PlayerPrefs.GetInt("DiceHandOption");

        CurrentHand = SetDiceHand;
        if(CurrentHand == 0){
            if(DiceWheelRight != null) DiceWheelRight.SetActive(false);
            if(DiceWheelLeft != null) DiceWheelLeft.SetActive(true);
        } 
        else if(CurrentHand == 1){
            if(DiceWheelRight != null) DiceWheelRight.SetActive(true);
            if(DiceWheelLeft != null) DiceWheelLeft.SetActive(false);
        }
    }
}
