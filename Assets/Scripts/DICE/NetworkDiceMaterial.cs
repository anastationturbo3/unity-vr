using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class NetworkDiceMaterial : MonoBehaviour {
    private PhotonView photonView;
    
    void Awake(){
        photonView = GetComponent<PhotonView>();
    }

    public void SetMaterial(string color){
        photonView.RPC("SetMaterialRPC", RpcTarget.All, color);
    }

    [PunRPC]
    public void SetMaterialRPC(string color){
        Material die_mat = null;
		for(int i=0; i<NetworkDice.instance.MaterialMappingTool.Count; i++) if(NetworkDice.instance.MaterialMappingTool[i].color == color) die_mat = NetworkDice.instance.MaterialMappingTool[i].mat;
        GetComponent<Renderer>().material = die_mat; 
    }
}
