using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class DiceMap {
	public string name;
	public GameObject die;
}

[System.Serializable]
public class MaterialMap {
	public string color;
	public Material mat;
}

public class Dice : MonoBehaviour {	
	// rollSpeed determines how many seconds pass between rolling the single dice
    public float rollSpeed = 0.25F;
	
	// rolling = true when there are dice still rolling, rolling is checked using rigidBody.velocity and rigidBody.angularVelocity
    public static bool rolling = true;

	// keep rolling time to determine when dice to be rolled, have to be instantiated
    protected float rollTime = 0;
	
	// material cache
	private static ArrayList matNames = new ArrayList();
	private static ArrayList materials = new ArrayList();
    private static ArrayList rollQueue = new ArrayList();
	private static ArrayList allDice = new ArrayList();
    private static ArrayList rollingDice = new ArrayList();

	// eh
	[SerializeField] 
	private List<DiceMap> DiceMappingTool;
	public static List<DiceMap> DiceMapping;
	[SerializeField] 
	private List<MaterialMap> MaterialMappingTool;
	public static List<MaterialMap> MaterialMapping;

	[HideInInspector] public static bool separate_dice;
	[HideInInspector] public static string hearder;

	public static Dice instance = null;

	void Awake(){
		if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

		DiceMapping = DiceMappingTool;
		MaterialMapping = MaterialMappingTool;
		separate_dice = false;
	}

	//------------------------------------------------------------------------------------------------------------------------------
	// public methods
	//------------------------------------------------------------------------------------------------------------------------------	

	public static GameObject dice_prefab(string die, string mat, Vector3 position, Quaternion rotation, Vector3 scale) {
		GameObject die_obj = null;
		for(int i=0; i<DiceMapping.Count; i++) if(DiceMapping[i].name == die) die_obj = DiceMapping[i].die;

		Material die_mat = null;
		for(int i=0; i<MaterialMapping.Count; i++) if(MaterialMapping[i].color == mat) die_mat = MaterialMapping[i].mat;
		
		if(die_obj != null && die_mat != null){
			GameObject new_die = GameObject.Instantiate(die_obj , position, rotation);
			new_die.GetComponent<Renderer>().material = die_mat;
			return new_die;
		}
		else
			Debug.Log("I can't make that die!");

		return null;
	}	

	public static void Roll(string die, int count, string color, Vector3 spawnPoint, Vector3 force) {
        rolling = true;

		if (die.IndexOf("d") == 0) {
			for (int d=0; d<count; d++) {
                GameObject die_obj = dice_prefab(die, color, spawnPoint, Quaternion.identity, Vector3.one);
				die_obj.transform.Rotate(new Vector3(Random.value * 360, Random.value * 360, Random.value * 360));
				die_obj.SetActive(false);

                RollingDie rDie = new RollingDie(die_obj, die, force);

				allDice.Add(rDie);
                rollQueue.Add(rDie);
			}
		}
		else Debug.Log("That's not a valid die!");
	}

    public static int Value(string dieType){
        int v = 0;
        for (int d = 0; d < allDice.Count; d++){
            RollingDie rDie = (RollingDie) allDice[d];
            if (rDie.type == dieType) v += rDie.die.value;
        }
        return v;
    }

    public static int Count(string dieType){
        int count = 0;
        for (int d = 0; d < allDice.Count; d++){
            RollingDie rDie = (RollingDie)allDice[d];
            if (rDie.type == dieType) count++;
        }
        return count;
    }

	public static string AsString(int modifier) {
		string output = "";

		// dice rolled
		int total_roll = modifier;
		int dice_types_rolled = 0;

		if (Count("d3") > 0){
			total_roll += Value("d3");
			output += Count("d3") + "d3";
			dice_types_rolled++;
		}
		if (Count("d4") > 0){
			total_roll += Value("d4");
			output += Count("d4") + "d4";
			dice_types_rolled++;
		}
		if (Count("d6") > 0){
			total_roll += Value("d6");
			if(output != "") output += " + ";
			output += Count("d6") + "d6";
			dice_types_rolled++;
		}
		if (Count("d8") > 0){
			total_roll += Value("d8");
			if(output != "") output += " + ";
			output += Count("d8") + "d8";
			dice_types_rolled++;
		}
		if (Count("d10") > 0){
			total_roll += Value("d10");
			if(output != "") output += " + ";
			output += Count("d10") + "d10";
			dice_types_rolled++;
		}
		if (Count("d12") > 0){
			total_roll += Value("d12");
			if(output != "") output += " + ";
			output += Count("d12") + "d12";
			dice_types_rolled++;
		}
		if (Count("d20") > 0){
			total_roll += Value("d20");
			if(output != "") output += " + ";
			output += Count("d20") + "d20";
			dice_types_rolled++;
		}

		if(modifier > 0) output += " + " + modifier;
		else if(modifier < 0) output += " - " + (-modifier);

		if(!separate_dice){
			if(dice_types_rolled == 0) return "";
			else output += " : " + total_roll;
		}
		else {
			if(dice_types_rolled == 0) return "";
			else if(allDice.Count == 1) output += " : " + total_roll;
			else{
				string dice_results = "";

				for (int d = 0; d < allDice.Count; d++){
					RollingDie rDie = (RollingDie) allDice[d];
					if(dice_results != "") dice_results += " / ";
					if(d==0 && modifier != 0) dice_results += rDie.die.value + modifier;
            		else dice_results += rDie.die.value; 
				}

				output += " : " + dice_results;
			}
		}

		// dice alignment check
		string question = "";
		for (int d = 0; d < allDice.Count; d++) {
			RollingDie rDie = (RollingDie)allDice[d];
			if (rDie.die.value == 0){
				question = " (?)";
				break;
			}
		}

		if(total_roll == modifier && question != null) return "Roll in Process";
		return output + question;
	}

	/// <summary>
	/// Clears all currently rolling dice
	/// </summary>
    public static void Clear()
	{
		for (int d=0; d<allDice.Count; d++)
			GameObject.Destroy(((RollingDie)allDice[d]).gameObject);

        allDice.Clear();
        rollingDice.Clear();
        rollQueue.Clear();

        rolling = false;
	}

	/// <summary>
	/// Update is called once per frame
	/// </summary>
    void Update()
    {
        if (rolling)
        {
			// there are dice rolling so increment rolling time
            rollTime += Time.deltaTime;
			// check rollTime against rollSpeed to determine if a die should be activated ( if one available in the rolling  queue )
            if (rollQueue.Count > 0 && rollTime > rollSpeed)
            {
				// get die from rolling queue
                RollingDie rDie = (RollingDie)rollQueue[0];
                GameObject die = rDie.gameObject;
				// activate the gameObject
				die.SetActive(true);
				// apply the force impuls
                die.GetComponent<Rigidbody>().AddForce((Vector3) rDie.starting_force, ForceMode.Impulse);
				// apply a random torque
                die.GetComponent<Rigidbody>().AddTorque(new Vector3(-50 * Random.value * die.transform.localScale.magnitude, -50 * Random.value * die.transform.localScale.magnitude, -50 * Random.value * die.transform.localScale.magnitude), ForceMode.Impulse);
				// add die to rollingDice
                rollingDice.Add(rDie);
				// remove the die from the queue
                rollQueue.RemoveAt(0);
				// reset rollTime so we can check when the next die has to be rolled
                rollTime = 0;
            }
            else
                if (rollQueue.Count == 0)
                {
					// roll queue is empty so if no dice are rolling we can set the rolling attribute to false
                    if (!IsRolling())
                        rolling = false;
                }
        }
    }

	/// <summary>
	/// Check if there all dice have stopped rolling
	/// </summary>
    public static bool IsRolling()
    {
        int d = 0;
		// loop rollingDice
        while (d < rollingDice.Count)
        {
			// if rolling die no longer rolling , remove it from rollingDice
            RollingDie rDie = (RollingDie)rollingDice[d];
            if (!rDie.rolling)
                rollingDice.Remove(rDie);
            else
                d++;
        }
		// return false if we have no rolling dice 
        return (rollingDice.Count > 0);
    }

	// Get Mean Position of Dice
	public static Vector3 DiceMeanPos(){
		if (allDice.Count == 0)
			return Vector3.zero;

		float x = 0f;
		float y = 0f;
		float z = 0f;

		for (int d=0; d<allDice.Count; d++){
			x += ((RollingDie)allDice[d]).gameObject.transform.position.x;
			y = Mathf.Max(y, ((RollingDie)allDice[d]).gameObject.transform.position.y + .1f);
			z += ((RollingDie)allDice[d]).gameObject.transform.position.z;
		}

		return new Vector3(x / allDice.Count, y, z / allDice.Count);
	}
}

class RollingDie{
    public GameObject gameObject;
    public Die die;
    public string type;
	public Vector3 starting_force;

	// rolling attribute specifies if this die is still rolling
    public bool rolling{
        get{
            return die.rolling;
        }
    }

    public int value{
        get{
            return die.value;
        }
    }

	// constructor
    public RollingDie(GameObject gameObject, string type, Vector3 starting_force){
        this.gameObject = gameObject;
        this.type = type;
		this.starting_force = starting_force;

        die = (Die)gameObject.GetComponent(typeof(Die));
    }

	// ReRoll this specific die
	/*
    public void ReRoll()
    {
        if (name != "")
        {
            GameObject.Destroy(gameObject);
            Dice.Roll(name, mat, spawnPoint, force);
        }
    }
	*/
}

