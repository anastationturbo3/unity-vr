using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceRotator : MonoBehaviour {
    void FixedUpdate() {
        transform.Rotate(.7f, .7f, .7f, Space.Self);
    }
}
