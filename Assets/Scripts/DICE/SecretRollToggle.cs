using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SecretRollToggle : MonoBehaviour {
    public GameObject diceManager;

    private DiceRoller diceRoller;
    private Toggle toggle;

    void Awake(){
        diceRoller = diceManager.GetComponent<DiceRoller>();
        toggle = GetComponent<Toggle>();
    }

    void Update(){
        diceRoller.NetworkMode = !toggle.isOn;
    }
}
