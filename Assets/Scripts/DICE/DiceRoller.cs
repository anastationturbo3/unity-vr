using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

[System.Serializable]
public class dieOption {
    public GameObject die;
    public string name;
    public string color;

    public dieOption (GameObject die_, string name_, string color_){
        die = die_;
        name = name_;
        color = color_;
    }

    public dieOption (string name_, string color_){
        die = null;
        name = name_;
        color = color_;
    }

    public dieOption (dieOption existing_die){
        die = existing_die.die;
        name = existing_die.name;
        color = existing_die.color;
    }

    public dieOption (){
        die = null;
        name = "";
        color = "";
    }
}

public class DiceRoller : MonoBehaviour {
    public static DiceRoller instance = null;
    [HideInInspector] public string[] AllColors = {"black", "white", "orange", "red", "blue", "green", "yellow", "purple"};

    [Header("Basics")]
    public GameObject spawnCenter;
    public GameObject spawnCenter_Network;
    public GameObject diceSphere;
    public GameObject diceAnchor;
    private GameObject currentDiceSphere;

    [HideInInspector] public int modifier;

    [Header("Dice LIsts")]
    public List<dieOption> d3_diceList;
    public List<dieOption> d4_diceList;
    public List<dieOption> d6_diceList;
    public List<dieOption> d8_diceList;
    public List<dieOption> d10_diceList;
    public List<dieOption> d12_diceList;
    public List<dieOption> d20_diceList;
    private List<dieOption> diceToThrow;

    [Header("Spawn Anchor Vars")]
    public float spawn_x_CubeSide;
    public float spawn_y_CubeSide;
    public float spawn_z_CubeSide;
    public float spawn_x_Tolerance;
    public float spawn_y_Tolerance;
    public float spawn_z_Tolerance;

    [Header("Spawn Anchor Network Vars")]
    public float spawn_x_CubeSide_Network;
    public float spawn_y_CubeSide_Network;
    public float spawn_z_CubeSide_Network;
    public float spawn_x_Tolerance_Network;
    public float spawn_y_Tolerance_Network;
    public float spawn_z_Tolerance_Network;

    [Header("Force Control")]
    public float force_x_Variation;
    public float force_y_Variation;
    public float force_z_Variation;

    [Header("Network")]
    public bool NetworkMode;

    [Header("Diceplate")]
    public GameObject Diceplate;
    public GameObject DiceplateNetwork;
    private GameObject CurrentDiceplate;
    private GameObject CurrentDiceplateNetwork;

    void Awake(){
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
        
        currentDiceSphere = null;
        diceToThrow = new List<dieOption>();
        modifier = 0;
    }

    private Vector3 Force(Vector3 spawnPoint) {
        //Vector3 rollDirection = XRholder.instance.vrHead.transform.rotation * Vector3.back;
        Vector3 rollDirection;
        if(!NetworkMode) rollDirection = (spawnPoint - spawnCenter.transform.position).normalized;
        else rollDirection = (spawnPoint - spawnCenter_Network.transform.position).normalized;
        Vector3 rollTarget = new Vector3(rollDirection.x - force_x_Variation + Random.value*2*force_x_Variation, rollDirection.y - force_y_Variation + Random.value*2*force_y_Variation, rollDirection.z - force_z_Variation + Random.value*2*force_z_Variation);
        return Vector3.Lerp(spawnPoint, rollTarget, 1).normalized * (-3.5f - Random.value * 4f);
    }

    void OnGUI() {
        if(!NetworkMode){
            if (Dice.Count("") > 0) {
                GUI.Box(new Rect( 10 , Screen.height - 75 , Screen.width - 20 , 30), "");
                GUI.Label(new Rect(20, Screen.height - 70, Screen.width, 20), Dice.AsString(modifier));
            }
        }
        else {
            if (NetworkDice.Count("") > 0) {
                GUI.Box(new Rect( 10 , Screen.height - 75 , Screen.width - 20 , 30), "");
                GUI.Label(new Rect(20, Screen.height - 70, Screen.width, 20), NetworkDice.AsString(modifier));
            }
        }
    }

    //~~ Multi Dice Handling ~~//

    public void AddDie(dieOption theDie){
        diceToThrow.Add(theDie);
    }

    //~~ Dice Sphere ~~//

    public void DiceSphereCreate(){
        GameObject diceSphereHolder = (DiceHandOption.instance.CurrentHand == 1) ? XRholder.instance.vrRightArm : XRholder.instance.vrLeftArm;
        currentDiceSphere = Instantiate(diceSphere, diceSphereHolder.transform.position, diceSphereHolder.transform.rotation);
        currentDiceSphere.transform.SetParent(diceSphereHolder.transform);
        diceToThrow.Clear();
    }

    public void DiceSphereSwitchHand(){
        if(currentDiceSphere != null) {
            GameObject diceSphereHolder = (DiceHandOption.instance.CurrentHand == 1) ? XRholder.instance.vrRightArm : XRholder.instance.vrLeftArm;
            currentDiceSphere.transform.position = diceSphereHolder.transform.position;
            currentDiceSphere.transform.SetParent(diceSphereHolder.transform);
        }
    }

    public void AddDie(string die){
        if(diceToThrow.Count < 10){
            if(currentDiceSphere == null) DiceSphereCreate();

            dieOption theDie = new dieOption();
            if(die == "d3") theDie = new dieOption(d3_diceList[Random.Range(0, d3_diceList.Count)]);
            if(die == "d4") theDie = new dieOption(d4_diceList[Random.Range(0, d4_diceList.Count)]);
            if(die == "d6") theDie = new dieOption(d6_diceList[Random.Range(0, d6_diceList.Count)]);
            if(die == "d8") theDie = new dieOption(d8_diceList[Random.Range(0, d8_diceList.Count)]);
            if(die == "d10") theDie = new dieOption(d10_diceList[Random.Range(0, d10_diceList.Count)]);
            if(die == "d12") theDie = new dieOption(d12_diceList[Random.Range(0, d12_diceList.Count)]);
            if(die == "d20") theDie = new dieOption(d20_diceList[Random.Range(0, d20_diceList.Count)]);

            GameObject anchor = Instantiate(diceAnchor, currentDiceSphere.transform.position, Random.rotation);
            anchor.transform.SetParent(currentDiceSphere.transform);
            
            GameObject dieVisual = Instantiate(theDie.die, new Vector3(currentDiceSphere.transform.position.x + .13f + Random.Range(0f,0.04f), currentDiceSphere.transform.position.y, currentDiceSphere.transform.position.z), currentDiceSphere.transform.rotation);
            dieVisual.transform.SetParent(anchor.transform);

            diceToThrow.Add(theDie);
        }
    }

    public void ThrowDice(bool separate, string header){
        if(diceToThrow.Count > 0){
            // Log Dice
            if(CurrentDiceplate != null) {
                if(!NetworkMode) DiceLogs.instance.AddToLog(Dice.AsString(DiceRoller.instance.modifier));
                else DiceLogs.instance.AddToLog(NetworkDice.AsString(DiceRoller.instance.modifier));
            }

            // The Roll
            if(!NetworkMode){
                Dice.Clear();
                Dice.separate_dice = separate;
                Dice.hearder = header;
            }
            else{
                NetworkDice.Clear();
                NetworkDice.separate_dice = separate;
                NetworkDice.hearder = header;
            }

            Vector3 spawnPoint = XRholder.instance.vrHead.transform.position;
            if(!NetworkMode){
                if(CurrentDiceplate != null) Destroy(CurrentDiceplate);
                CurrentDiceplate = Instantiate(Diceplate, Dice.DiceMeanPos(), Quaternion.identity);

                if(spawnPoint.x > spawnCenter.transform.position.x + spawn_x_Tolerance) spawnPoint.x = spawnCenter.transform.position.x + spawn_x_Tolerance;
                else if(spawnPoint.x < spawnCenter.transform.position.x - spawn_x_Tolerance) spawnPoint.x = spawnCenter.transform.position.x - spawn_x_Tolerance;
                if(spawnPoint.y > spawnCenter.transform.position.y + spawn_y_Tolerance) spawnPoint.y = spawnCenter.transform.position.y + spawn_y_Tolerance;
                else if(spawnPoint.y < spawnCenter.transform.position.y - spawn_y_Tolerance) spawnPoint.y = spawnCenter.transform.position.y - spawn_y_Tolerance;
                if(spawnPoint.z > spawnCenter.transform.position.z + spawn_z_Tolerance) spawnPoint.z = spawnCenter.transform.position.z + spawn_z_Tolerance;
                else if(spawnPoint.z < spawnCenter.transform.position.z - spawn_z_Tolerance) spawnPoint.z = spawnCenter.transform.position.z - spawn_z_Tolerance;
            }
            else{
                if(CurrentDiceplateNetwork != null) PhotonNetwork.Destroy(CurrentDiceplateNetwork);
                CurrentDiceplateNetwork = PhotonNetwork.Instantiate("Dice/Diceplate", NetworkDice.DiceMeanPos(), Quaternion.identity);

                if(spawnPoint.x > spawnCenter_Network.transform.position.x + spawn_x_Tolerance) spawnPoint.x = spawnCenter_Network.transform.position.x + spawn_x_Tolerance;
                else if(spawnPoint.x < spawnCenter_Network.transform.position.x - spawn_x_Tolerance) spawnPoint.x = spawnCenter_Network.transform.position.x - spawn_x_Tolerance;
                if(spawnPoint.y > spawnCenter_Network.transform.position.y + spawn_y_Tolerance) spawnPoint.y = spawnCenter_Network.transform.position.y + spawn_y_Tolerance;
                else if(spawnPoint.y < spawnCenter_Network.transform.position.y - spawn_y_Tolerance) spawnPoint.y = spawnCenter_Network.transform.position.y - spawn_y_Tolerance;
                if(spawnPoint.z > spawnCenter_Network.transform.position.z + spawn_z_Tolerance) spawnPoint.z = spawnCenter_Network.transform.position.z + spawn_z_Tolerance;
                else if(spawnPoint.z < spawnCenter_Network.transform.position.z - spawn_z_Tolerance) spawnPoint.z = spawnCenter_Network.transform.position.z - spawn_z_Tolerance;
            }

            foreach(dieOption die in diceToThrow){
                if(!NetworkMode) Dice.Roll(die.name, 1, die.color, new Vector3(spawnPoint.x - spawn_x_CubeSide + Random.value*2*spawn_x_CubeSide, spawnPoint.y - spawn_y_CubeSide + Random.value*2*spawn_y_CubeSide, spawnPoint.z - spawn_z_CubeSide + Random.value*2*spawn_z_CubeSide), Force(spawnPoint));
                else NetworkDice.Roll(die.name, 1, die.color,new Vector3(spawnPoint.x - spawn_x_CubeSide + Random.value*2*spawn_x_CubeSide, spawnPoint.y - spawn_y_CubeSide + Random.value*2*spawn_y_CubeSide, spawnPoint.z - spawn_z_CubeSide + Random.value*2*spawn_z_CubeSide), Force(spawnPoint));
            }

            diceToThrow.Clear();
            Destroy(currentDiceSphere);
        }
    }

    public void CancelThrow(){
        diceToThrow.Clear();
        if(currentDiceSphere != null){
            Destroy(currentDiceSphere);
            currentDiceSphere = null;
        }
    }

    public void RollSingle(dieOption die, string header){
        // Log Dice
        if(CurrentDiceplate != null){
            if(!NetworkMode) DiceLogs.instance.AddToLog(Dice.AsString(DiceRoller.instance.modifier));
            else DiceLogs.instance.AddToLog(NetworkDice.AsString(DiceRoller.instance.modifier));
        }

        // The Roll
        if(!NetworkMode){
            Dice.Clear();
            Dice.separate_dice = false;
            Dice.hearder = header;
        }
        else{
            NetworkDice.Clear();
            NetworkDice.separate_dice = false;
            NetworkDice.hearder = header;
        }

        Vector3 spawnPoint = XRholder.instance.vrHead.transform.position;
        if(!NetworkMode){
            if(CurrentDiceplate != null) Destroy(CurrentDiceplate);
            CurrentDiceplate = Instantiate(Diceplate, Dice.DiceMeanPos(), Quaternion.identity);

            if(spawnPoint.x > spawnCenter.transform.position.x + spawn_x_Tolerance) spawnPoint.x = spawnCenter.transform.position.x + spawn_x_Tolerance;
            else if(spawnPoint.x < spawnCenter.transform.position.x - spawn_x_Tolerance) spawnPoint.x = spawnCenter.transform.position.x - spawn_x_Tolerance;
            if(spawnPoint.y > spawnCenter.transform.position.y + spawn_y_Tolerance) spawnPoint.y = spawnCenter.transform.position.y + spawn_y_Tolerance;
            else if(spawnPoint.y < spawnCenter.transform.position.y - spawn_y_Tolerance) spawnPoint.y = spawnCenter.transform.position.y - spawn_y_Tolerance;
            if(spawnPoint.z > spawnCenter.transform.position.z + spawn_z_Tolerance) spawnPoint.z = spawnCenter.transform.position.z + spawn_z_Tolerance;
            else if(spawnPoint.z < spawnCenter.transform.position.z - spawn_z_Tolerance) spawnPoint.z = spawnCenter.transform.position.z - spawn_z_Tolerance;
        }
        else{
            if(CurrentDiceplateNetwork != null) PhotonNetwork.Destroy(CurrentDiceplateNetwork);
            CurrentDiceplateNetwork = PhotonNetwork.Instantiate("Dice/Diceplate", NetworkDice.DiceMeanPos(), Quaternion.identity);

            if(spawnPoint.x > spawnCenter_Network.transform.position.x + spawn_x_Tolerance) spawnPoint.x = spawnCenter_Network.transform.position.x + spawn_x_Tolerance;
            else if(spawnPoint.x < spawnCenter_Network.transform.position.x - spawn_x_Tolerance) spawnPoint.x = spawnCenter_Network.transform.position.x - spawn_x_Tolerance;
            if(spawnPoint.y > spawnCenter_Network.transform.position.y + spawn_y_Tolerance) spawnPoint.y = spawnCenter_Network.transform.position.y + spawn_y_Tolerance;
            else if(spawnPoint.y < spawnCenter_Network.transform.position.y - spawn_y_Tolerance) spawnPoint.y = spawnCenter_Network.transform.position.y - spawn_y_Tolerance;
            if(spawnPoint.z > spawnCenter_Network.transform.position.z + spawn_z_Tolerance) spawnPoint.z = spawnCenter_Network.transform.position.z + spawn_z_Tolerance;
            else if(spawnPoint.z < spawnCenter_Network.transform.position.z - spawn_z_Tolerance) spawnPoint.z = spawnCenter_Network.transform.position.z - spawn_z_Tolerance;
        }

        if(!NetworkMode) Dice.Roll(die.name, 1, die.color, new Vector3(spawnPoint.x - spawn_x_CubeSide + Random.value*2*spawn_x_CubeSide, spawnPoint.y - spawn_y_CubeSide + Random.value*2*spawn_y_CubeSide, spawnPoint.z - spawn_z_CubeSide + Random.value*2*spawn_z_CubeSide), Force(spawnPoint));
        else NetworkDice.Roll(die.name, 1, die.color, new Vector3(spawnPoint.x - spawn_x_CubeSide + Random.value*2*spawn_x_CubeSide, spawnPoint.y - spawn_y_CubeSide + Random.value*2*spawn_y_CubeSide, spawnPoint.z - spawn_z_CubeSide + Random.value*2*spawn_z_CubeSide), Force(spawnPoint));

        diceToThrow.Clear();
        Destroy(currentDiceSphere);
    }

    public void ClearDice() {
        if(!NetworkMode) Dice.Clear();
        else NetworkDice.Clear();
    }

    public void ClearAllDice() {
        Dice.Clear();
        NetworkDice.Clear();
    }
}
