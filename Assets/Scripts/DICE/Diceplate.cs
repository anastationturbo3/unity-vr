using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Diceplate : MonoBehaviour {
    private PhotonView photonView;

    public TMPro.TextMeshProUGUI DiceText;
    public GameObject DiceHolder;
    public GameObject DicePlateCanvas;
    public bool NetworkMode;
    public GameObject TargetCamera;

    void Awake(){
        photonView = GetComponent<PhotonView>();
        DiceplateHolder.instance.AddToList(DiceHolder);
        TargetCamera = XRholder.instance.vrHead;
        if(NetworkMode && photonView.IsMine) DiceplateHolder.instance.Diceplates_Mine.Add(DiceHolder);
    }

    void LateUpdate(){
        transform.LookAt(TargetCamera.transform, Vector3.up);

        if(!NetworkMode){
            transform.position = Dice.DiceMeanPos();
            /*
            if(DiceplateHolder.instance.Diceplates.IndexOf(DiceHolder) == 0){
                transform.position = Dice.DiceMeanPos();
            }
            else{
                transform.position = new Vector3(Dice.DiceMeanPos().x,
                                                 Mathf.Max(Dice.DiceMeanPos().y, DiceplateHolder.instance.Diceplates[DiceplateHolder.instance.Diceplates.IndexOf(DiceHolder)-1].transform.position.y + PlayerPrefs.GetFloat("DiceplateStacking") * PlayerPrefs.GetFloat("DiceplateScaleOption")),
                                                 Dice.DiceMeanPos().z);
            }
            */

            string text = Dice.hearder + "\n";
            text += Dice.AsString(DiceRoller.instance.modifier);
            if(text == "" || text == "Roll in Process") DicePlateCanvas.SetActive(false);
            else {
                DicePlateCanvas.SetActive(true);
                DiceText.text = text;
            }
        }
        else if(photonView.IsMine){
            transform.position = NetworkDice.DiceMeanPos();

            string text = NetworkDice.hearder + "\n";
            text += NetworkDice.AsString(DiceRoller.instance.modifier);
            if(text == "" || text == "Roll in Process") photonView.RPC("SetDiceplateOff", RpcTarget.All);
            else photonView.RPC("SetDiceplateOn", RpcTarget.All, text);
        }
        /*
        else {
            if(DiceplateHolder.instance.Diceplates_Network.IndexOf(DiceHolder) > 0)
                transform.GetChild(0).position = new Vector3(transform.GetChild(0).position.x,
                                                             Mathf.Max(transform.GetChild(0).position.y, DiceplateHolder.instance.Diceplates_Network[DiceplateHolder.instance.Diceplates_Network.IndexOf(DiceHolder)-1].transform.GetChild(0).position.y + PlayerPrefs.GetFloat("DiceplateStacking") * PlayerPrefs.GetFloat("DiceplateScaleOption")),
                                                             transform.GetChild(0).position.z);
        }
        */
    }

    void OnDestroy(){
        DiceplateHolder.instance.RemoveFromList(DiceHolder);
    }

    public void DestroyEverywhere(){
        PhotonNetwork.Destroy(this.gameObject);
    }

    [PunRPC]
    public void SetDiceplateOff(){
        DicePlateCanvas.SetActive(false);
    }

    [PunRPC]
    public void SetDiceplateOn(string text){
        DicePlateCanvas.SetActive(true);
        DiceText.text = text;
    }
}
