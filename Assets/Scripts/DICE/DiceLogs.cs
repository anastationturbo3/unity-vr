using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceLogs : MonoBehaviour {
    public static DiceLogs instance = null;
    public List<string> logs;
    public int max_logs = 20;

    void Awake(){
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        logs = new List<string>();
    }

    public void AddToLog(string text){
        logs.Add(text);
        if(logs.Count > max_logs) logs.RemoveAt(logs.Count-1);
    }
}
