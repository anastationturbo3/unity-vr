using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnchorRotator : MonoBehaviour {
    void FixedUpdate() {
        transform.Rotate(1.0f, 0.0f, 0.0f, Space.Self);
    }
}
