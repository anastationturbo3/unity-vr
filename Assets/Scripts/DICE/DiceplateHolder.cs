using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiceplateHolder : MonoBehaviour {
    public static DiceplateHolder instance = null;
    public List<GameObject> Diceplates;
    public List<GameObject> Diceplates_Network;
    public List<GameObject> Diceplates_Mine;

    private bool DiceplatesActive;
    public bool SetDiceplates {
        get {
            return DiceplatesActive;
        }
        set {
            if(value == DiceplatesActive) return;
            DiceplatesActive = value;
            foreach(GameObject Diceplate in Diceplates) Diceplate.SetActive(DiceplatesActive);
            foreach(GameObject Diceplate in Diceplates_Network) Diceplate.SetActive(DiceplatesActive);
        }    
    }

    private float DiceplateScale;
    public float SetDiceplateScale {
        get {
            return DiceplateScale;
        }
        set {
            if(value == DiceplateScale) return;
            DiceplateScale = value;
            foreach(GameObject Diceplate in Diceplates) Diceplate.transform.localScale = Vector3.one * DiceplateScale;
            foreach(GameObject Diceplate in Diceplates_Network) Diceplate.transform.localScale = Vector3.one * DiceplateScale;
        }    
    }

    private float DiceplateAlpha;
    public float SetDiceplateAlpha {
        get {
            return DiceplateAlpha;
        }
        set {
            if(value == DiceplateAlpha) return;
            DiceplateAlpha = value;
            foreach(GameObject Diceplate in Diceplates){
                Color c = Diceplate.GetComponent<Diceplate>().DiceText.transform.parent.GetComponent<Image>().color;
                c.a = DiceplateAlpha;
                Diceplate.GetComponent<Diceplate>().DiceText.transform.parent.GetComponent<Image>().color = c;
            }
            foreach(GameObject Diceplate in Diceplates_Network){
                Color c = Diceplate.GetComponent<Diceplate>().DiceText.transform.parent.GetComponent<Image>().color;
                c.a = DiceplateAlpha;
                Diceplate.GetComponent<Diceplate>().DiceText.transform.parent.GetComponent<Image>().color = c;
            }
        }    
    }

    void Awake(){
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        Diceplates = new List<GameObject>();
        Diceplates_Network = new List<GameObject>();
        SetDiceplates = (PlayerPrefs.GetInt("DiceplateOption") != 0);

        DiceplatesActive = SetDiceplates;
        foreach(GameObject Diceplate in Diceplates) Diceplate.SetActive(DiceplatesActive);
        foreach(GameObject Diceplate in Diceplates_Network) Diceplate.SetActive(DiceplatesActive);

        DiceplateScale = PlayerPrefs.GetFloat("DiceplateScaleOption");
        foreach(GameObject Diceplate in Diceplates) Diceplate.transform.localScale = Vector3.one * DiceplateScale;
        foreach(GameObject Diceplate in Diceplates_Network) Diceplate.transform.localScale = Vector3.one * DiceplateScale;

        DiceplateAlpha = PlayerPrefs.GetFloat("DiceplateAlphaOption");
        foreach(GameObject Diceplate in Diceplates){
            Color c = Diceplate.GetComponent<Diceplate>().DiceText.transform.parent.GetComponent<Image>().color;
            c.a = DiceplateAlpha;
            Diceplate.GetComponent<Diceplate>().DiceText.transform.parent.GetComponent<Image>().color = c;
        }
        foreach(GameObject Diceplate in Diceplates_Network){
            Color c = Diceplate.GetComponent<Diceplate>().DiceText.transform.parent.GetComponent<Image>().color;
            c.a = DiceplateAlpha;
            Diceplate.GetComponent<Diceplate>().DiceText.transform.parent.GetComponent<Image>().color = c;
        }
    }

    public void AddToList(GameObject Diceplate){
        if(!Diceplate.GetComponent<Diceplate>().NetworkMode){
            if(!Diceplates.Contains(Diceplate)){
                Diceplates.Add(Diceplate);
                Diceplate.transform.localScale = Vector3.one * DiceplateScale;
                Color c = Diceplate.GetComponent<Diceplate>().DiceText.transform.parent.GetComponent<Image>().color;
                c.a = DiceplateAlpha;
                Diceplate.GetComponent<Diceplate>().DiceText.transform.parent.GetComponent<Image>().color = c;
            }
        }
        else{
            if(!Diceplates_Network.Contains(Diceplate)){
                Diceplates_Network.Add(Diceplate);
                Diceplate.transform.localScale = Vector3.one * DiceplateScale;
                Color c = Diceplate.GetComponent<Diceplate>().DiceText.transform.parent.GetComponent<Image>().color;
                c.a = DiceplateAlpha;
                Diceplate.GetComponent<Diceplate>().DiceText.transform.parent.GetComponent<Image>().color = c;
            }
        }
    }

    public void RemoveFromList(GameObject Diceplate){
        if(Diceplates.Contains(Diceplate)){
            Diceplates.Remove(Diceplate);
        }
        if(Diceplates_Network.Contains(Diceplate)){
            Diceplates_Network.Remove(Diceplate);
        }
        if(Diceplates_Mine.Contains(Diceplate)){
            Diceplates_Mine.Remove(Diceplate);
        }
    }

    public void EnableDiceplates(){
        SetDiceplates = true;
    }

    public void DisableDiceplates(){
        SetDiceplates = false;
    }

    public void ClearAllDiceplates(){
        foreach(GameObject Diceplate in Diceplates) Destroy(Diceplate);
        Diceplates.Clear();
        foreach(GameObject Diceplate in Diceplates_Network) Destroy(Diceplate);
        Diceplates_Network.Clear();
        foreach(GameObject Diceplate in Diceplates_Mine) Destroy(Diceplate);
        Diceplates_Mine.Clear();
    }

    public void ClearLocalDiceplates(){
        foreach(GameObject Diceplate in Diceplates) Destroy(Diceplate);
        Diceplates.Clear();
    }

    public void ClearNetworkDiceplates(){
        foreach(GameObject Diceplate in Diceplates_Network) Destroy(Diceplate);
        Diceplates_Network.Clear();
        foreach(GameObject Diceplate in Diceplates_Mine) Destroy(Diceplate);
        Diceplates_Mine.Clear();
    }

    public void ClearMyDiceplates(){
        foreach(GameObject Diceplate in Diceplates_Mine) Diceplate.GetComponent<Diceplate>().DestroyEverywhere();
    }
}
