using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DicePanel : MonoBehaviour {
    public DiceRoller diceRoller;
    private TMPro.TextMeshProUGUI text;

    void Awake(){
        text = transform.Find("Canvas").transform.Find("Text").GetComponent<TMPro.TextMeshProUGUI>();
    }

    public void DiplayDice(){
        text.text = Dice.AsString(diceRoller.modifier);
    }

    void Update(){
        if(Dice.Count("") > 0) {
            if(!Dice.IsRolling()) DiplayDice();
        }
    }
}
