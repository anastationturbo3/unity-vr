using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class RoomManagement : MonoBehaviour {
    public Transform RoomPresets;
    public GameObject startingRoom;
    GameObject currentRoom;

    void Awake(){
        currentRoom = startingRoom;
    }

    public void ChangeRoom(GameObject newRoom){
        if(newRoom != currentRoom){
            PhotonView photonView = PhotonView.Get(this);
            photonView.RPC("ChangeRoomPun", RpcTarget.All, newRoom.name);
        }
    }

    [PunRPC]
    void ChangeRoomPun(string newRoom_str){
        GameObject newRoom = RoomPresets.Find(newRoom_str).gameObject;
        currentRoom.SetActive(false);
        newRoom.SetActive(true);
        currentRoom = newRoom;
    }
}
