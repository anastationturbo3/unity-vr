using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using Photon.Pun;

public class Bin : MonoBehaviour {
    void OnCollisionEnter(Collision collision) {
        if(collision.gameObject.GetComponent<XRGrabNetworkInteractable>() != null){
            PhotonNetwork.Destroy(collision.gameObject);
        }
        else if(collision.gameObject.GetComponent<XRGrabInteractable>() != null){
            Destroy(collision.gameObject);
        }
    }
}
