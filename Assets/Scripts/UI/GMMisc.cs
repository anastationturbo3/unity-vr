using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GMMisc : MonoBehaviour {
    public GameObject[] GMScreen;

    public void ScreenToggle(){
        if(GMScreen[0].activeSelf) foreach(GameObject ScreenComponent in GMScreen) ScreenComponent.SetActive(false);
        else foreach(GameObject ScreenComponent in GMScreen) ScreenComponent.SetActive(true);
    }

    public void ClearGMDice(){
        DiceRoller.instance.ClearAllDice();
        DiceplateHolder.instance.ClearLocalDiceplates();
        DiceplateHolder.instance.ClearMyDiceplates();
    }

    public void ClearAllDice(){
        NetworkDiceManager.instance.ClearAllDice();
    }
}
