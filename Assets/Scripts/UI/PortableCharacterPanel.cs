using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PortableCharacterPanel : MonoBehaviour {
    private GameObject Canvas;
    private Vector3 CanvasStartingScale;

    public GameObject CharacterPanelAnchor;
    private GameObject TargetCamera;

    public GameObject BiggenButton;
    public GameObject SmallenButton;
    private bool big;

    public TMPro.TextMeshProUGUI CharacterName;
    private CharacterSheetClassSelector ClassSelector;
    public CharacterSheetAbilities AbilitiesPage;
    public CharacterSheetAbilityDescriptions DescriptionsPage;

    public Image Panel;

    void Awake(){
        TargetCamera = XRholder.instance.vrHead;
        big = false;

        Canvas = transform.Find("Canvas").gameObject;
        CanvasStartingScale = Canvas.transform.localScale;

        ClassSelector = GetComponent<CharacterSheetClassSelector>();
        if(CharacterName != null) CharacterName.SetText(TransitionManager.instance.CharacterName);
    }

    void LateUpdate(){
        if(CharacterPanelAnchor != null){
            Vector3 distancedPos = (CharacterPanelAnchor.transform.position - XRholder.instance.vrHead.transform.position).normalized * .25f + new Vector3(0f, 0.2f, 0f);;
            transform.position = CharacterPanelAnchor.transform.position + distancedPos;
        }
        transform.LookAt(TargetCamera.transform, Vector3.up);
    }

    public void SheetUpdate(){
        CharacterName.SetText(TransitionManager.instance.CharacterName);
        AbilitiesPage.UpdateAbilities();
        ClassSelector.UpdateStatSheet();
    }

    public void ChangeSize(){
        if(big){
            Canvas.transform.localScale = CanvasStartingScale;
            BiggenButton.SetActive(true);
            SmallenButton.SetActive(false);
            big = false;
        }
        else{
            Canvas.transform.localScale = CanvasStartingScale * 2.5f;
            BiggenButton.SetActive(false);
            SmallenButton.SetActive(true);
            big = true;
        }
    }

    public void Close(){
        Destroy(this.gameObject);
    }
}
