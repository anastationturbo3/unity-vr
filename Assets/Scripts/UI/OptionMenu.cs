using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionMenu : MonoBehaviour {
    public OptionList list;

    public void ChangeMenu(){
        foreach(Transform menu in transform)
            if(menu.name == "Options List" || menu.name == list.ListText.text) menu.gameObject.SetActive(true);
            else menu.gameObject.SetActive(false);
    }
}
