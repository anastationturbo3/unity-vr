using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSheetMultiRoller : MonoBehaviour {
    private string[] AllColors = {"black", "white", "orange", "red", "blue", "green", "yellow", "purple"};
    public CharacterSheetRollManager rollManager;
    public Toggle separateCheck;

    private Transform dice;
    private CharacterSheetCounter counter;
    private CharacterSheetCounter multiplier;

    void Awake(){
        if(transform.Find("Dice") != null) dice = transform.Find("Dice");
        if(transform.Find("Counter") != null) counter = transform.Find("Counter").GetComponent<CharacterSheetCounter>();
        if(transform.Find("Multiplier") != null) multiplier = transform.Find("Multiplier").GetComponent<CharacterSheetCounter>();
    }

    public void Roll(string die){
        int m_num = (multiplier == null) ? 1 : multiplier.num;
        int c_num = (counter == null) ? 1 : counter.num;

        bool separate = false;
        if(separateCheck != null) separate = separateCheck.isOn;

        if(PlayerPrefs.GetString("DiceColorOption") != "multicolor"){
            string color = PlayerPrefs.GetString("DiceColorOption");
            if (color == "random") color = DiceRoller.instance.AllColors[Random.Range(0, DiceRoller.instance.AllColors.Length)];
            rollManager.Roll(die, color, c_num, m_num, separate, TransitionManager.instance.CharacterName + " rolled:");
        }
        else {
            rollManager.PrepareRoll();
            for(int i=0; i<m_num; i++){
                string color = DiceRoller.instance.AllColors[Random.Range(0, DiceRoller.instance.AllColors.Length)];
                rollManager.AddToRoll(die, color, 1);
            }

            rollManager.CommitRoll(c_num, separate, TransitionManager.instance.CharacterName + " rolled:");
        }
    }

    public void RollSimple(string die){
        string color = PlayerPrefs.GetString("DiceColorOption");
        if (color == "random" || color == "multicolor") color = DiceRoller.instance.AllColors[Random.Range(0, DiceRoller.instance.AllColors.Length)];
        rollManager.Roll(die, color, 0, 1, false, TransitionManager.instance.CharacterName + " rolled:");
    }

    public void RollGeneral(){
        Roll(dice.Find("Dice Image").Find("Dice Counter").GetComponent<TMPro.TextMeshProUGUI>().text);
    }
}
