using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
using UnityEngine.XR.Interaction.Toolkit;

public class RaycastReveal : MonoBehaviour {
    public GameObject grabRayCast;
    private ActionBasedController ActionController;
    private XRController XRController;
    private XRInteractorLineVisual LineVisual;

    private InputActionProperty handInput_action;
    private InputHelpers.Button handInput_device;
    private bool action_mode;
    private float m_ButtonPressPoint = 0.5f;
    private bool pressed_down;

    void Awake() {
        ActionController = GetComponent<ActionBasedController>();
        XRController = GetComponent<XRController>();
        LineVisual = GetComponent<XRInteractorLineVisual>();

        if(ActionController != null){
            handInput_action = ActionController.uiPressAction;
            action_mode = true;
        }
        else if(XRController != null){
            handInput_device = XRController.uiPressUsage;
            action_mode = false;
        }

        pressed_down = false;
    }

    void Update() {
        if(action_mode){
            if(IsPressed(handInput_action.action)){
                if(pressed_down == false){
                    Pressed();
                    pressed_down = true;
                }
            }
            else{
                if(pressed_down == true){
                    Released();
                    pressed_down = false;
                }
            }
        }
        else{
            if(IsPressedDevice(handInput_device)){
                if(pressed_down == false){
                    Pressed();
                    pressed_down = true;
                }
            }
            else{
                if(pressed_down == true){
                    Released();
                    pressed_down = false;
                }
            }
        }
    }

    private bool IsPressed(InputAction action){
        if (action == null)
            return false;

#if INPUT_SYSTEM_1_1_OR_NEWER || INPUT_SYSTEM_1_1_PREVIEW // 1.1.0-preview.2 or newer, including pre-release
            return action.phase == InputActionPhase.Performed;
#else
        if (action.activeControl is ButtonControl buttonControl)
            return buttonControl.isPressed;

        if (action.activeControl is AxisControl)
            return action.ReadValue<float>() >= m_ButtonPressPoint;

        return action.triggered || action.phase == InputActionPhase.Performed;
#endif
    }

    private bool IsPressedDevice(InputHelpers.Button button){
        XRController.inputDevice.IsPressed(button, out var pressed, XRController.axisToPressThreshold);
        return pressed;
    }
    
    private void Pressed(){
        XRholder.instance.GetComponent<XRGrabManager>().lastHand = grabRayCast.GetComponent<XRRayInteractor>();
        for(int i=0; i<LineVisual.invalidColorGradient.alphaKeys.Length; i++){
            GradientAlphaKey[] keys = {new GradientAlphaKey(1f,0f), new GradientAlphaKey(1f,1f)};
            LineVisual.invalidColorGradient.SetKeys(LineVisual.invalidColorGradient.colorKeys, keys);
        }
    }

    private void Released() {
        for(int i=0; i<LineVisual.invalidColorGradient.alphaKeys.Length; i++){
            GradientAlphaKey[] keys = {new GradientAlphaKey(0f,0f), new GradientAlphaKey(0f,1f)};
            LineVisual.invalidColorGradient.SetKeys(LineVisual.invalidColorGradient.colorKeys, keys);
        }
    }
}
