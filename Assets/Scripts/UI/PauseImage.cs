using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseImage : MonoBehaviour {

    public GameObject PauseButton;
    public GameObject UnpauseButton;

    void FixedUpdate(){
        if(MusicManager.instance != null && MusicManager.instance.music_audioSrc.isPlaying){
            PauseButton.SetActive(true);
            UnpauseButton.SetActive(false);
        }
        else{
            PauseButton.SetActive(false);
            UnpauseButton.SetActive(true);
        }
    }
}
