using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSheetRoller : MonoBehaviour {
    public string die;
    public CharacterSheetRollManager rollManager;
    private CharacterSheetCounter counter;
    private CharacterSheetCounter multiplier;

    void Awake(){
        if(die.Length == 0) die = "d6";
        if(transform.Find("Counter") != null) counter = transform.Find("Counter").GetComponent<CharacterSheetCounter>();
        if(transform.Find("Multiplier") != null) multiplier = transform.Find("Multiplier").GetComponent<CharacterSheetCounter>();
    }

    public void Roll(){
        if(multiplier != null) {
            if(counter != null){
                if(PlayerPrefs.GetString("DiceColorOption") != "multicolor"){
                    string color = PlayerPrefs.GetString("DiceColorOption");
                    if (color == "random") color = DiceRoller.instance.AllColors[Random.Range(0, DiceRoller.instance.AllColors.Length)];
                    rollManager.Roll(die, color, counter.num, multiplier.num, false, TransitionManager.instance.CharacterName + " rolled:");
                }
                else {
                    rollManager.PrepareRoll();
                    for(int i=0; i<multiplier.num; i++){
                        string color = DiceRoller.instance.AllColors[Random.Range(0, DiceRoller.instance.AllColors.Length)];
                        rollManager.AddToRoll(die, color, 1);
                    }

                    rollManager.CommitRoll(counter.num, false, TransitionManager.instance.CharacterName + " rolled:");
                } 
            }
            else {
                if(PlayerPrefs.GetString("DiceColorOption") != "multicolor"){
                    string color = PlayerPrefs.GetString("DiceColorOption");
                    if (color == "random") color = DiceRoller.instance.AllColors[Random.Range(0, DiceRoller.instance.AllColors.Length)];
                    rollManager.Roll(die, color, 0, multiplier.num, false, TransitionManager.instance.CharacterName + " rolled:");
                }
                else {
                    rollManager.PrepareRoll();
                    for(int i=0; i<multiplier.num; i++){
                        string color = DiceRoller.instance.AllColors[Random.Range(0, DiceRoller.instance.AllColors.Length)];
                        rollManager.AddToRoll(die, color, 1);
                    }

                    rollManager.CommitRoll(0, false, TransitionManager.instance.CharacterName + " rolled:");
                } 
            }
        }
        else {
            string color = PlayerPrefs.GetString("DiceColorOption");
            if (color == "random" || color == "multicolor") color = DiceRoller.instance.AllColors[Random.Range(0, DiceRoller.instance.AllColors.Length)];
            if(counter != null) rollManager.Roll(die, color, counter.num, 1, false, TransitionManager.instance.CharacterName + " rolled:");
            else rollManager.Roll(die, color, 0, 1, false, TransitionManager.instance.CharacterName + " rolled:");
        }
    }

    public advancedRollOprion RollInfo(){
        if(multiplier != null) {
            if(counter != null) return new advancedRollOprion(die, counter.num, multiplier.num);
            else return new advancedRollOprion(die, 0, multiplier.num);
            
        }
        else {
            if(counter != null) return new advancedRollOprion(die, counter.num, 1);
            else return new advancedRollOprion(die, 0, 1);
        }
    }
}
