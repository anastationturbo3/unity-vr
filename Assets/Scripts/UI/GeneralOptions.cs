using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GeneralOptions : MonoBehaviour {
    [Header("Character Options")]
    public Toggle StickyhandsActive_option;
    public Slider CharacterPanelAlpha_option;
    public Slider OptionPanelAlpha_option;
    public Toggle NameplatesActive_option;
    public Slider NameplatesScale_option;
    public Slider NameplatesAlpha_option;
    [Header("Dice Options")]
    public OptionList DiceColor_option;
    public Toggle DiceplatesActive_option;
    public Slider DiceplatesScale_option;
    public Slider DiceplatesAlpha_option;
    public Slider DiceplateStacking_option;     //TODO
    public OptionList DiceHandOption_option;
    [Header("Sound Options")]
    public Slider MusicVolume_option;
    public Slider SoundVolume_option;
    public Slider VoiceVolume_option;
    [Header("GM Options")]
    public Toggle StatblockAutoLock_option;
    public Toggle DisableObjectRotation_option;

    void Awake(){
        if(DiceColor_option != null){
            DiceColor_option.SetOption(PlayerPrefs.GetInt("DiceColorOptionIndex"));
            DiceColor_option.ButtonPrev.GetComponent<Button>().onClick.AddListener(delegate {OptionsManager.instance.DiceColorOption_check(DiceColor_option.cur_value-1, DiceColor_option.ListOfThings[DiceColor_option.cur_value-1]); DiceColor_option.PrevElement();});
            DiceColor_option.ButtonNext.GetComponent<Button>().onClick.AddListener(delegate {OptionsManager.instance.DiceColorOption_check(DiceColor_option.cur_value+1, DiceColor_option.ListOfThings[DiceColor_option.cur_value+1]); DiceColor_option.NextElement();});
        }

        if(CharacterPanelAlpha_option != null){
            CharacterPanelAlpha_option.value = PlayerPrefs.GetFloat("PanelAlphaOption");
            CharacterPanelAlpha_option.onValueChanged.AddListener(delegate {
                OptionsManager.instance.PanelAlpha_check(CharacterPanelAlpha_option.value);
            });
        }

        if(OptionPanelAlpha_option != null){
            OptionPanelAlpha_option.value = PlayerPrefs.GetFloat("PanelAlphaOption");
            OptionPanelAlpha_option.onValueChanged.AddListener(delegate {
                OptionsManager.instance.OptionPanelAlpha_check(OptionPanelAlpha_option.value);
            });
        }
        
        if(NameplatesActive_option != null){
            NameplatesActive_option.isOn = (PlayerPrefs.GetInt("NameplateOption") != 0);
            NameplatesActive_option.onValueChanged.AddListener(delegate {
                OptionsManager.instance.NameplatesActive_check(NameplatesActive_option.isOn);
            });
        }

        if(NameplatesScale_option != null){
            NameplatesScale_option.value = PlayerPrefs.GetFloat("NameplateScaleOption");
            NameplatesScale_option.onValueChanged.AddListener(delegate {
                OptionsManager.instance.NameplateScale_check(NameplatesScale_option.value);
            });
        }

        if(NameplatesAlpha_option != null){
            NameplatesAlpha_option.value = PlayerPrefs.GetFloat("NameplateAlphaOption");
            NameplatesAlpha_option.onValueChanged.AddListener(delegate {
                OptionsManager.instance.NameplateAlpha_check(NameplatesAlpha_option.value);
            });
        }

        if(DiceplatesActive_option != null){
            DiceplatesActive_option.isOn = (PlayerPrefs.GetInt("DiceplateOption") != 0);
            DiceplatesActive_option.onValueChanged.AddListener(delegate {
                OptionsManager.instance.DiceplatesActive_check(DiceplatesActive_option.isOn);
            });
        }

        if(DiceplatesScale_option != null){
            DiceplatesScale_option.value = PlayerPrefs.GetFloat("DiceplateScaleOption");
            DiceplatesScale_option.onValueChanged.AddListener(delegate {
                OptionsManager.instance.DiceplateScale_check(DiceplatesScale_option.value);
            });
        }

        if(DiceplatesAlpha_option != null){
            DiceplatesAlpha_option.value = PlayerPrefs.GetFloat("DiceplateAlphaOption");
            DiceplatesAlpha_option.onValueChanged.AddListener(delegate {
                OptionsManager.instance.DiceplateAlpha_check(DiceplatesAlpha_option.value);
            });
        }

        if(DiceplateStacking_option != null){
            DiceplateStacking_option.value = (PlayerPrefs.GetFloat("DiceplateStacking"));
            DiceplateStacking_option.onValueChanged.AddListener(delegate {
                OptionsManager.instance.DiceplateStacking_check(DiceplateStacking_option.value);
            });
        }

        if(StickyhandsActive_option != null){
            StickyhandsActive_option.isOn = (PlayerPrefs.GetInt("StickyHandsOption") != 0);
            StickyhandsActive_option.onValueChanged.AddListener(delegate {
                OptionsManager.instance.StickyhandsActive_check(StickyhandsActive_option.isOn);
            });
        }

        if(DiceHandOption_option != null){
            DiceHandOption_option.SetOption(PlayerPrefs.GetInt("DiceHandOption"));
            DiceHandOption_option.ButtonPrev.GetComponent<Button>().onClick.AddListener(delegate {OptionsManager.instance.DiceHandOption_check(DiceHandOption_option.cur_value-1); DiceHandOption_option.PrevElement();});
            DiceHandOption_option.ButtonNext.GetComponent<Button>().onClick.AddListener(delegate {OptionsManager.instance.DiceHandOption_check(DiceHandOption_option.cur_value+1); DiceHandOption_option.NextElement();});
        }

        if(MusicVolume_option != null){
            MusicVolume_option.value = PlayerPrefs.GetFloat("MusicVolume");
            MusicVolume_option.onValueChanged.AddListener(delegate {
                OptionsManager.instance.MusicVolume_check(MusicVolume_option.value);
            });
        }

        if(SoundVolume_option != null){
            SoundVolume_option.value = PlayerPrefs.GetFloat("SoundVolume");
            SoundVolume_option.onValueChanged.AddListener(delegate {
                OptionsManager.instance.SoundVolume_check(SoundVolume_option.value);
            });
        }

        if(VoiceVolume_option != null){
            VoiceVolume_option.value = PlayerPrefs.GetFloat("VoiceVolume");
            VoiceVolume_option.onValueChanged.AddListener(delegate {
                OptionsManager.instance.VoiceVolume_check(VoiceVolume_option.value);
            });
        }

        if(StatblockAutoLock_option != null){
            StatblockAutoLock_option.isOn = (PlayerPrefs.GetInt("StatblockAutoLock") != 0);
            StatblockAutoLock_option.onValueChanged.AddListener(delegate {
                OptionsManager.instance.StatblockAutoLock_check(StatblockAutoLock_option.isOn);
            });
        }

        if(DisableObjectRotation_option != null){
            DisableObjectRotation_option.isOn = (PlayerPrefs.GetInt("DisableObjectRotation") != 0);
            DisableObjectRotation_option.onValueChanged.AddListener(delegate {
                OptionsManager.instance.DisableObjectRotation_check(DisableObjectRotation_option.isOn);
            });
        }
    }
}
