using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionList : MonoBehaviour {
    public List<string> ListOfThings;
    public GameObject ButtonPrev;
    public GameObject ButtonNext;
    public TMPro.TextMeshProUGUI ListText;
    public bool loop;

    public int cur_value;

    public void SetOption(int value) {
        cur_value = value;

        if(cur_value == 0 && !loop){
            ButtonPrev.SetActive(false);
            ButtonNext.SetActive(true);
        }
        else if(cur_value == ListOfThings.Count-1  && !loop){
            ButtonPrev.SetActive(true);
            ButtonNext.SetActive(false);
        }
        else {
            ButtonPrev.SetActive(true);
            ButtonNext.SetActive(true);
        }

        ListText.text = ListOfThings[cur_value];   
    }

    public void NextElement(){
        if(cur_value < ListOfThings.Count-1) SetOption(cur_value + 1);
        else if(loop) SetOption(0);
    }

    public void PrevElement(){
        if(cur_value > 0) SetOption(cur_value - 1);
        else if(loop) SetOption(ListOfThings.Count-1);
    }

    public void SetElement(int value){
        SetOption(value);
    }
}
