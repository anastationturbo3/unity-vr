using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollerReset : MonoBehaviour {
    private bool should_update;
    public bool reverse;

    void Awake(){
        should_update = true;
    }

    void OnEnable(){
        should_update = true;
    }

    void Update(){
        if(should_update){
            if(!reverse) GetComponent<Scrollbar>().value = 0f;
            else GetComponent<Scrollbar>().value = 1f;
            should_update = false;
        }
    }
}
