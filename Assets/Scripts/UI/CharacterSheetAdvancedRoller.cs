using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class advancedRollOprion {
    public string die;
    public int modifier;
    public int multiplier;

    public advancedRollOprion(string die_){
        die = die_;
        modifier = 0;
        multiplier = 1;
    }

    public advancedRollOprion(string die_, int modifier_, int multiplier_){
        die = die_;
        modifier = modifier_;
        multiplier = multiplier_;
    }
}

public class CharacterSheetAdvancedRoller : MonoBehaviour {
    public List<GameObject> RollSteps;
    public CharacterSheetRollManager rollManager;

    public void RollTheBones(){
        int mod = 0;
        string chosen_color = PlayerPrefs.GetString("DiceColorOption");
        if (chosen_color == "random") chosen_color = DiceRoller.instance.AllColors[Random.Range(0, DiceRoller.instance.AllColors.Length)];

        rollManager.PrepareRoll();
        for(int i=0; i<RollSteps.Count; i++){
            if(RollSteps[i].activeInHierarchy){
                string color = chosen_color;
                if(color == "multicolor") color = DiceRoller.instance.AllColors[Random.Range(0, DiceRoller.instance.AllColors.Length)];
                advancedRollOprion rollInfo = RollSteps[i].GetComponent<CharacterSheetRoller>().RollInfo();
                rollManager.AddToRoll(rollInfo.die, color, rollInfo.multiplier);
                mod += rollInfo.modifier;
            }
        }

        rollManager.CommitRoll(mod, false, TransitionManager.instance.CharacterName + " rolled:");
    }
}
