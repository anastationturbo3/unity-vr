using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSheetCounter : MonoBehaviour {
    public int num;
    public int max_num = 0;
    public bool pos_only;
    public bool non_negative;

    private TMPro.TextMeshProUGUI counter;

    void Awake(){
        NumberToCounter();
    }

    void NumberToCounter(){
        counter = transform.Find("Text").GetComponent<TMPro.TextMeshProUGUI>();
        
        if(num == 0){
            counter.text = num.ToString();
            counter.fontSize = 20;
        }
        else if(num > 0){
            if(pos_only || non_negative){
                counter.text = num.ToString();
                if(num < 10) counter.fontSize = 20;
                else counter.fontSize = 14;
            }
            else{
                counter.text = "+" + num.ToString();
                if(num < 10) counter.fontSize = 14;
                else counter.fontSize = 9.5f;
            }
        }
        else if(num < 0){
            counter.text = num.ToString();
            if(num > -10) counter.fontSize = 14;
            else counter.fontSize = 9.5f;
        }

        if(max_num > 0){
            counter.text += "/" + max_num.ToString();
            int max_fontSize = 20;
            if(max_num >= 10) max_fontSize = 14;
            counter.fontSize = (counter.fontSize + max_fontSize)/2;
        }
    }

    public void UpdateCounter(int change){
        num += change;
        if(pos_only && num < 1) num = 1;
        if(non_negative  && num < 0) num = 0;
        if(max_num > 0 && num > max_num) num = max_num;
        NumberToCounter();
    }

    public void SetConuter(int number){
        num = number;
        if(pos_only && num < 1) num = 1;
        if(non_negative  && num < 0) num = 0;
        if(max_num > 0 && num > max_num) num = max_num;
        NumberToCounter();
    }
}
