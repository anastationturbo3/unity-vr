using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleOnOff : MonoBehaviour {
    public GameObject target;
    private Toggle toggle;

    void Awake(){
        toggle = GetComponent<Toggle>();
    }

    void Update(){
        if(toggle.isOn) target.SetActive(true);
        else target.SetActive(false);
    }
}