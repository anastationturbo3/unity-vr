using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMaterial : MonoBehaviour {
    public AvatarOptions AvatarOptions;
    public string target;
    public int option;

    public void Change(){
        AvatarOptions.ChangeMaterial(target, option);
    }

    public void ChangeClothing(){
        AvatarOptions.ChangeClothingMaterial(target, option);
    }
}
