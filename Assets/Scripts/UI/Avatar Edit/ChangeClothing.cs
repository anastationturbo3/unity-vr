using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeClothing : MonoBehaviour {
    public AvatarOptions AvatarOptions;
    public string target;
    public int option;

    public void Change(){
        AvatarOptions.ChangeClothing(target, option);
    }
}