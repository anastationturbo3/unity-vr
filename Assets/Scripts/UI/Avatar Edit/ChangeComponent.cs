using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeComponent : MonoBehaviour {
    public AvatarOptions AvatarOptions;
    public string target;
    public int option;

    public void Change(){
        AvatarOptions.ChangeComponent(target, option);
    }
}
