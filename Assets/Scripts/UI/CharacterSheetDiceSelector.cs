using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSheetDiceSelector : MonoBehaviour {
    public Transform current_dice;

    public void ChangeDice(Transform new_dice){
        Destroy(current_dice.Find("Dice Image").gameObject);
        Transform created_dice_image = Instantiate(new_dice.Find("Dice Image"), current_dice);
        created_dice_image.name = "Dice Image";
    }
}
