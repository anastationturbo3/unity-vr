using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonHighlight : MonoBehaviour {
    public Color base_color;
    public Color highlight_color;
    public Button current_button;

    void Awake(){
        Highlight();
    }

    void Highlight(){
        ColorBlock c = current_button.colors;
        c.normalColor = highlight_color;
        c.highlightedColor = highlight_color;
        c.selectedColor = highlight_color;
        current_button.colors = c;
        if(current_button.GetComponent<Image>() != null) current_button.GetComponent<Image>().color = c.normalColor;
    }

    void UnHighlight(){
        ColorBlock c = current_button.colors;
        c.normalColor = base_color;
        c.highlightedColor = base_color;
        c.selectedColor = base_color;
        current_button.colors = c;
        if(current_button.GetComponent<Image>() != null) current_button.GetComponent<Image>().color = c.normalColor;
    }

    public void ChangeButton(Button new_button){
        UnHighlight();
        current_button = new_button;
        Highlight();
    }
}
