using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSheetManager : MonoBehaviour {
    private GameObject Canvas;
    private GameObject Collider;
    private Vector3 CanvasStartingScale;
    private Vector3 ColliderStartingScale;

    public GameObject BiggenButton;
    public GameObject SmallenButton;
    private bool big;

    public Image Panel;

    void Awake(){
        Canvas = transform.Find("Canvas").gameObject;
        Collider = transform.Find("Colliders").gameObject;
        CanvasStartingScale = Canvas.transform.localScale;
        ColliderStartingScale = Collider.transform.localScale;
        big = false;
    }

    public void ChangeSize(){
        if(big){
            Canvas.transform.localScale = CanvasStartingScale;
            Collider.transform.localScale = ColliderStartingScale;
            BiggenButton.SetActive(true);
            SmallenButton.SetActive(false);
            big = false;
        }
        else{
            Canvas.transform.localScale = CanvasStartingScale * 2.5f;
            Collider.transform.localScale = ColliderStartingScale * 2.5f;
            BiggenButton.SetActive(false);
            SmallenButton.SetActive(true);
            big = true;
        }
    }

    /*
    public void Lock(){
        lockSheet = true;
        LockedButton.SetActive(true);
        UnlockedButton.SetActive(false);
    }

    public void Unlock(){
        lockSheet = false;
        LockedButton.SetActive(false);
        UnlockedButton.SetActive(true);
    }

    public void LockToggle(){
        lockSheet = !lockSheet;
        LockedButton.SetActive(lockSheet);
        UnlockedButton.SetActive(!lockSheet);
    }
    */

    public void Close(){
        Destroy(this.gameObject);
    }
}
