using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GMRoller : MonoBehaviour {
    private Transform dice;
    private CharacterSheetCounter counter;
    private CharacterSheetCounter multiplier;
    public Toggle separateCheck;

    void Awake(){
        if(transform.Find("Dice") != null) dice = transform.Find("Dice");
        if(transform.Find("Counter") != null) counter = transform.Find("Counter").GetComponent<CharacterSheetCounter>();
        if(transform.Find("Multiplier") != null) multiplier = transform.Find("Multiplier").GetComponent<CharacterSheetCounter>();
    }

    public void Roll(string die, bool separate){
        int m_num = (multiplier == null) ? 1 : multiplier.num;
        int c_num = (counter == null) ? 1 : counter.num;

        if(m_num < 1) return; 
        DiceRoller.instance.modifier = c_num;

        if(m_num == 1){
            string color = PlayerPrefs.GetString("DiceColorOption");
            if (color == "random" || color == "multicolor") color = DiceRoller.instance.AllColors[Random.Range(0, DiceRoller.instance.AllColors.Length)];
            DiceRoller.instance.RollSingle(new dieOption(die, color), "The Dungeon Master rolled:");
        }
        else {
            Dice.Clear();
            string chosen_color = PlayerPrefs.GetString("DiceColorOption");
            if(chosen_color == "random") chosen_color = DiceRoller.instance.AllColors[Random.Range(0, DiceRoller.instance.AllColors.Length)];
            for(int i=0; i<m_num; i++){
                string color = chosen_color;
                if(color == "multicolor") color = DiceRoller.instance.AllColors[Random.Range(0, DiceRoller.instance.AllColors.Length)];
                DiceRoller.instance.AddDie(new dieOption(die, color));
            }
            DiceRoller.instance.ThrowDice(separate, "The Dungeon Master  rolled:");
        }
    }

    public void RollSimple(string die){
        DiceRoller.instance.modifier = 0;
        string color = PlayerPrefs.GetString("DiceColorOption");
        if (color == "random" || color == "multicolor") color = DiceRoller.instance.AllColors[Random.Range(0, DiceRoller.instance.AllColors.Length)];
        DiceRoller.instance.RollSingle(new dieOption(die, color), TransitionManager.instance.CharacterName + " rolled:");
    }

    public void RollGeneral(){
        bool separate = false;
        if(separateCheck != null) separate = separateCheck.isOn;
        Roll(dice.Find("Dice Image").Find("Dice Counter").GetComponent<TMPro.TextMeshProUGUI>().text, separate);
    }
}
