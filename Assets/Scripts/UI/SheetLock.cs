using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SheetLock : MonoBehaviour {
    private bool lockSheet;
    public string LockOptionName;

    void Awake(){
        lockSheet = false;
    }

    void Update(){
        GetComponent<Rigidbody>().isKinematic = lockSheet;
        if(PlayerPrefs.GetInt(LockOptionName) == 0) lockSheet = false;
    }

    public void GrabLock(){
        if(PlayerPrefs.GetInt(LockOptionName) == 1) lockSheet = true;
    }
}
