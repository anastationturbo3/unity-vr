using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleSprite : MonoBehaviour{
    private Image ToggleImage;
    private Toggle ToggleButton;
    private Color OriginalColor;

    void Awake(){
        ToggleImage = GetComponent<Image>();
        ToggleButton = GetComponentInParent<Toggle>();
        OriginalColor = GetComponent<Image>().color;
    }

    void Update(){
        if(ToggleButton.isOn == true) ToggleImage.color = new Color(OriginalColor.r, OriginalColor.g , OriginalColor.b, 1);
        else  ToggleImage.color = new Color(OriginalColor.r, OriginalColor.g , OriginalColor.b, 0);
    }
}
