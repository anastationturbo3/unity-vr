using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSheetRollManager : MonoBehaviour {
    private DiceRoller DiceManager;

    void Awake(){
        DiceManager = GameObject.Find("Dice Manager").GetComponent<DiceRoller>();
    }

    // simple rolls

    public void Roll(string die, string color, int mod, int multi, bool separate, string header){
        if(DiceManager == null) DiceManager = GameObject.Find("Dice Manager").GetComponent<DiceRoller>();
        if(multi < 1) return; 
        DiceManager.modifier = mod;

        if(multi == 1) DiceManager.RollSingle(new dieOption(die, color), header);
        else {
            Dice.Clear();
            for(int i=0; i<multi; i++) DiceManager.AddDie(new dieOption(die, color));
            DiceManager.ThrowDice(separate, header);
        }
    }

    // multi-dice rolls

    public void PrepareRoll(){
        Dice.Clear();
    }

    public void AddToRoll(string die, string color, int multi){
        if(DiceManager == null) DiceManager = GameObject.Find("Dice Manager").GetComponent<DiceRoller>();
        for(int i=0; i<multi; i++) DiceManager.AddDie(new dieOption(die, color));
    }

    public void CommitRoll(int mod, bool separate, string header){
        if(DiceManager == null) DiceManager = GameObject.Find("Dice Manager").GetComponent<DiceRoller>();
        DiceManager.modifier = mod;
        DiceManager.ThrowDice(separate, header);
    }

    // misc

    public void ClearDice(){
        DiceRoller.instance.ClearDice();
        if(!TransitionManager.instance.InTheNetwork) DiceplateHolder.instance.ClearLocalDiceplates();
        else DiceplateHolder.instance.ClearMyDiceplates();
    }
}
