using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionHighlight : MonoBehaviour {
    public bool highlight = false;

    void FixedUpdate(){
        if(highlight) {
            transform.localScale = new Vector3(1, 1, 1);
            GetComponent<Image>().color = new Color(.5f,.5f,.5f);
            
        }
        else {
            transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
            GetComponent<Image>().color = new Color(0,0,0);
        }
    }
}
