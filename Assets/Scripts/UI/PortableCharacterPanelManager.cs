using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortableCharacterPanelManager : MonoBehaviour {
    public static PortableCharacterPanelManager instance = null;

    public GameObject PortableCharacterPanel;
    public GameObject CurrentPortableCharacterPanel;

    private float PanelAlpha;
    public float SetPanelAlpha {
        get {
            return PanelAlpha;
        }
        set {
            if(value == PanelAlpha) return;
            PanelAlpha = value;
            Color c = CurrentPortableCharacterPanel.GetComponent<PortableCharacterPanel>().Panel.color;
            c.a = PanelAlpha;
            CurrentPortableCharacterPanel.GetComponent<PortableCharacterPanel>().Panel.color = c;
        }    
    }

    void Awake(){
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        PanelAlpha = PlayerPrefs.GetFloat("PanelAlphaOption");
    }

    public void Summon(Vector3 pos){
        Vector3 distancedPos = (pos - XRholder.instance.vrHead.transform.position).normalized * .25f + new Vector3(0f, 0.2f, 0f);
        if(CurrentPortableCharacterPanel == null) CurrentPortableCharacterPanel = Instantiate(PortableCharacterPanel, pos + distancedPos, Quaternion.identity);
        else CurrentPortableCharacterPanel.transform.position = pos + distancedPos;

        Color c = CurrentPortableCharacterPanel.GetComponent<PortableCharacterPanel>().Panel.color;
        c.a = PanelAlpha;
        CurrentPortableCharacterPanel.GetComponent<PortableCharacterPanel>().Panel.color = c;
    }
}
