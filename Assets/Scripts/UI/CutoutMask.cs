﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering;

public class CutoutMask : Image {

    Material new_material;

    public override Material materialForRendering {
        get{
            new_material = new Material(base.materialForRendering);
            new_material.SetInt("_StencilComp", (int)CompareFunction.NotEqual);
            return new_material;
        }
    }
}
