using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Class {
    public string name;
    public int hp;
    public int sp;
    public int ac;
    public List<string> abilities;
    public List<string> equipment;
    public string held_left;
    public string held_right;

    public Class(Class class_){
        name = class_.name;
        hp = class_.hp;
        sp = class_.sp;
        ac = class_.ac;
        abilities = new List<string>(class_.abilities);
        equipment = new List<string>(class_.equipment);
        held_left = class_.held_left;
        held_right = class_.held_right;
    }
}

public class CharacterOptions : MonoBehaviour {
    public static CharacterOptions instance = null;

    public Class[] Classes;

    public Class selected_class;
    [HideInInspector] public string selected_class_name;
    [HideInInspector] public int hp_current;
    [HideInInspector] public int hp_max;
    [HideInInspector] public int sp_current;
    [HideInInspector] public int sp_max;
    [HideInInspector] public int ac;
    [HideInInspector] public int ac_bonus;
    [HideInInspector] public List<string> abilities;
    [HideInInspector] public List<string> equipment;

    void Awake(){
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(this.gameObject);

        selected_class = new Class(Classes[0]);
        selected_class_name = Classes[0].name;
        hp_current = Classes[0].hp;
        hp_max = Classes[0].hp;
        sp_current = Classes[0].sp;
        sp_max = Classes[0].sp;
        ac = Classes[0].ac;
        ac_bonus = 0;
        abilities = new List<string>(Classes[0].abilities);
        equipment = new List<string>(Classes[0].equipment);
    }

    public void ResetEquipment(){
        foreach(string e in equipment){
            TheCollection.instance.AbilityDescriptions.transform.Find(e).GetComponent<AbilityDescription>().DestroySummoned();
            if(PortableCharacterPanelManager.instance.CurrentPortableCharacterPanel != null)
                PortableCharacterPanelManager.instance.CurrentPortableCharacterPanel.GetComponent<PortableCharacterPanel>().DescriptionsPage.transform.Find(e).gameObject.SetActive(false);
        }
        if(PortableCharacterPanelManager.instance.CurrentPortableCharacterPanel != null)
            PortableCharacterPanelManager.instance.CurrentPortableCharacterPanel.GetComponent<PortableCharacterPanel>().AbilitiesPage.gameObject.SetActive(true);
    }

    public void ChangeClass(int new_class_num){
        ResetEquipment();

        selected_class = new Class(Classes[new_class_num]);
        selected_class_name = Classes[new_class_num].name;
        hp_current = Classes[new_class_num].hp;
        hp_max = Classes[new_class_num].hp;
        sp_current = Classes[new_class_num].sp;
        sp_max = Classes[new_class_num].sp;
        ac = Classes[new_class_num].ac;
        abilities = new List<string>(Classes[new_class_num].abilities);
        equipment = new List<string>(Classes[new_class_num].equipment);

        if(PortableCharacterPanelManager.instance.CurrentPortableCharacterPanel != null)
            PortableCharacterPanelManager.instance.CurrentPortableCharacterPanel.GetComponent<PortableCharacterPanel>().SheetUpdate();
    }

    public void UpdateSheet(){
        if(PortableCharacterPanelManager.instance.CurrentPortableCharacterPanel != null)
            PortableCharacterPanelManager.instance.CurrentPortableCharacterPanel.GetComponent<PortableCharacterPanel>().SheetUpdate();
    }
}
