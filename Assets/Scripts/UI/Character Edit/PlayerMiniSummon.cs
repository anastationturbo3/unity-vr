using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.Interaction.Toolkit;
using Photon.Pun;

public class PlayerMiniSummon : MonoBehaviour {
    public GameObject Summonable;
    [HideInInspector] public GameObject CurrentSummoned;

    [Header ("Button Things")]
    public Color base_color;
    public Color highlight_color;
    public Button current_button;
    public Image[] extra_images;

    public void Summon(){
        if(CurrentSummoned == null){
            XRRayInteractor Hand = XRholder.instance.GetComponent<XRGrabManager>().lastHand;

            if(!TransitionManager.instance.InTheNetwork) CurrentSummoned = Instantiate(Summonable, Hand.gameObject.transform.position, Quaternion.identity);
            else CurrentSummoned = PhotonNetwork.Instantiate("Minis/" + Summonable.name, Hand.gameObject.transform.position, Quaternion.identity);

            if(Hand.selectTarget != null){
                Hand.interactionManager.SelectExit(Hand, Hand.selectTarget);
            }

            Hand.interactionManager.SelectEnter(Hand, CurrentSummoned.GetComponent<XRBaseInteractable>());

            // Material Things
            string color = PlayerPrefs.GetString("DiceColorOption");
            if (color == "random" || color == "multicolor") color = DiceRoller.instance.AllColors[Random.Range(0, DiceRoller.instance.AllColors.Length)];
            CurrentSummoned.GetComponent<ObjectList>().MaterialChange(color);
            
            // Button Things
            ColorBlock c = current_button.colors;
            c.normalColor = highlight_color;
            c.highlightedColor = highlight_color;
            c.selectedColor = highlight_color;
            current_button.colors = c;
            if(current_button.GetComponent<Image>() != null) current_button.GetComponent<Image>().color = c.normalColor;
            foreach(Image i in extra_images) i.color = c.normalColor;
        }
        else{
            DestroySummoned();
        }
    }

    public void DestroySummoned(){
        if(CurrentSummoned != null){
            if(!TransitionManager.instance.InTheNetwork) Destroy(CurrentSummoned);
            else PhotonNetwork.Destroy(CurrentSummoned);
        }

        // Button Things
        ColorBlock c = current_button.colors;
        c.normalColor = base_color;
        c.highlightedColor = base_color;
        c.selectedColor = base_color;
        current_button.colors = c;
        if(current_button.GetComponent<Image>() != null) current_button.GetComponent<Image>().color = c.normalColor;
        foreach(Image i in extra_images) i.color = c.normalColor;
    }
}
