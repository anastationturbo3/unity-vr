using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GearSet {
    public GameObject Menu;
    public List<OptionList> Options;
}

public class ChooseGear : MonoBehaviour {
    public List<GearSet> GearLists;
    private int cur_list = 1;

    void Awake(){
        for(int i=0; i<GearLists.Count; i++){
            if(i==0){
                GearLists[i].Menu.SetActive(true);
                foreach(OptionList option in GearLists[i].Options){
                    option.SetElement(0);
                }
                cur_list = i;
            }
            else{
                GearLists[i].Menu.SetActive(false);
            }
        }
    }

    public void ResetGears(GameObject Selected){
        for(int i=0; i<GearLists.Count; i++){
            if(GearLists[i].Menu == Selected){
                GearLists[i].Menu.SetActive(true);
                foreach(OptionList option in GearLists[i].Options){
                    option.SetElement(0);
                }
                cur_list = i;
            }
            else{
                GearLists[i].Menu.SetActive(false);
            }
        }
    }

    public void ChangeGear(int item_index, int list_index){
        string cur_equipment = CharacterOptions.instance.equipment[item_index];

        TheCollection.instance.AbilityDescriptions.transform.Find(cur_equipment).GetComponent<AbilityDescription>().DestroySummoned();
        if(PortableCharacterPanelManager.instance.CurrentPortableCharacterPanel != null){
            if(PortableCharacterPanelManager.instance.CurrentPortableCharacterPanel.GetComponent<PortableCharacterPanel>().DescriptionsPage.transform.Find(cur_equipment).gameObject.activeInHierarchy){
                PortableCharacterPanelManager.instance.CurrentPortableCharacterPanel.GetComponent<PortableCharacterPanel>().DescriptionsPage.transform.Find(cur_equipment).gameObject.SetActive(false);
                PortableCharacterPanelManager.instance.CurrentPortableCharacterPanel.GetComponent<PortableCharacterPanel>().AbilitiesPage.gameObject.SetActive(true);
            }
        }

        CharacterOptions.instance.equipment[item_index] = GearLists[cur_list].Options[list_index].ListOfThings[GearLists[cur_list].Options[list_index].cur_value];
        if(CharacterOptions.instance.selected_class.held_left == cur_equipment) CharacterOptions.instance.selected_class.held_left = CharacterOptions.instance.equipment[item_index];
        if(CharacterOptions.instance.selected_class.held_right == cur_equipment) CharacterOptions.instance.selected_class.held_right = CharacterOptions.instance.equipment[item_index];
        CharacterOptions.instance.UpdateSheet();
    }
}
