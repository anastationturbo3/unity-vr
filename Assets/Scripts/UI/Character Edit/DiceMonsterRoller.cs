using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceMonsterRoller : MonoBehaviour {
    public CharacterSheetRollManager rollManager;

    [Header ("Roll Info")]
    public string die;
    public int number;
    public int modifier;
    public string color;
    public string battlecry;
    public bool separate;

    public void Roll(){
        if(number > 0){
            rollManager.Roll(die, color, modifier, number, separate, battlecry);
        }
    }
}
