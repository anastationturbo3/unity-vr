using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityScoreTranslator : MonoBehaviour {
    public CharacterSheetCounter AbilityScore;
    public CharacterSheetCounter AbilityModifier;

    void OnEnable(){
        UpdateAbility();
    }

    public void UpdateAbility(){
        if(AbilityScore.num == 0 || AbilityScore.num == 1) AbilityModifier.num = -5;
        if(AbilityScore.num == 2 || AbilityScore.num == 3) AbilityModifier.num = -4;
        if(AbilityScore.num == 4 || AbilityScore.num == 5) AbilityModifier.num = -3;
        if(AbilityScore.num == 6 || AbilityScore.num == 7) AbilityModifier.num = -2;
        if(AbilityScore.num == 8 || AbilityScore.num == 9) AbilityModifier.num = -1;
        if(AbilityScore.num == 10 || AbilityScore.num == 11) AbilityModifier.num = 0;
        if(AbilityScore.num == 12 || AbilityScore.num == 13) AbilityModifier.num = 1;
        if(AbilityScore.num == 14 || AbilityScore.num == 15) AbilityModifier.num = 2;
        if(AbilityScore.num == 16 || AbilityScore.num == 17) AbilityModifier.num = 3;
        if(AbilityScore.num == 18 || AbilityScore.num == 19) AbilityModifier.num = 4;
        if(AbilityScore.num == 20 || AbilityScore.num == 21) AbilityModifier.num = 5;
        if(AbilityScore.num == 22 || AbilityScore.num == 23) AbilityModifier.num = 6;
        if(AbilityScore.num == 24 || AbilityScore.num == 25) AbilityModifier.num = 7;
        if(AbilityScore.num == 26 || AbilityScore.num == 27) AbilityModifier.num = 8;
        if(AbilityScore.num == 28 || AbilityScore.num == 29) AbilityModifier.num = 9;
        if(AbilityScore.num == 30) AbilityModifier.num = 10;
    }
}
