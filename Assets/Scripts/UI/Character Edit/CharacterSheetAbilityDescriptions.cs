using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class CharacterSheetAbilityDescriptions : MonoBehaviour {
    public Transform AbilityScrollArea;
    public Transform ObjectPortal;

    public void OpenDescription(string ability){
        AbilityScrollArea.parent.gameObject.SetActive(false);
        transform.Find(ability).gameObject.SetActive(true);
    }

    public void Discard(string ability){
        if(TransitionManager.instance.InTheNetwork){
            AbilityCard DiscardedAbility = PhotonNetwork.Instantiate("Objects/AbilityCard", PortableCharacterPanelManager.instance.CurrentPortableCharacterPanel.transform.position, Quaternion.identity).GetComponent<AbilityCard>();
            DiscardedAbility.UpdateCard(ability, AbilityScrollArea.Find(ability).Find("Text").GetComponent<TMPro.TextMeshProUGUI>().text, AbilityScrollArea.Find(ability).GetComponent<DiceAbilityRoller>().type);

            CharacterOptions.instance.equipment.Remove(ability);
            if(TheCollection.instance.AbilityDescriptions.transform.Find(ability).GetComponent<Artifact>() != null) TheCollection.instance.AbilityDescriptions.transform.Find(ability).GetComponent<Artifact>().Remove();
            CharacterOptions.instance.UpdateSheet();
            AbilityScrollArea.parent.gameObject.SetActive(true);
            transform.Find(ability).gameObject.SetActive(false);
        }
    }

    public void OneTimeUse(string ability){
        if(TransitionManager.instance.InTheNetwork){
            AbilityScrollArea.Find(ability).GetComponent<DiceAbilityRoller>().Roll();

            CharacterOptions.instance.equipment.Remove(ability);
            if(TheCollection.instance.AbilityDescriptions.transform.Find(ability).GetComponent<Artifact>() != null) TheCollection.instance.AbilityDescriptions.transform.Find(ability).GetComponent<Artifact>().Remove();
            CharacterOptions.instance.UpdateSheet();
            AbilityScrollArea.parent.gameObject.SetActive(true);
            transform.Find(ability).gameObject.SetActive(false);
        }
    }

    public void MultiTimeUse(string ability){
        if(TransitionManager.instance.InTheNetwork){
            AbilityScrollArea.Find(ability).GetComponent<DiceAbilityRoller>().Roll();
        }
    }

    public void ProduceCard(string ability){
        if(TransitionManager.instance.InTheNetwork){
            AbilityCard Ability = PhotonNetwork.Instantiate("Objects/AbilityCard", ObjectPortal.position, Quaternion.Euler(ObjectPortal.GetComponent<ObjectPortal>().q_rotation.x, ObjectPortal.GetComponent<ObjectPortal>().q_rotation.y, ObjectPortal.GetComponent<ObjectPortal>().q_rotation.z)).GetComponent<AbilityCard>();
            Ability.UpdateCard(ability, AbilityScrollArea.Find(ability).Find("Text").GetComponent<TMPro.TextMeshProUGUI>().text, AbilityScrollArea.Find(ability).GetComponent<DiceAbilityRoller>().type);
        }
    }
}
