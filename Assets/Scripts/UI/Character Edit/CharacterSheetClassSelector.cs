using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSheetClassSelector : MonoBehaviour {
    private string[] Classes = {"Fighter", "Cleric", "Rogue", "Wizard"};
    public GameObject Class_Fighter;
    public GameObject Class_Cleric;
    public GameObject Class_Rogue;
    public GameObject Class_Wizard;
    [HideInInspector] public Dictionary<string,GameObject> ClassObject;

    void Awake(){

        ClassObject = new Dictionary<string, GameObject>();
        ClassObject.Add("Fighter", Class_Fighter);
        ClassObject.Add("Cleric", Class_Cleric);
        ClassObject.Add("Rogue", Class_Rogue);
        ClassObject.Add("Wizard", Class_Wizard);

        foreach(string Class_Name in Classes){
            if(Class_Name == CharacterOptions.instance.selected_class_name) ClassObject[Class_Name].SetActive(true);
            else ClassObject[Class_Name].SetActive(false);
        }
    }

    public void UpdateStatSheet(){
        foreach(string Class_Name in Classes){
            if(Class_Name == CharacterOptions.instance.selected_class_name){
                ClassObject[Class_Name].SetActive(true);
                ClassObject[Class_Name].GetComponent<CharacterSheetStats>().UpdateSheet();
            }
            else ClassObject[Class_Name].SetActive(false);
        }
    }
}
