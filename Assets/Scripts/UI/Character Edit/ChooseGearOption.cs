using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseGearOption : MonoBehaviour {
    public ChooseGear TheHandler;
    public int ItemIndex;
    public int ListIndex;

    public void SendIt(){
        TheHandler.ChangeGear(ItemIndex, ListIndex);
    }
}
