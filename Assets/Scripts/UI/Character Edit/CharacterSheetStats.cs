using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSheetStats : MonoBehaviour {
    public CharacterSheetCounter HP_Counter;
    public CharacterSheetCounter SP_Counter;
    public CharacterSheetCounter AC_Counter;

    void Awake(){
        if(HP_Counter != null){
            HP_Counter.max_num = CharacterOptions.instance.hp_max;
            HP_Counter.num = CharacterOptions.instance.hp_current;
        }

        if(SP_Counter != null){
            SP_Counter.max_num = CharacterOptions.instance.sp_max;
            SP_Counter.num = CharacterOptions.instance.sp_current;
        }

        if(AC_Counter != null){
            AC_Counter.num = CharacterOptions.instance.ac + CharacterOptions.instance.ac_bonus;
        }
    }

    public void UpdateSheet(){
        if(HP_Counter != null){
            HP_Counter.max_num = CharacterOptions.instance.hp_max;
            HP_Counter.SetConuter(CharacterOptions.instance.hp_current);
        }

        if(SP_Counter != null){
            SP_Counter.max_num = CharacterOptions.instance.sp_max;
            SP_Counter.SetConuter(CharacterOptions.instance.sp_current);
        }

        if(AC_Counter != null){
            AC_Counter.SetConuter(CharacterOptions.instance.ac + CharacterOptions.instance.ac_bonus);
        }
    }

    public void UpdateStats(){
        if(HP_Counter != null){
            CharacterOptions.instance.hp_max = HP_Counter.max_num;
            CharacterOptions.instance.hp_current = HP_Counter.num;
        }

        if(SP_Counter != null){
            CharacterOptions.instance.sp_max = SP_Counter.max_num;
            CharacterOptions.instance.sp_current = SP_Counter.num;
        }

        if(AC_Counter != null){
            if(CharacterOptions.instance.abilities.Contains("Mage Armor")){
                int current_ac = 0;
                int ac_threshold_low = (CharacterOptions.instance.abilities.Contains("Abjurer")) ? 1 : 3;
                int ac_threshold_high = (CharacterOptions.instance.abilities.Contains("Abjurer")) ? 4 : 6;
                if(CharacterOptions.instance.sp_current >= ac_threshold_low) current_ac = 1;
                if(CharacterOptions.instance.sp_current >= ac_threshold_high) current_ac = 2;
                CharacterOptions.instance.ac = current_ac + CharacterOptions.instance.ac_bonus;
                AC_Counter.SetConuter(current_ac + CharacterOptions.instance.ac_bonus);
            }
            else CharacterOptions.instance.ac = AC_Counter.num + CharacterOptions.instance.ac_bonus;
        }
    }
}
