using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceAbilityRoller : MonoBehaviour {
    public CharacterSheetRollManager rollManager;
    public CharacterSheetAbilityDescriptions abilityDescriptions;
    public string print_name;
    
    //add option: chosen color

    [Header ("Roll Info")]
    public string die;
    public int number;
    public string type;
    public bool separate;

    [Header ("Sounds")]
    public AudioClip[] Clips;
    public float volume_mod;

    public void Roll(){
        if(rollManager != null){
            if(number > 0){
                int modifier = 0;
                string header = TransitionManager.instance.CharacterName + " rolled for " + print_name + ":";

                if(type.Contains("melee")){
                    if(CharacterOptions.instance.abilities.Contains("Brawler")) modifier += 2;
                    header = TransitionManager.instance.CharacterName + " attacked with their " + print_name + ":";
                }

                if(type.Contains("ranged")){
                    if(CharacterOptions.instance.abilities.Contains("Sharpshooter")) modifier += 2;
                    header = TransitionManager.instance.CharacterName + " attacked with their " + print_name + ":";
                }

                if(type.Contains("spell")){
                    if(CharacterOptions.instance.abilities.Contains("Focused")) modifier += 2;
                    header = TransitionManager.instance.CharacterName + " casted " + print_name + ":";
                }

                if(type.Contains("support")){
                    header = TransitionManager.instance.CharacterName + " casted " + print_name + ":";
                }

                if(type.Contains("potion")){
                    header = TransitionManager.instance.CharacterName + " drank a " + print_name + ":";
                }

                if(PlayerPrefs.GetString("DiceColorOption") != "multicolor"){
                    string color = PlayerPrefs.GetString("DiceColorOption");
                    if (color == "random") color = DiceRoller.instance.AllColors[Random.Range(0, DiceRoller.instance.AllColors.Length)];
                    rollManager.Roll(die, color, modifier, number, separate, header);
                }
                else {
                    rollManager.PrepareRoll();
                    for(int i=0; i<number; i++){
                        string color = DiceRoller.instance.AllColors[Random.Range(0, DiceRoller.instance.AllColors.Length)];
                        rollManager.AddToRoll(die, color, 1);
                    }

                    rollManager.CommitRoll(modifier, separate, header);
                }
            }

            if(Clips.Length > 0){
                AudioClip randomClip = Clips[Random.Range(0, Clips.Length)];
                float randomPitch = Random.Range(0.7f, 1.3f);
                SoundManager.instance.PlaySound(randomClip, transform.position, randomPitch, volume_mod);
            }
        }
    }

    public void OpenDescription(){
        abilityDescriptions.OpenDescription(gameObject.name);
    }
}
