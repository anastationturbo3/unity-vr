using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class AbilityCard : MonoBehaviour {
    private PhotonView photonView;

    public string AbilityName;
    public TMPro.TextMeshProUGUI AbilityNameText;
    public GameObject SymbolMelee;
    public GameObject SymbolRanged;
    public GameObject SymbolSpell;
    public GameObject SymbolArtifact;
    public GameObject SymbolPotion;
    
    void Awake(){
        photonView = GetComponent<PhotonView>();
    }

    public void AddAbility(){
        if(!CharacterOptions.instance.equipment.Contains(AbilityName) && TransitionManager.instance.InTheNetwork){
            CharacterOptions.instance.equipment.Add(AbilityName);
            if(TheCollection.instance.AbilityDescriptions.transform.Find(AbilityName).GetComponent<Artifact>() != null) TheCollection.instance.AbilityDescriptions.transform.Find(AbilityName).GetComponent<Artifact>().Gain();
            CharacterOptions.instance.UpdateSheet();
            PhotonNetwork.Destroy(this.gameObject);
        }
    }

    public void UpdateCard(string name, string text, string type){
        photonView.RPC("UpdateCardRPC", RpcTarget.All, name, text, type);
    }

    [PunRPC]
    private void UpdateCardRPC(string name, string text, string type){

        AbilityName = name;
        AbilityNameText.SetText(text);
        if(type.Contains("melee")) SymbolMelee.SetActive(true);
        else if(type.Contains("ranged")) SymbolRanged.SetActive(true);
        else if(type.Contains("spell")) SymbolSpell.SetActive(true);
        else if(type.Contains("artifact")) SymbolArtifact.SetActive(true);
        else if(type.Contains("potion")) SymbolPotion.SetActive(true);
    }
}