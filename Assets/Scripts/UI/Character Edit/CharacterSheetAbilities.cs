using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSheetAbilities : MonoBehaviour {
    void OnEnable(){
        UpdateAbilities();
    }

    public void UpdateAbilities(){
        foreach(Transform Ability in transform.Find("Scroll Area").transform){
            if(CharacterOptions.instance.equipment.Contains(Ability.name)) Ability.gameObject.SetActive(true);
            else Ability.gameObject.SetActive(false);
        }
    }
}
