using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using Photon.Pun;

public class AbilityDescription : MonoBehaviour {
    public CharacterSheetAbilityDescriptions AbilityDescriptions;
    public bool DualSummon;

    public void Discard(){
        AbilityDescriptions.Discard(gameObject.name);
        DestroySummoned();
    }

    public void OneTimeUse(){
        AbilityDescriptions.OneTimeUse(gameObject.name);
        DestroySummoned();
    }

    public void MultiTimeUse(){
        AbilityDescriptions.MultiTimeUse(gameObject.name);
    }

    public void ProduceCard(){
        AbilityDescriptions.ProduceCard(gameObject.name);
    }

    // ~~

    public GameObject Summonable;
    [HideInInspector] public GameObject CurrentSummoned;
    [HideInInspector] public GameObject CurrentSummoned_2;

    public void Summon(){
        if(!DualSummon){
            XRRayInteractor Hand = XRholder.instance.GetComponent<XRGrabManager>().lastHand;
            AbilityDescription CollectionObject = TheCollection.instance.AbilityDescriptions.transform.Find(gameObject.name).GetComponent<AbilityDescription>();
            
            if(CollectionObject.CurrentSummoned == null){
                if(!TransitionManager.instance.InTheNetwork) CollectionObject.CurrentSummoned = Instantiate(CollectionObject.Summonable, Hand.gameObject.transform.position, Quaternion.identity);
                else CollectionObject.CurrentSummoned = PhotonNetwork.Instantiate("Objects/" + CollectionObject.Summonable.name, Hand.gameObject.transform.position, Quaternion.identity);
            }

            if(Hand.selectTarget != null){
                Hand.interactionManager.SelectExit(Hand, Hand.selectTarget);
            }

            Hand.interactionManager.SelectEnter(Hand, CollectionObject.CurrentSummoned.GetComponent<XRBaseInteractable>());
        }
        else{
            XRholder.instance.GetComponent<XRGrabManager>().lastHand = XRholder.instance.vrLeftArm.GetComponent<XRRayInteractor>();
            XRRayInteractor Hand = XRholder.instance.GetComponent<XRGrabManager>().lastHand;
            AbilityDescription CollectionObject = TheCollection.instance.AbilityDescriptions.transform.Find(gameObject.name).GetComponent<AbilityDescription>();
            
            if(CollectionObject.CurrentSummoned == null){
                if(!TransitionManager.instance.InTheNetwork) CollectionObject.CurrentSummoned = Instantiate(CollectionObject.Summonable, Hand.gameObject.transform.position, Quaternion.identity);
                else CollectionObject.CurrentSummoned = PhotonNetwork.Instantiate("Objects/" + CollectionObject.Summonable.name, Hand.gameObject.transform.position, Quaternion.identity);
            }

            if(Hand.selectTarget != null){
                Hand.interactionManager.SelectExit(Hand, Hand.selectTarget);
            }

            Hand.interactionManager.SelectEnter(Hand, CollectionObject.CurrentSummoned.GetComponent<XRBaseInteractable>());

            XRholder.instance.GetComponent<XRGrabManager>().lastHand = XRholder.instance.vrRightArm.GetComponent<XRRayInteractor>();
            Hand = XRholder.instance.GetComponent<XRGrabManager>().lastHand;
            
            if(CollectionObject.CurrentSummoned_2 == null){
                if(!TransitionManager.instance.InTheNetwork) CollectionObject.CurrentSummoned_2 = Instantiate(CollectionObject.Summonable, Hand.gameObject.transform.position, Quaternion.identity);
                else CollectionObject.CurrentSummoned_2 = PhotonNetwork.Instantiate("Objects/" + CollectionObject.Summonable.name, Hand.gameObject.transform.position, Quaternion.identity);
            }

            if(Hand.selectTarget != null){
                Hand.interactionManager.SelectExit(Hand, Hand.selectTarget);
            }

            Hand.interactionManager.SelectEnter(Hand, CollectionObject.CurrentSummoned_2.GetComponent<XRBaseInteractable>());
        }
    }

    public void DestroySummoned(){
        AbilityDescription CollectionObject = TheCollection.instance.AbilityDescriptions.transform.Find(gameObject.name).GetComponent<AbilityDescription>();
        if(CollectionObject.CurrentSummoned != null){
            if(!TransitionManager.instance.InTheNetwork) Destroy(CollectionObject.CurrentSummoned);
            else PhotonNetwork.Destroy(CollectionObject.CurrentSummoned);
        }
        if(CollectionObject.CurrentSummoned_2 != null){
            if(!TransitionManager.instance.InTheNetwork) Destroy(CollectionObject.CurrentSummoned_2);
            else PhotonNetwork.Destroy(CollectionObject.CurrentSummoned_2);
        }
    }
}
