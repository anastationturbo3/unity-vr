using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSheetResummon : MonoBehaviour {
    public Transform RestingPostition;

    public void BackToYourPlace(){
        if(TransitionManager.instance.CharacterSheet != null)
            TransitionManager.instance.CharacterSheet.transform.position = RestingPostition.position;
    }

    public void BackToYourPlaceLock(){
        if(TransitionManager.instance.CharacterSheet != null){
            TransitionManager.instance.CharacterSheet.transform.position = RestingPostition.position;
            //TransitionManager.instance.CharacterSheet.GetComponent<CharacterSheetManager>().Lock();
        }
    }
}
