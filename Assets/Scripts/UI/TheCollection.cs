using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TheCollection : MonoBehaviour {
    public static TheCollection instance = null;
    public CharacterSheetAbilityDescriptions AbilityDescriptions;

    void Awake(){
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }
}
