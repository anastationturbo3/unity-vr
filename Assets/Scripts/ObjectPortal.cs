using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class ObjectPortal : MonoBehaviour  {
    public bool networked;
    public string network_folder;
    public Vector3 q_rotation;

    public void SpawnObject(GameObject obj){
        if(networked) PhotonNetwork.Instantiate(network_folder + "/" + obj.name, transform.position, Quaternion.Euler(q_rotation.x, q_rotation.y, q_rotation.z));
        else Instantiate(obj, transform.position, Quaternion.Euler(q_rotation.x, q_rotation.y, q_rotation.z));
    }
}