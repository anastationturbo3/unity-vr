using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using Photon.Pun;
using Photon.Realtime;

public class NetworkVRrig : MonoBehaviour {
    public bool bodyReverseRotation;
    public bool body90DegreeRotation;
    public bool body90DegreeSideRotation;

    public VRmap head;
    public VRmap leftHand;
    public VRmap rightHand;

    public Transform headConstraint;
    public Vector3 headBodyOffset;

    private PhotonView photonView;

    void Awake() {
        if(transform.parent.GetComponent<PlayerManager>() == null) headBodyOffset = transform.position - headConstraint.position;
        photonView = transform.parent.GetComponent<PhotonView>();
    }

    void Update() {
        if(photonView.IsMine){
            transform.position = headConstraint.position + headBodyOffset;
            if(bodyReverseRotation) transform.forward = Vector3.ProjectOnPlane(-headConstraint.up, Vector3.up).normalized;
            else if(body90DegreeRotation) transform.forward = Vector3.ProjectOnPlane(headConstraint.forward, Vector3.up).normalized;
            else if(body90DegreeSideRotation) transform.forward = Vector3.ProjectOnPlane(-headConstraint.right, Vector3.up).normalized;
            else transform.forward = Vector3.ProjectOnPlane(headConstraint.up, Vector3.up).normalized;
            
            head.Map();
            leftHand.Map();
            rightHand.Map();
        }
    }
}