using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

public class NetworkPlayerSpawner : MonoBehaviourPunCallbacks {
    public PlayerSynchronizer playerSynchronizer;
    public MusicPlayer musicPlayer;
    public TeleporterChairManager teleporterChairManager;

    private GameObject Player;
    private GameObject CharacterSheet;

    [HideInInspector]public GameObject CharacterSheet_prefab;
    [HideInInspector] public PlayerManager playerManager;

    public override void OnJoinedRoom(){
        base.OnJoinedRoom();
        Debug.Log("Creating New Player");

        if(TransitionManager.instance.isGM){
            Player = PhotonNetwork.Instantiate("GM", transform.position, transform.rotation);
            DontDestroyOnLoad(Player);
        }
        else{
            Player = PhotonNetwork.Instantiate("Player", transform.position, transform.rotation);
            playerManager = Player.GetComponent<PlayerManager>();
            XRholder.instance.GetComponent<XRGrabManager>().StartingHeldItems();
            DontDestroyOnLoad(Player);

            /*
            CharacterSheet = TransitionManager.instance.CharacterSheet;
            CharacterSheet.GetComponent<CharacterSheetManager>().Lock();
            CharacterSheet.transform.position = GameObject.Find("XR Rig").transform.position + new Vector3(0, -0.25f, 0.75f);
            CharacterSheet.transform.rotation = GameObject.Find("XR Rig").transform.rotation;
            CharacterSheet.transform.Rotate(new Vector3(0,-90,0));
            CharacterSheet.GetComponent<XRGrabNetworkInteractable>().interactionManager = GameObject.Find("XR Interaction Manager").GetComponent<XRInteractionManager>();
            */
        }

        playerSynchronizer.UpdateAvatar();
        teleporterChairManager.VerifyTakenChairs();
        musicPlayer.PlayCurrentSong();
        VoiceManager.instance.ChangeVoiceVolume(VoiceManager.instance.CurrentVolume);
    }

    public override void OnLeftRoom(){
        base.OnLeftRoom();
        PhotonNetwork.Destroy(Player);
        //Destroy(CharacterSheet);
    }
}
