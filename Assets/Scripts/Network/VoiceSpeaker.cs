using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoiceSpeaker : MonoBehaviour {
    public AudioSource voiceSource;

    void Awake(){
        voiceSource.volume = VoiceManager.instance.CurrentVolume;
        VoiceManager.instance.Speakers.Add(this);
    }

    public void ChangeVoiceVolume(float Volume){
        voiceSource.volume = Volume;
    }
}
