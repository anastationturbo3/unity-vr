using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

[System.Serializable]
public class DefaultRoom {
    public string name;
    public int sceneIndex;
    public int maxPlayers;
}

public class NetworkManager : MonoBehaviourPunCallbacks {
    public GameObject MenuConnecting;
    public GameObject MenuMain;
    public GameObject MenuWait;

    void Awake(){
        ConnectToServer();
    }

    public void ConnectToServer(){
        PhotonNetwork.ConnectUsingSettings();
        //Debug.Log
    }

    public override void OnConnectedToMaster(){
        base.OnConnectedToMaster();
        Debug.Log("Connected.");

        MenuConnecting.SetActive(false);
        MenuMain.SetActive(true);

        PhotonNetwork.JoinLobby();
    }

    public override void OnJoinedLobby(){
        base.OnJoinedLobby();
        Debug.Log("Joined Lobby.");
    }

    public override void OnJoinedRoom(){
        base.OnJoinedRoom();
        Debug.Log("Joined Room.");
    }

    public override void OnPlayerEnteredRoom(Player newPlayer){
        Debug.Log("New Player in Room.");
        base.OnPlayerEnteredRoom(newPlayer);
    }

    private void InitializeRoom(string roomName, string sceneName, int maxPlayers){
        //Load Scene
        PhotonNetwork.LoadLevel(sceneName);

        //Room Settings
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = (byte)maxPlayers;
        roomOptions.IsVisible = true;
        roomOptions.IsOpen = true;

        PhotonNetwork.JoinOrCreateRoom(roomName, roomOptions, TypedLobby.Default);
    }

    public void GameTime(){
        MenuMain.SetActive(false);
        MenuWait.SetActive(true);

        if(!TransitionManager.instance.isGM){
            if(TransitionManager.instance.CharacterSheet != null) DontDestroyOnLoad(TransitionManager.instance.CharacterSheet);
            TransitionManager.instance.SetPlayerAvatar();
        }

        TransitionManager.instance.InTheNetwork = true;
        InitializeRoom("GameTime", "Photon Test", 15);
    }
}
