using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class NetworkDiceManager : MonoBehaviour {
    public static NetworkDiceManager instance = null;
    public DiceRoller diceRoller;

    void Awake(){
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        diceRoller = GameObject.Find("Dice Manager").GetComponent<DiceRoller>();
    }

    public void ClearAllDice(){
        GetComponent<PhotonView>().RPC("ClearAllDiceRPC", RpcTarget.All);
    }

    [PunRPC]
    public void ClearAllDiceRPC() {
        diceRoller.ClearAllDice();
        DiceplateHolder.instance.ClearAllDiceplates();
    }
}
