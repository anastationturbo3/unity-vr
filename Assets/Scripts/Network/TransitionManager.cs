using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionManager : MonoBehaviour {
    public static TransitionManager instance = null;
    public GameObject CharacterSheet;
    public bool InTheNetwork;

    [HideInInspector] public bool isGM;
    [HideInInspector] public Dictionary<string,int> ComponentChoices;
    [HideInInspector] public Dictionary<string,int> ClothingChoices;
    [HideInInspector] public Dictionary<string,int> MaterialChoices;
    [HideInInspector] public string CharacterName;
    [HideInInspector] public string LeftHandHold;
    [HideInInspector] public string RightHandHold;

    void Awake(){
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
        isGM = false;
        InTheNetwork = false;
        CharacterName = "";
    }

    public void SetPlayerAvatar(){
        ComponentChoices = new Dictionary<string, int>(AvatarChoices.instance.Chosen_AvatarComponentOptions);
        ClothingChoices = new Dictionary<string, int>(AvatarChoices.instance.Chosen_ClothingOptions);
        MaterialChoices = new Dictionary<string, int>(AvatarChoices.instance.Chosen_AvatarMaterialOptions);

        LeftHandHold = CharacterOptions.instance.selected_class.held_left;
        RightHandHold = CharacterOptions.instance.selected_class.held_right;
    }

    public void UpdateSheet(GameObject newSheet){
        CharacterSheet = newSheet;
    }

    public void SetGM(bool setGM){
       isGM = setGM;
       if(isGM) CharacterName = "The Dungeon Master";
    }

    public void UpdateName(string newName){
        CharacterName = newName;
        if(PortableCharacterPanelManager.instance.CurrentPortableCharacterPanel != null)
            PortableCharacterPanelManager.instance.CurrentPortableCharacterPanel.GetComponent<PortableCharacterPanel>().SheetUpdate();
    }
}
