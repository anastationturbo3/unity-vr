using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoiceManager : MonoBehaviour {
    public static VoiceManager instance = null;

    [HideInInspector] public List<VoiceSpeaker> Speakers;
    [HideInInspector] public float CurrentVolume;
    [HideInInspector] public float SetVolume {
        get {
            return CurrentVolume;
        }
        set {
            if(value == CurrentVolume) return;
            CurrentVolume = value;
            ChangeVoiceVolume(CurrentVolume);
        } 
    }

    void Awake(){
        DontDestroyOnLoad(this.gameObject);
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        CurrentVolume = PlayerPrefs.GetFloat("VoiceVolume");
    }

    public void ChangeVoiceVolume(float Volume){
        foreach(VoiceSpeaker speaker in Speakers)
            if(speaker != null)
                speaker.ChangeVoiceVolume(Volume);
    }
}
