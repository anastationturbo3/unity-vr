using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerSynchronizer : MonoBehaviour {
    private PhotonView photonView;
    public NetworkPlayerSpawner playerSpawner;

    void Awake(){
        photonView = GetComponent<PhotonView>();
    }

    public void UpdateAvatar() {
        photonView.RPC("UpdateAvatarRPC", RpcTarget.All);
    }

    [PunRPC]
    public void UpdateAvatarRPC() {
        if(playerSpawner.playerManager != null){
            playerSpawner.playerManager.AvatarRPC();
        }
    }
}
