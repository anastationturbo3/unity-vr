using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionsManager : MonoBehaviour {
    public static OptionsManager instance = null;

    void Awake(){
        DontDestroyOnLoad(this.gameObject);
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        if(PlayerPrefs.GetInt("InitialStetup", 0) == 0){
            PlayerPrefs.SetFloat("PanelAlphaOption", 0.75f);
            PlayerPrefs.SetInt("NameplateOption", 1);
            PlayerPrefs.SetFloat("NameplateScaleOption", 0.05f);
            PlayerPrefs.SetFloat("NameplateAlphaOption", 0.5f);
            PlayerPrefs.SetInt("DiceplateOption", 1);
            PlayerPrefs.SetFloat("DiceplateScaleOption", 0.05f);
            PlayerPrefs.SetFloat("DiceplateAlphaOption", 0.5f);
            PlayerPrefs.SetInt("StickyHandsOption", 1);
            PlayerPrefs.SetInt("DiceHandOption", 1);
            PlayerPrefs.SetInt("StatblockAutoLock", 1);
            PlayerPrefs.SetInt("DisableObjectRotation", 0);
            PlayerPrefs.SetFloat("MusicVolume", .5f);
            PlayerPrefs.SetFloat("SoundVolume", .5f);
            PlayerPrefs.SetFloat("VoiceVolume", 1f);
            PlayerPrefs.SetInt("DiceColorOptionIndex", 0);
            PlayerPrefs.SetString("DiceColorOption", "black");
            PlayerPrefs.SetInt("InitialStetup", 1);
        }
    }

    public void DiceColorOption_check(int index, string color){
        PlayerPrefs.SetInt("DiceColorOptionIndex", index);
        PlayerPrefs.SetString("DiceColorOption", color.ToLower());
    }

    public void PanelAlpha_check(float value){
        if(PortableCharacterPanelManager.instance != null) PortableCharacterPanelManager.instance.SetPanelAlpha = value;
        PlayerPrefs.SetFloat("PanelAlphaOption", value);
    }

    public void OptionPanelAlpha_check(float value){
        if(GMOptionPanelManager.instance != null) GMOptionPanelManager.instance.SetOptionPanelAlpha = value;
        PlayerPrefs.SetFloat("PanelAlphaOption", value);
    }

    public void NameplatesActive_check(bool state){
        if(NameplateHolder.instance != null) NameplateHolder.instance.SetNameplates = state;
        PlayerPrefs.SetInt("NameplateOption", state ? 1 : 0);
    }

    public void NameplateScale_check(float value){
        if(NameplateHolder.instance != null) NameplateHolder.instance.SetNameplateScale = value;
        PlayerPrefs.SetFloat("NameplateScaleOption", value);
    }

    public void NameplateAlpha_check(float value){
        if(NameplateHolder.instance != null) NameplateHolder.instance.SetNameplateAlpha = value;
        PlayerPrefs.SetFloat("NameplateAlphaOption", value);
    }

    public void DiceplatesActive_check(bool state){
        if(DiceplateHolder.instance != null) DiceplateHolder.instance.SetDiceplates = state;
        PlayerPrefs.SetInt("DiceplateOption", state ? 1 : 0);
    }

    public void DiceplateScale_check(float value){
        if(DiceplateHolder.instance != null) DiceplateHolder.instance.SetDiceplateScale = value;
        PlayerPrefs.SetFloat("DiceplateScaleOption", value);
    }

    public void DiceplateAlpha_check(float value){
        if(DiceplateHolder.instance != null) DiceplateHolder.instance.SetDiceplateAlpha = value;
        PlayerPrefs.SetFloat("DiceplateAlphaOption", value);
    }

    public void DiceplateStacking_check(float value){
        PlayerPrefs.SetFloat("DiceplateStacking", value);
    }

    public void DiceHandOption_check(int hand){
        if(hand < 0 || hand > 1) return;
        if(DiceHandOption.instance != null) DiceHandOption.instance.SetDiceHand = hand;
        PlayerPrefs.SetInt("DiceHandOption", hand);
    }

    public void StickyhandsActive_check(bool state){
        if(StickyHandsController.instance != null) StickyHandsController.instance.SetStickyhands = state;
        PlayerPrefs.SetInt("StickyHandsOption", state ? 1 : 0);
    }

    public void StatblockAutoLock_check(bool state){
        PlayerPrefs.SetInt("StatblockAutoLock", state ? 1 : 0);
    }

    public void DisableObjectRotation_check(bool state){
        if(ObjectRotationToggle.instance != null) ObjectRotationToggle.instance.SetObjectRotationDisabled = state;
        PlayerPrefs.SetInt("DisableObjectRotation", state ? 1 : 0);
    }

    public void MusicVolume_check(float volume){
        if(MusicManager.instance != null) MusicManager.instance.SetVolume = volume;
        PlayerPrefs.SetFloat("MusicVolume", volume);
    }

    public void SoundVolume_check(float volume){
        if(SoundManager.instance != null) SoundManager.instance.SetVolume = volume;
        PlayerPrefs.SetFloat("SoundVolume", volume);
    }

    public void VoiceVolume_check(float volume){
        if(VoiceManager.instance != null) VoiceManager.instance.SetVolume = volume;
        PlayerPrefs.SetFloat("VoiceVolume", volume);
    }
}
