using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class GrabGive : MonoBehaviour{
    private XRDirectInteractor Holder;
    public XRRayInteractor otherHolder;

    void Awake(){
        Holder = GetComponent<XRDirectInteractor>();
    }

    void Update(){
        if(Holder.selectTarget != null){
            XRBaseInteractable target = Holder.selectTarget;
            Holder.allowSelect = false;
            otherHolder.interactionManager.SelectEnter(otherHolder, target);

            //Holder.startingSelectedInteractable
            //m_InteractionManager.SelectEnter(this, (IXRSelectInteractable)m_StartingSelectedInteractable);
        }
        else if(otherHolder.selectTarget != null){
            Holder.allowSelect = false;
        }
        else{
            Holder.allowSelect = true;
        }
    }
}
