using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Animations.Rigging;

public class PlayerRigSetup : MonoBehaviour {
    void Start(){
        NetworkVRrig rig = GetComponent<NetworkVRrig>();
        Transform vrConstraints = transform.Find("Human").Find("VR Constraints");
        rig.head.rigTarget = vrConstraints.Find("Head Contraint");
        rig.leftHand.rigTarget = vrConstraints.Find("RightArmIK").Find("Target");
        rig.rightHand.rigTarget = vrConstraints.Find("LeftArmIK").Find("Target");
        rig.headConstraint = vrConstraints.Find("Head Contraint");

        GetComponent<RigBuilder>().layers.Clear();
        //GetComponent<RigBuilder>().layers.Add(transform.Find("Human").Find("VR Constraints").GetComponent<Rig>());
        GetComponent<RigBuilder>().Build();
    }
}
