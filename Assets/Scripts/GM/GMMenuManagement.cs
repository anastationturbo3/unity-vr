using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GMMenuManagement : MonoBehaviour {
    public GameObject startingScreen;
    GameObject currentScreen;

    void Awake(){
        currentScreen = startingScreen;
    }

    public void ChangeScreen(GameObject newScreen){
        currentScreen.SetActive(false);
        newScreen.SetActive(true);
        currentScreen = newScreen;
    }
}