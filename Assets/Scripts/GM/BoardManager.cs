using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class BoardManager : MonoBehaviour {
    private PhotonView photonView;
    public GameObject TheBoard;

    void Awake(){
        photonView = GetComponent<PhotonView>();
    }

    public void BoardToggle(){
        photonView.RPC("BoardToggleRPC", RpcTarget.All, !TheBoard.activeInHierarchy);
    }

    [PunRPC]
    public void BoardToggleRPC(bool active){
        TheBoard.SetActive(active);
    }
}
