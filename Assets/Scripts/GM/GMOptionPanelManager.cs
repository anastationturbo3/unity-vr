using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GMOptionPanelManager : MonoBehaviour {
    public static GMOptionPanelManager instance = null;

    public GameObject PortableOptionPanel;
    public GameObject CurrentPortableOptionPanel;

    private float OptionPanelAlpha;
    public float SetOptionPanelAlpha {
        get {
            return OptionPanelAlpha;
        }
        set {
            if(value == OptionPanelAlpha) return;
            OptionPanelAlpha = value;
            Color c = CurrentPortableOptionPanel.GetComponent<PortableCharacterPanel>().Panel.color;
            c.a = OptionPanelAlpha;
            CurrentPortableOptionPanel.GetComponent<PortableCharacterPanel>().Panel.color = c;
        }    
    }

    void Awake(){
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        OptionPanelAlpha = PlayerPrefs.GetFloat("PanelAlphaOption");
    }

    public void Summon(Vector3 pos){
        Vector3 distancedPos = (pos - XRholder.instance.vrHead.transform.position).normalized * .25f + new Vector3(0f, 0.2f, 0f);;
        if(CurrentPortableOptionPanel == null) CurrentPortableOptionPanel = Instantiate(PortableOptionPanel, pos + distancedPos, Quaternion.identity);
        else CurrentPortableOptionPanel.transform.position = pos + distancedPos;

        Color c = CurrentPortableOptionPanel.GetComponent<PortableCharacterPanel>().Panel.color;
        c.a = OptionPanelAlpha;
        CurrentPortableOptionPanel.GetComponent<PortableCharacterPanel>().Panel.color = c;
    }
}
