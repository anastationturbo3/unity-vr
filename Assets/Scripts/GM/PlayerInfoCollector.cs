using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class PlayerInfoCollector : MonoBehaviour {
    private PhotonView photonView;
    private string InspectionName;
    public GameObject PlayerInfo;
    public GameObject PlayerInfoMenu;
    public GameObject PlayerInfoDetails;

    [Header("Player Info List")]
    public GameObject ScrollArea;

    [Header("Player Info Details")]
    public TMPro.TextMeshProUGUI DetailsText_1;
    public TMPro.TextMeshProUGUI DetailsText_2;

    void Awake(){
        photonView = GetComponent<PhotonView>();
    }

    // ~~

    public void GatherPlayerNames(){
        foreach (Transform child in ScrollArea.transform) Destroy(child.gameObject);
        photonView.RPC("SendNameRPC", RpcTarget.All);
    }

    public void SetPlayerInspectionName(string name){
        InspectionName = name;
        PlayerInfoMenu.SetActive(false);
        PlayerInfoDetails.SetActive(true);
    }

    public void GatherPlayerDetails(){
        DetailsText_1.text = "";
        DetailsText_2.text = "";
        photonView.RPC("SendDetailsRPC", RpcTarget.All, InspectionName);
    }

    // ~~

    [PunRPC]
    public void SendNameRPC(){
        if(!TransitionManager.instance.isGM){
            photonView.RPC("GatherNameRPC", RpcTarget.All, TransitionManager.instance.CharacterName);
        }
    }

    [PunRPC]
    public void GatherNameRPC(string name){
        if(TransitionManager.instance.isGM){
            GameObject newPlayerInfo = Instantiate(PlayerInfo, ScrollArea.transform);
            newPlayerInfo.GetComponent<PlayerInfo>().NameText.text = name;
        }
    }

    [PunRPC]
    public void SendDetailsRPC(string name){
        if(!TransitionManager.instance.isGM && TransitionManager.instance.CharacterName == name){
            photonView.RPC("GatherDetailsRPC", RpcTarget.All, TransitionManager.instance.CharacterName,
                                                              CharacterOptions.instance.selected_class_name,
                                                              CharacterOptions.instance.hp_max,
                                                              CharacterOptions.instance.hp_current,
                                                              CharacterOptions.instance.sp_max,
                                                              CharacterOptions.instance.sp_current,
                                                              CharacterOptions.instance.ac + CharacterOptions.instance.ac_bonus);
        }
    }

    [PunRPC]
    public void GatherDetailsRPC(string name, string charatcer_class, int hp_max, int hp, int sp_max, int sp, int ac){
        if(TransitionManager.instance.isGM){
            string line_1_1 = "Name: " + name;
            string line_1_2 = "Class: " + charatcer_class;
            string line_1_3 = "HP: " + hp.ToString() + "/" + hp_max.ToString();
            string line_1_4 = "AC: " + ac.ToString();
            string line_2_1 = "";
            string line_2_2 = "";
            if(sp_max > 0) line_2_2 = "SP: " + sp.ToString() + "/" + sp_max.ToString();
            string line_2_3 = "";
            string line_2_4 = "";

            DetailsText_1.text = line_1_1 + "\n" + line_1_2 + "\n" + line_1_3 + "\n" + line_1_4;
            DetailsText_2.text = line_2_1 + "\n" + line_2_2 + "\n" + line_2_3 + "\n" + line_2_4;
        }
    }
}
