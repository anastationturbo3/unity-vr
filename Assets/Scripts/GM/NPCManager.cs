using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class NPCManager : MonoBehaviour {

    public static NPCManager instance = null;
    GameObject currentRig;

    void Awake(){
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    public void ChangeRig(string newRig){
        PhotonView photonView = PhotonView.Get(this);
        //photonView.RPC("ChangeRigPun", RpcTarget.All, newRig.name);

        if(currentRig == null){
            GameObject rig = PhotonNetwork.Instantiate("Rigs/" + newRig, transform.position, transform.rotation);
            rig.transform.SetParent(this.transform);
            currentRig = rig;
        }    
        else if(newRig != currentRig.name){
            //currentRig.GetPhotonView().RequestOwnership();
            PhotonNetwork.Destroy(currentRig.GetPhotonView());
            GameObject rig = PhotonNetwork.Instantiate("Rigs/" + newRig, transform.position, transform.rotation);
            rig.transform.SetParent(this.transform);
            currentRig = rig;
        }
    }
}
