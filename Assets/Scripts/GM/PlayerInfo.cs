using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInfo : MonoBehaviour {
    public TMPro.TextMeshProUGUI NameText;

    public void PrepareDetails(){
        PlayerInfoMenu.instance.PrepareDetails(NameText.text);
    }

    public void AddInitiative(){
        InitiativeTracker.instance.Add(NameText.text);
    }
}
