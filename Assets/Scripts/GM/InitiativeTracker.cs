using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitiativeTracker : MonoBehaviour {
    public static InitiativeTracker instance = null;
    public GameObject InitiativeToken;
    public GameObject InitiativePanel;
    public GameObject Scroller;

    void Awake(){
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        InitiativePanel.SetActive(false);
    }

    public void Add(string name){
        InitiativePanel.SetActive(true);
        for(int i=0; i<Scroller.transform.childCount; i++)
            if(Scroller.transform.GetChild(i).GetComponent<InitiativeToken>().TextHolder.text == name) return;

        GameObject NewInitiative = Instantiate(InitiativeToken, Scroller.transform);
        NewInitiative.GetComponent<InitiativeToken>().TextHolder.text = name;
    }
}
