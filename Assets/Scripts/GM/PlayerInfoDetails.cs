using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInfoDetails : MonoBehaviour {
    public static PlayerInfoDetails instance = null;
    public PlayerInfoCollector playerInfoCollector;

    void Awake(){
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    void OnEnable(){
        GatherPlayerDetails();
    }

    public void GatherPlayerDetails(){
        playerInfoCollector.GatherPlayerDetails();
    }
}
