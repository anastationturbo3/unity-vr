using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InitiativeToken : MonoBehaviour {
    public TMPro.TextMeshProUGUI TextHolder;

    public void Up(){
        if(transform.GetSiblingIndex() > 0) transform.SetSiblingIndex(transform.GetSiblingIndex()-1);
    }

    public void Down(){
        if(transform.GetSiblingIndex() < transform.parent.childCount-1) transform.SetSiblingIndex(transform.GetSiblingIndex()+1);
    }

    public void Remove(){
        if(transform.parent.childCount == 1) InitiativeTracker.instance.InitiativePanel.SetActive(false);
        Destroy(gameObject);
    }
}
