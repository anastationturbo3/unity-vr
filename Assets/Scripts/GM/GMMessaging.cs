using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GMMessaging : MonoBehaviour {
    public GameObject startingRig;

    void Awake(){
        if(TransitionManager.instance.isGM) StartCoroutine(StartSet());
    }

    IEnumerator StartSet(){
        yield return new WaitUntil(() => NPCManager.instance != null);
        NPCManager.instance.ChangeRig(startingRig.name);
    }

    public void NPCChange(GameObject rig){
        NPCManager.instance.ChangeRig(rig.name);
    }
}
