using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInfoMenu : MonoBehaviour {
    public static PlayerInfoMenu instance = null;
    public PlayerInfoCollector playerInfoCollector;

    void Awake(){
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    void OnEnable(){
        GatherPlayerNames();
    }

    public void GatherPlayerNames(){
        playerInfoCollector.GatherPlayerNames();
    }

    public void PrepareDetails(string name){
        playerInfoCollector.SetPlayerInspectionName(name);
    }
}
