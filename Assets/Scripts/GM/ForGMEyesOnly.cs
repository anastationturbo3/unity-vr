using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForGMEyesOnly : MonoBehaviour {
    void Awake(){
        if(!TransitionManager.instance.isGM){
            this.gameObject.SetActive(false);
        }
    }

    void OnEnable(){
        if(!TransitionManager.instance.isGM){
            this.gameObject.SetActive(false);
        }
    }
}
