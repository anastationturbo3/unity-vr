using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GMChairSetup : MonoBehaviour {
    public GameObject GMChair;

    void Awake(){
        if(TransitionManager.instance.isGM){
            transform.position = GMChair.transform.Find("Anchor").transform.position;
            transform.rotation = GMChair.transform.Find("Anchor").transform.rotation;
        }
    }
}
