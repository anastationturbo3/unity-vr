using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class NetworkToggleObject : MonoBehaviour {
    public GameObject Obj;

    public void TobbleObj(){
        bool active = !Obj.activeSelf;
        PhotonView photonView = GetComponent<PhotonView>();
        photonView.RPC("TobbleObjRPC", RpcTarget.All, active);
    }

    [PunRPC]
    public void TobbleObjRPC(bool active){
        Obj.SetActive(active);
    }
}
