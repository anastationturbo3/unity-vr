using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;

public class SoundManager : MonoBehaviour {
    public static SoundManager instance = null;
    public GameObject SoundSource;
    public int max_sounds;

    private List<GameObject> SoundPlayers;
    [HideInInspector] public float CurrentVolume;
    [HideInInspector] public float SetVolume {
        get {
            return CurrentVolume;
        }
        set {
            if(value == CurrentVolume) return;
            CurrentVolume = value;
            for(int i=0; i<SoundPlayers.Count; i++){
                SoundPlayers[i].GetComponent<AudioSource>().volume = CurrentVolume;
            }
        } 
    }

    void Awake(){
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        CurrentVolume = PlayerPrefs.GetFloat("SoundVolume");
        SoundPlayers = new List<GameObject>();
    }

    void FixedUpdate(){
        for(int i=0; i<SoundPlayers.Count; i++){
            if(!SoundPlayers[i].GetComponent<AudioSource>().isPlaying){
                GameObject sp = SoundPlayers[0];
                SoundPlayers.Remove(sp);
                Destroy(sp);
            }
        }
    }

    public void PlaySound(AudioClip clip, Vector3 position, float pitch, float volume_mod){
        if(SoundPlayers.Count >= max_sounds) {
            GameObject sp = SoundPlayers[0];
            sp.GetComponent<AudioSource>().Stop();
            SoundPlayers.Remove(sp);
            Destroy(sp);
        }

        GameObject new_sound = Instantiate(SoundSource, position, Quaternion.identity);
        new_sound.transform.parent = transform;
        new_sound.GetComponent<AudioSource>().clip = clip;
        new_sound.GetComponent<AudioSource>().pitch = pitch;
        new_sound.GetComponent<AudioSource>().volume = CurrentVolume * volume_mod;
        new_sound.GetComponent<AudioSource>().Play();
        SoundPlayers.Add(new_sound);
    }
}
