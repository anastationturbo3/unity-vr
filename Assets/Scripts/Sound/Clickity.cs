using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clickity : MonoBehaviour {
    public AudioClip[] Clips;
    public float volume_mod;

    void OnCollisionEnter(Collision col) {
        AudioClip randomClip = Clips[Random.Range(0, Clips.Length)];
        float randomPitch = Random.Range(0.7f, 1.3f);
        SoundManager.instance.PlaySound(randomClip, transform.position, randomPitch, volume_mod);
    }
}
