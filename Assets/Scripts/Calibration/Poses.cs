using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poses : MonoBehaviour {
    public GameObject RightArmTarget;
    public GameObject LeftArmTarget;
    public GameObject BodyCenter;
    public GameObject RightShoulder;
    public GameObject LeftShoulder;
    public float zBodyOffset;
    public float yBodyOffset;

    public GameObject RightArm;
    public GameObject LeftArm;

    public void T_Pose(){
        RightArmTarget.transform.position = new Vector3(RightShoulder.transform.position.x + 1,
                                                        RightShoulder.transform.position.y,
                                                        RightShoulder.transform.position.z);
        RightArmTarget.transform.rotation = Quaternion.Euler(180,
                                                             0,
                                                             -2.5f);
        LeftArmTarget.transform.position = new Vector3(LeftShoulder.transform.position.x - 1,
                                                       LeftShoulder.transform.position.y,
                                                       LeftShoulder.transform.position.z);
        LeftArmTarget.transform.rotation = Quaternion.Euler(0,
                                                            0,
                                                            2.5f);
    }

    public void Calibration(){
        RightArm.GetComponent<CalibrateArm>().Calibrate();
        LeftArm.GetComponent<CalibrateArm>().Calibrate();
    }

    void Awake(){
        T_Pose();
        //Calibration();
    }
}
