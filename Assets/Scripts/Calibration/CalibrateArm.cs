using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Animations.Rigging;

public class CalibrateArm : MonoBehaviour {
    public GameObject UpperArm;
    public GameObject Forearm;
    public GameObject Wrist;

    public GameObject UpperArm_scaleConstraint;
    public GameObject Forearm_scaleConstraint;
    public GameObject Wrist_scaleConstraint;

    public GameObject Target;
    public GameObject Hint;

    void Awake(){
        ScaleConstraint ConstraintComponent = UpperArm.AddComponent<ScaleConstraint>();
        AddSourceToConstraint(ConstraintComponent, UpperArm_scaleConstraint);
        ConstraintComponent = Forearm.AddComponent<ScaleConstraint>();
        AddSourceToConstraint(ConstraintComponent, Forearm_scaleConstraint);
        ConstraintComponent = Wrist.AddComponent<ScaleConstraint>();
        AddSourceToConstraint(ConstraintComponent, Wrist_scaleConstraint);
    }

    public void Calibrate(){
        float cur_lenght = Mathf.Abs(Forearm.transform.position.x - Wrist.transform.position.x);
        float final_lenght = Mathf.Abs(Forearm.transform.position.x - Target.transform.position.x);

        Debug.Log(Forearm.transform.localScale.x*(final_lenght/cur_lenght));

        // Scale
        //Debug.Log(Forearm.transform.localScale.x*(final_lenght/cur_lenght));
        float scale_factor = Forearm_scaleConstraint.transform.localScale.x*(final_lenght/cur_lenght);
        Forearm_scaleConstraint.transform.localScale = new Vector3(scale_factor,scale_factor,1f+(scale_factor-1f)/2f);
        //Wrist_scaleConstraint.transform.localScale = new Vector3(1f/Forearm_scaleConstraint.transform.localScale.x,1f/Forearm_scaleConstraint.transform.localScale.y,1f/Forearm_scaleConstraint.transform.localScale.z);
    }

    void AddSourceToConstraint(ScaleConstraint constraint, GameObject target){
        ConstraintSource constraintSource = new ConstraintSource();
        constraintSource.sourceTransform = target.transform;
        constraintSource.weight = 1f;
        constraint.AddSource(constraintSource);
        constraint.constraintActive = true;
    }

    /*
    void AddSourceToConstraint(IConstraint constraint, GameObject target){
        ConstraintSource constraintSource = new ConstraintSource();
        constraintSource.sourceTransform = target.transform;
        constraintSource.weight = 1f;
        constraint.AddSource(constraintSource);
    }
    */
}
