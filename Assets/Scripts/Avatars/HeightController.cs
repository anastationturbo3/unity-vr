using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeightController : MonoBehaviour {
    public float max_offset;
    private float cur_offset = 0;
    public void Heighten(float y){
        if((y > 0 && cur_offset < max_offset) || (y < 0 && cur_offset > -max_offset)){
            XRholder.instance.vrRig.transform.position = new Vector3(XRholder.instance.vrRig.transform.position.x, XRholder.instance.vrRig.transform.position.y + y, XRholder.instance.vrRig.transform.position.z);
            cur_offset += y;
        }
    }
}
