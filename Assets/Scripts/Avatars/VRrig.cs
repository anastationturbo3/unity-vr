using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class VRmap{
    public Transform vrTarget;
    public Transform rigTarget;
    public Vector3 trackingPositionOffset;
    public Vector3 trackingRotationOffset;

    public void Map(){
        if(vrTarget != null){
            rigTarget.position = vrTarget.TransformPoint(trackingPositionOffset);
            rigTarget.rotation = vrTarget.rotation * Quaternion.Euler(trackingRotationOffset);
        }
    }
}

public class VRrig : MonoBehaviour {
    public bool bodyReverseRotation;
    public bool body90DegreeRotation;

    public VRmap head;
    public VRmap leftHand;
    public VRmap rightHand;

    public Transform headConstraint;
    public Vector3 headBodyOffset;

    void Awake() {
        headBodyOffset = transform.position - headConstraint.position;
    }

    void Update() {
        transform.position = headConstraint.position + headBodyOffset;
        if(bodyReverseRotation) transform.forward = Vector3.ProjectOnPlane(-headConstraint.up, Vector3.up).normalized;
        else if(body90DegreeRotation) transform.forward = Vector3.ProjectOnPlane(headConstraint.forward, Vector3.up).normalized;
        else transform.forward = Vector3.ProjectOnPlane(headConstraint.up, Vector3.up).normalized;
        
        head.Map();
        leftHand.Map();
        rightHand.Map();
    }
}