using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarOptions : MonoBehaviour {
    private string[] rigs = {"Head", "Torso", "Pants"};
    private HelmetOptions HelmetOptions;

    [Header("Rig Components")]
    public GameObject AvatarHead;
    public GameObject AvatarTorso;
    public GameObject AvatarPants;
    public GameObject AvatarHandRight;
    public GameObject AvatarHandLeft;
    [HideInInspector] public Dictionary<string,GameObject> AvatarComponents;

    [Header("Rig Bones")]
    public Transform BoneHead;
    public Transform BoneTorso;
    public Transform BoneArmRight;
    public Transform BoneForearmRight;
    public Transform BoneArmLeft;
    public Transform BoneForearmLeft;
    public Transform BoneWaist;
    [HideInInspector] public Dictionary<string,Transform> AvatarBones;

    private List<GameObject> ClothingHead;
    private List<GameObject> ClothingTorso;
    private List<GameObject> ClothingWaist;
    [HideInInspector] public Dictionary<string,List<GameObject>> ClothingComponents;

    void Awake(){
        AvatarComponents = new Dictionary<string, GameObject>();
        AvatarComponents.Add("Head", AvatarHead);
        AvatarComponents.Add("Torso", AvatarTorso);
        AvatarComponents.Add("Pants", AvatarPants);
        HelmetOptions = GetComponent<HelmetOptions>();

        AvatarBones = new Dictionary<string, Transform>();
        AvatarBones.Add("Head", BoneHead);
        AvatarBones.Add("Torso", BoneTorso);
        AvatarBones.Add("Arm Right", BoneArmRight);
        AvatarBones.Add("Forearm Right", BoneForearmRight);
        AvatarBones.Add("Arm Left", BoneArmLeft);
        AvatarBones.Add("Forearm Left", BoneForearmLeft);
        AvatarBones.Add("Waist", BoneWaist);

        ClothingHead = new List<GameObject>();
        ClothingTorso = new List<GameObject>();
        ClothingWaist = new List<GameObject>();
        ClothingComponents = new Dictionary<string, List<GameObject>>();
        ClothingComponents.Add("Hat", ClothingHead);
        ClothingComponents.Add("Armor", ClothingTorso);
        ClothingComponents.Add("Waist", ClothingWaist);
    }

    public void ChangeComponent(string target, int option){
        AvatarChoices.instance.Chosen_AvatarComponentOptions[target] = option;

        SkinnedMeshRenderer Renderer = AvatarComponents[target].GetComponent<SkinnedMeshRenderer>();
        Renderer.sharedMesh = AvatarChoices.instance.AvatarComponentOptions[target][option].Mesh;

        string[] material_keys = AvatarChoices.instance.AvatarComponentOptions[target][option].Materials;
        Material[] materials = new Material[material_keys.Length];
        for(int i=0; i<material_keys.Length; i++) materials[i] = AvatarChoices.instance.AvatarMaterialOptions[material_keys[i]][AvatarChoices.instance.Chosen_AvatarMaterialOptions[material_keys[i]]];
        Renderer.sharedMaterials = materials;

        if(target == "Head") HelmetOptions.current_option = option;
    }

    public void ChangeComponentHelm(int option){
        AvatarChoices.instance.Chosen_AvatarComponentOptions["Head"] = option;

        SkinnedMeshRenderer Renderer = AvatarComponents["Head"].GetComponent<SkinnedMeshRenderer>();
        Renderer.sharedMesh = AvatarChoices.instance.AvatarComponentOptions["Head"][option].Mesh;

        string[] material_keys = AvatarChoices.instance.AvatarComponentOptions["Head"][option].Materials;
        Material[] materials = new Material[material_keys.Length];
        for(int i=0; i<material_keys.Length; i++) materials[i] = AvatarChoices.instance.AvatarMaterialOptions[material_keys[i]][AvatarChoices.instance.Chosen_AvatarMaterialOptions[material_keys[i]]];
        Renderer.sharedMaterials = materials;
    }

    public void ChangeMaterial(string target, int option){
        AvatarChoices.instance.Chosen_AvatarMaterialOptions[target] = option;

        foreach(string rig in rigs){
            SkinnedMeshRenderer Renderer = AvatarComponents[rig].GetComponent<SkinnedMeshRenderer>();
            string[] material_keys = AvatarChoices.instance.AvatarComponentOptions[rig][AvatarChoices.instance.Chosen_AvatarComponentOptions[rig]].Materials;
            Material[] materials = new Material[material_keys.Length];
            for(int i=0; i<material_keys.Length; i++) materials[i] = AvatarChoices.instance.AvatarMaterialOptions[material_keys[i]][AvatarChoices.instance.Chosen_AvatarMaterialOptions[material_keys[i]]];
            Renderer.sharedMaterials = materials;
        }

        if(target == "Skin"){
            Material[] materials = new Material[1];
            materials[0] = AvatarChoices.instance.AvatarMaterialOptions["Skin"][AvatarChoices.instance.Chosen_AvatarMaterialOptions["Skin"]];
            AvatarHandRight.GetComponent<SkinnedMeshRenderer>().sharedMaterials = materials;
            AvatarHandLeft.GetComponent<SkinnedMeshRenderer>().sharedMaterials = materials;
        }
    }

    //~~

    public void RemoveClothing(string target){
        AvatarChoices.instance.Chosen_ClothingOptions[target] = -1;
        for(int i=0; i<ClothingComponents[target].Count; i++)
            Destroy(ClothingComponents[target][i]);
        ClothingComponents[target].Clear();
    }

    public void ChangeClothing(string target, int option){
        RemoveClothing(target);
        AvatarChoices.instance.Chosen_ClothingOptions[target] = option;

        for(int i=0; i<AvatarChoices.instance.ClothingOptions[target][option].Clothings.Length; i++){
            ClothingComponents[target].Add(Instantiate(AvatarChoices.instance.ClothingOptions[target][option].Clothings[i].Object));
            ClothingComponents[target][i].transform.SetParent(AvatarBones[AvatarChoices.instance.ClothingOptions[target][option].Clothings[i].RootBone]);
            ClothingComponents[target][i].transform.localPosition = AvatarChoices.instance.ClothingOptions[target][option].Clothings[i].OffsetPosition;
            ClothingComponents[target][i].transform.localRotation = Quaternion.Euler(AvatarChoices.instance.ClothingOptions[target][option].Clothings[i].OffsetRotation);
            ClothingComponents[target][i].transform.localScale = AvatarChoices.instance.ClothingOptions[target][option].Clothings[i].OffsetScale;

            ClothingComponents[target][i].GetComponent<MeshRenderer>().sharedMaterial = AvatarChoices.instance.AvatarMaterialOptions[AvatarChoices.instance.ClothingOptions[target][option].Clothings[i].Material][AvatarChoices.instance.Chosen_AvatarMaterialOptions[target]];
        }
    }

    public void ChangeClothingMaterial(string target, int option){
        AvatarChoices.instance.Chosen_AvatarMaterialOptions[target] = option;
        if(AvatarChoices.instance.Chosen_ClothingOptions[target] < 0) return;

        for(int i=0; i<AvatarChoices.instance.ClothingOptions[target][AvatarChoices.instance.Chosen_ClothingOptions[target]].Clothings.Length; i++)
            ClothingComponents[target][i].GetComponent<MeshRenderer>().sharedMaterial = AvatarChoices.instance.AvatarMaterialOptions[AvatarChoices.instance.ClothingOptions[target][AvatarChoices.instance.Chosen_ClothingOptions[target]].Clothings[i].Material][option];
    }
}
