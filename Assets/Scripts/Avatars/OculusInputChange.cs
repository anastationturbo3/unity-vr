using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

public class OculusInputChange : MonoBehaviour {
    public XRController XRController;
    public GameObject RayOrigin;
    public string Hand;

    /*
    void Start()
    {
        StartCoroutine(CheckXRDevice());
    }

    public IEnumerator CheckXRDevice()
    {
        bool found = false;
        bool oculusification = false;
        List<InputDevice> devices = new List<InputDevice>();

        while(!found)
        {
            InputDevices.GetDevices(devices);
            //InputDevices.GetDevicesAtXRNode(XRNode.Head, devices);
            if(devices.Count > 0) found = true;

            yield return new WaitForEndOfFrame();
        }

        foreach(InputDevice device in devices){
            Debug.Log(device.name);
            if(device.name.Contains("Oculus"))
                oculusification = true;
        }

        if(oculusification){
            Debug.Log("Oculus Detected!");
            XRController.uiPressUsage = InputHelpers.Button.PrimaryButton;
            RayOrigin.transform.Rotate(0f, 30f, 0f);
        }
    }
    */

    void OnEnable(){
        InputDevices.deviceConnected += DeviceConnected;
        List<InputDevice> devices = new List<InputDevice>();
        InputDevices.GetDevices(devices);
        foreach(var device in devices)
            DeviceConnected(device);
    }

    void OnDisable(){
        InputDevices.deviceConnected -= DeviceConnected;
    }

    void DeviceConnected(InputDevice device){
        // The Left Hand
        if (Hand == "Left" && ((device.characteristics & InputDeviceCharacteristics.Left) != 0)){
            Debug.Log(("Left Hand Device", device.name));
            if(device.name.Contains("Oculus")) Oculusification ();
        }
        // The Right hand
        else if (Hand == "Right" && ((device.characteristics & InputDeviceCharacteristics.Right) != 0)){
            Debug.Log(("Right Hand Device", device.name));
            if(device.name.Contains("Oculus")) Oculusification ();
        }
    }

    void Oculusification(){
        XRController.uiPressUsage = InputHelpers.Button.PrimaryButton;
        GetComponent<PortableCharacterPanelSummoner>().UpdateHandInput();
        RayOrigin.transform.localPosition = new Vector3(0f, -0.02f, 0.1f);
        RayOrigin.transform.localRotation = Quaternion.identity;
        RayOrigin.transform.Rotate(45f, 0f, 0f);
        //AttachPoint.transform.localRotation = Quaternion.identity;
        //AttachPoint.transform.Rotate(45f, 0f, 0f);
    }
}
