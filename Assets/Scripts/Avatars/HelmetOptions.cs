using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelmetOptions : MonoBehaviour {
    private bool helmet_is_on;
    public bool helmet_on {
        get {
            return helmet_is_on;
        }
        set {
            if(value == helmet_is_on) return;
            helmet_is_on = value;
            if(helmet_is_on) Change(0);
            else Change(current_option);
        } 
    }

    public int current_option;
    private AvatarOptions AvatarOptions;

    void Awake(){
        AvatarOptions = GetComponent<AvatarOptions>();
        helmet_is_on = false;
    }

    private void Change(int option){
        AvatarOptions.ChangeComponentHelm(option);
    }
}
