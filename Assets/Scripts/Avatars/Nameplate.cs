using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nameplate : MonoBehaviour {
    public TMPro.TextMeshProUGUI NameText;
    public GameObject NameHolder;
    public GameObject NameplateAnchor;
    public GameObject TargetCamera;

    void Awake(){
        NameplateHolder.instance.AddToList(NameHolder);
        TargetCamera = XRholder.instance.vrHead;
    }

    void LateUpdate(){
        transform.position = NameplateAnchor.transform.position;
        transform.LookAt(TargetCamera.transform, Vector3.up);
    }

    void OnDestroy(){
        NameplateHolder.instance.RemoveFromList(NameHolder);
    }
}
