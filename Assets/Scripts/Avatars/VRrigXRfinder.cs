using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRrigXRfinder : MonoBehaviour {
    public bool NetworkMode;
    void Start(){
        StartCoroutine(SetupRig());
    }

    IEnumerator SetupRig(){
        yield return new WaitUntil(() => XRholder.instance != null);

        if(NetworkMode){
            NetworkVRrig rig = GetComponent<NetworkVRrig>();
            rig.head.vrTarget = XRholder.instance.vrHead.transform;
            rig.leftHand.vrTarget = XRholder.instance.vrLeftArm.transform;
            rig.rightHand.vrTarget = XRholder.instance.vrRightArm.transform;
        }
        else{
            VRrig rig = GetComponent<VRrig>();
            rig.head.vrTarget = XRholder.instance.vrHead.transform;
            rig.leftHand.vrTarget = XRholder.instance.vrLeftArm.transform;
            rig.rightHand.vrTarget = XRholder.instance.vrRightArm.transform;
        }
    }
}
