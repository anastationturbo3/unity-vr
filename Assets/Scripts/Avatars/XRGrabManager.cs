using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class XRGrabManager : MonoBehaviour {
    public XRRayInteractor lastHand;

    public void StartingHeldItems(){
        if(TransitionManager.instance != null){
            if(!TransitionManager.instance.isGM){
                lastHand = XRholder.instance.vrLeftArm.GetComponent<XRRayInteractor>();
                if(TransitionManager.instance.LeftHandHold.Length > 0) TheCollection.instance.AbilityDescriptions.transform.Find(TransitionManager.instance.LeftHandHold).GetComponent<AbilityDescription>().Summon();
                lastHand = XRholder.instance.vrRightArm.GetComponent<XRRayInteractor>();
                if(TransitionManager.instance.RightHandHold.Length > 0) TheCollection.instance.AbilityDescriptions.transform.Find(TransitionManager.instance.RightHandHold).GetComponent<AbilityDescription>().Summon();
            }
        }
    }
}
