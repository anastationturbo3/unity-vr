using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class StickyHandsController : MonoBehaviour {
    public static StickyHandsController instance = null;
    private bool StickyhandsActive;

    public bool SetStickyhands {
        get {
            return StickyhandsActive;
        }
        set {
            if(value == StickyhandsActive) return;
            StickyhandsActive = value;
            StartCoroutine(StickySetWait());
        }    
    }

    IEnumerator StickySetWait(){
        yield return new WaitUntil(() => XRholder.instance != null);
        
        if(StickyhandsActive){
            XRholder.instance.vrLeftArm.GetComponent<XRRayInteractor>().selectActionTrigger = XRBaseControllerInteractor.InputTriggerType.Sticky;
            XRholder.instance.vrRightArm.GetComponent<XRRayInteractor>().selectActionTrigger = XRBaseControllerInteractor.InputTriggerType.Sticky;
        } 
        else {
            XRholder.instance.vrLeftArm.GetComponent<XRRayInteractor>().selectActionTrigger = XRBaseControllerInteractor.InputTriggerType.State;
            XRholder.instance.vrRightArm.GetComponent<XRRayInteractor>().selectActionTrigger = XRBaseControllerInteractor.InputTriggerType.State;
        }
    }

    void Awake(){
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        SetStickyhands = (PlayerPrefs.GetInt("StickyHandsOption") != 0);
    }

    public void EnableStickyhands(){
        SetStickyhands = true;
    }

    public void DisableStickyhands(){
        SetStickyhands = false;
    }
}
