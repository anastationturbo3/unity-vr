using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MeshOption{
    public Mesh Mesh;
    public string[] Materials;
}

[System.Serializable]
public class Clothing{
    public GameObject Object;
    public Vector3 OffsetPosition;
    public Vector3 OffsetRotation;
    public Vector3 OffsetScale;
    public string RootBone;
    public string Material;
}

[System.Serializable]
public class ClothingSetOption{
    public Clothing[] Clothings;
}

public class AvatarChoices : MonoBehaviour {
    public static AvatarChoices instance = null;

    [Header("Avatar Components")]
    public List<MeshOption> AvatarComponentOptions_Heads;
    public List<MeshOption> AvatarComponentOptions_Torsos;
    public List<MeshOption> AvatarComponentOptions_Pants;
    [HideInInspector] public Dictionary<string,int> Chosen_AvatarComponentOptions;
    [HideInInspector] public Dictionary<string,List<MeshOption>> AvatarComponentOptions;

    [Header("Clothing")]
    public List<ClothingSetOption> ClothingOptions_Hat;
    public List<ClothingSetOption> ClothingOptions_Armor;
    public List<ClothingSetOption> ClothingOptions_Waist;
    [HideInInspector] public Dictionary<string,int> Chosen_ClothingOptions;
    [HideInInspector] public Dictionary<string,List<ClothingSetOption>> ClothingOptions;

    [Header("Materials")]
    public List<Material> AvatarMaterialOptions_Skin;
    public List<Material> AvatarMaterialOptions_Hair_1;
    public List<Material> AvatarMaterialOptions_Hair_2;
    public List<Material> AvatarMaterialOptions_Shirt;
    public List<Material> AvatarMaterialOptions_Pants;

    [Header("Clothing Materials")]
    public List<Material> AvatarMaterialOptions_Adventure;
    public List<Material> AvatarMaterialOptions_Dungeon;
    public List<Material> AvatarMaterialOptions_Bone;
    public List<Material> AvatarMaterialOptions_Explorer;
    public List<Material> AvatarMaterialOptions_Samurai;
    public List<Material> AvatarMaterialOptions_Viking;
    public List<Material> AvatarMaterialOptions_Pirate;
    [HideInInspector] public Dictionary<string,int> Chosen_AvatarMaterialOptions;
    [HideInInspector] public Dictionary<string,List<Material>> AvatarMaterialOptions;

    void Awake(){
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);

        Chosen_AvatarComponentOptions = new Dictionary<string, int>();
        Chosen_AvatarComponentOptions.Add("Head", 6);
        Chosen_AvatarComponentOptions.Add("Torso", 3);
        Chosen_AvatarComponentOptions.Add("Pants", 2);

        AvatarComponentOptions = new Dictionary<string, List<MeshOption>>();
        AvatarComponentOptions.Add("Head", AvatarComponentOptions_Heads);
        AvatarComponentOptions.Add("Torso", AvatarComponentOptions_Torsos);
        AvatarComponentOptions.Add("Pants", AvatarComponentOptions_Pants);

        Chosen_ClothingOptions = new Dictionary<string, int>();
        Chosen_ClothingOptions.Add("Hat", -1);
        Chosen_ClothingOptions.Add("Armor", -1);
        Chosen_ClothingOptions.Add("Waist", -1);

        ClothingOptions = new Dictionary<string, List<ClothingSetOption>>();
        ClothingOptions.Add("Hat", ClothingOptions_Hat);
        ClothingOptions.Add("Armor", ClothingOptions_Armor);
        ClothingOptions.Add("Waist", ClothingOptions_Waist);

        Chosen_AvatarMaterialOptions = new Dictionary<string, int>();
        Chosen_AvatarMaterialOptions.Add("Skin", 0);
        Chosen_AvatarMaterialOptions.Add("Hair 1", 0);
        Chosen_AvatarMaterialOptions.Add("Hair 2", 0);
        Chosen_AvatarMaterialOptions.Add("Shirt", 0);
        Chosen_AvatarMaterialOptions.Add("Pants", 0);

        Chosen_AvatarMaterialOptions.Add("Hat", 0);
        Chosen_AvatarMaterialOptions.Add("Armor", 0);
        Chosen_AvatarMaterialOptions.Add("Waist", 0);

        AvatarMaterialOptions = new Dictionary<string, List<Material>>();
        AvatarMaterialOptions.Add("Skin", AvatarMaterialOptions_Skin);
        AvatarMaterialOptions.Add("Hair 1", AvatarMaterialOptions_Hair_1);
        AvatarMaterialOptions.Add("Hair 2", AvatarMaterialOptions_Hair_2);
        AvatarMaterialOptions.Add("Shirt", AvatarMaterialOptions_Shirt);
        AvatarMaterialOptions.Add("Pants", AvatarMaterialOptions_Pants);

        AvatarMaterialOptions.Add("Adventure", AvatarMaterialOptions_Adventure);
        AvatarMaterialOptions.Add("Dungeon", AvatarMaterialOptions_Dungeon);
        AvatarMaterialOptions.Add("Bone", AvatarMaterialOptions_Bone);
        AvatarMaterialOptions.Add("Explorer", AvatarMaterialOptions_Explorer);
        AvatarMaterialOptions.Add("Samurai", AvatarMaterialOptions_Samurai);
        AvatarMaterialOptions.Add("Viking", AvatarMaterialOptions_Viking);
        AvatarMaterialOptions.Add("Pirate", AvatarMaterialOptions_Pirate);
    }
}
