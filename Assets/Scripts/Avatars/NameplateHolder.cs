using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NameplateHolder : MonoBehaviour {
    public static NameplateHolder instance = null;
    public List<GameObject> Nameplates;
    private bool NameplatesActive;

    public bool SetNameplates {
        get {
            return NameplatesActive;
        }
        set {
            if(value == NameplatesActive) return;
            NameplatesActive = value;
            foreach(GameObject Nameplate in Nameplates) Nameplate.SetActive(NameplatesActive);
        }    
    }

    private float NameplateScale;
    public float SetNameplateScale {
        get {
            return NameplateScale;
        }
        set {
            if(value == NameplateScale) return;
            NameplateScale = value;
            foreach(GameObject Nameplate in Nameplates) Nameplate.transform.localScale = Vector3.one * NameplateScale;
        }    
    }

    private float NameplateAlpha;
    public float SetNameplateAlpha {
        get {
            return NameplateAlpha;
        }
        set {
            if(value == NameplateAlpha) return;
            NameplateAlpha = value;
            foreach(GameObject Nameplate in Nameplates){
                Color c = Nameplate.GetComponent<Nameplate>().NameText.transform.parent.GetComponent<Image>().color;
                c.a = NameplateAlpha;
                Nameplate.GetComponent<Nameplate>().NameText.transform.parent.GetComponent<Image>().color = c;
            }
        }    
    }

    void Awake(){
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        Nameplates = new List<GameObject>();
        SetNameplates = (PlayerPrefs.GetInt("NameplateOption") != 0);

        NameplatesActive = SetNameplates;
        foreach(GameObject Nameplate in Nameplates) Nameplate.SetActive(NameplatesActive);

        NameplateScale = PlayerPrefs.GetFloat("NameplateScaleOption");
        foreach(GameObject Nameplate in Nameplates) Nameplate.transform.localScale = Vector3.one * NameplateScale;

        NameplateAlpha = PlayerPrefs.GetFloat("NameplateAlphaOption");
        foreach(GameObject Nameplate in Nameplates){
            Color c = Nameplate.GetComponent<Nameplate>().NameText.transform.parent.GetComponent<Image>().color;
            c.a = NameplateAlpha;
            Nameplate.GetComponent<Nameplate>().NameText.transform.parent.GetComponent<Image>().color = c;
        }
    }

    public void AddToList(GameObject Nameplate){
        if(!Nameplates.Contains(Nameplate)) Nameplates.Add(Nameplate);
    }

    public void RemoveFromList(GameObject Nameplate){
        if(Nameplates.Contains(Nameplate)) Nameplates.Remove(Nameplate);
    }

    public void EnableNameplates(){
        SetNameplates = true;
    }

    public void DisableNameplates(){
        SetNameplates = false;
    }
}
