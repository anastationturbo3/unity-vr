using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
using UnityEngine.XR.Interaction.Toolkit;
using Photon.Pun;

public class AnimatorHandLeft : MonoBehaviour {
    private Animator handAnimator;
    private float m_ButtonPressPoint = 0.5f;
    private XRholder vr;
    private XRRayInteractor Holder;

    private InputActionProperty handInput_action;
    private InputHelpers.Button handInput_device;
    private bool action_mode;

    public PhotonView photonView;

    void Awake() {
        handAnimator = GetComponent<Animator>();
        StartCoroutine(SetHand());
    }

    IEnumerator SetHand(){
        yield return new WaitUntil(() => XRholder.instance != null);
        vr = XRholder.instance;
        Holder = vr.vrLeftArm.GetComponent<XRRayInteractor>();

        if(vr.vrLeftArm.GetComponent<ActionBasedController>() != null){
            handInput_action = vr.vrLeftArm.GetComponent<ActionBasedController>().selectAction;
            action_mode = true;
        }
        else if(vr.vrLeftArm.GetComponent<XRController>() != null){
            handInput_device = vr.vrLeftArm.GetComponent<XRController>().selectUsage;
            action_mode = false;
        }
    }

    void Update() {
        if(Holder != null){
            if(action_mode){
                // Wheel Activation Action
                if((IsPressed(handInput_action.action) || Holder.selectTarget != null) && ((photonView == null) || (photonView != null && photonView.IsMine))){
                    handAnimator.SetBool("ToFist", true);
                    handAnimator.SetBool("ToIdle", false);
                }
                else{
                    handAnimator.SetBool("ToFist", false);
                    handAnimator.SetBool("ToIdle", true);
                }
            }
            else{
                // Wheel Activation Device
                if((IsPressedDevice(handInput_device) || Holder.selectTarget != null) && ((photonView == null) || (photonView != null && photonView.IsMine))){
                    handAnimator.SetBool("ToFist", true);
                    handAnimator.SetBool("ToIdle", false);
                }
                else{
                    handAnimator.SetBool("ToFist", false);
                    handAnimator.SetBool("ToIdle", true);
                }
            }
        }
    }

    private bool IsPressed(InputAction action){
        if (action == null)
            return false;

#if INPUT_SYSTEM_1_1_OR_NEWER || INPUT_SYSTEM_1_1_PREVIEW // 1.1.0-preview.2 or newer, including pre-release
            return action.phase == InputActionPhase.Performed;
#else
        if (action.activeControl is ButtonControl buttonControl)
            return buttonControl.isPressed;

        if (action.activeControl is AxisControl)
            return action.ReadValue<float>() >= m_ButtonPressPoint;

        return action.triggered || action.phase == InputActionPhase.Performed;
#endif
    }

    // device stuff

    private bool IsPressedDevice(InputHelpers.Button button){
        vr.vrLeftArm.GetComponent<XRController>().inputDevice.IsPressed(button, out var pressed, vr.vrLeftArm.GetComponent<XRController>().axisToPressThreshold);
        return pressed;
    }
}

