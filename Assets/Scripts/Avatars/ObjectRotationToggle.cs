using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class ObjectRotationToggle : MonoBehaviour {
    public static ObjectRotationToggle instance = null;
    private bool ObjectRotationDisabled;

    public bool SetObjectRotationDisabled {
        get {
            return ObjectRotationDisabled;
        }
        set {
            if(value == ObjectRotationDisabled) return;
            ObjectRotationDisabled = value;
            StartCoroutine(RotationSetWait());
        }    
    }

    IEnumerator RotationSetWait(){
        yield return new WaitUntil(() => XRholder.instance != null);
        
        if(ObjectRotationDisabled){
            XRholder.instance.vrLeftArm.GetComponent<XRController>().rotateObjectLeft = InputHelpers.Button.None;
            XRholder.instance.vrLeftArm.GetComponent<XRController>().rotateObjectRight = InputHelpers.Button.None;
            XRholder.instance.vrLeftArm.GetComponent<XRController>().moveObjectIn = InputHelpers.Button.None;
            XRholder.instance.vrLeftArm.GetComponent<XRController>().moveObjectOut = InputHelpers.Button.None;

            XRholder.instance.vrRightArm.GetComponent<XRController>().rotateObjectLeft = InputHelpers.Button.None;
            XRholder.instance.vrRightArm.GetComponent<XRController>().rotateObjectRight = InputHelpers.Button.None;
            XRholder.instance.vrRightArm.GetComponent<XRController>().moveObjectIn = InputHelpers.Button.None;
            XRholder.instance.vrRightArm.GetComponent<XRController>().moveObjectOut = InputHelpers.Button.None;
        } 
        else {
            XRholder.instance.vrLeftArm.GetComponent<XRController>().rotateObjectLeft = InputHelpers.Button.PrimaryAxis2DLeft;
            XRholder.instance.vrLeftArm.GetComponent<XRController>().rotateObjectRight = InputHelpers.Button.PrimaryAxis2DRight;
            XRholder.instance.vrLeftArm.GetComponent<XRController>().moveObjectIn = InputHelpers.Button.PrimaryAxis2DUp;
            XRholder.instance.vrLeftArm.GetComponent<XRController>().moveObjectOut = InputHelpers.Button.PrimaryAxis2DDown;

            XRholder.instance.vrRightArm.GetComponent<XRController>().rotateObjectLeft = InputHelpers.Button.PrimaryAxis2DLeft;
            XRholder.instance.vrRightArm.GetComponent<XRController>().rotateObjectRight = InputHelpers.Button.PrimaryAxis2DRight;
            XRholder.instance.vrRightArm.GetComponent<XRController>().moveObjectIn = InputHelpers.Button.PrimaryAxis2DUp;
            XRholder.instance.vrRightArm.GetComponent<XRController>().moveObjectOut = InputHelpers.Button.PrimaryAxis2DDown;
        }
    }

    void Awake(){
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        ObjectRotationDisabled = (PlayerPrefs.GetInt("DisableObjectRotation") != 0);
    }

    public void EnableObjectRotation(){
        ObjectRotationDisabled = true;
    }

    public void DisableObjectRotation(){
        ObjectRotationDisabled = false;
    }
}
