using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Artifact_AC : Artifact {
    public int val;

    override public void Gain(){
        CharacterOptions.instance.ac_bonus += val;
        CharacterOptions.instance.UpdateSheet();
    }

    override public void Remove(){
        CharacterOptions.instance.ac_bonus -= val;
        CharacterOptions.instance.UpdateSheet();
    }
}
