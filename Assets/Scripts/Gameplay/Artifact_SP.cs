using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Artifact_SP : Artifact {
    public int val;

    override public void Gain(){
        if(CharacterOptions.instance.sp_max > 0){
            CharacterOptions.instance.sp_max += val;
            CharacterOptions.instance.UpdateSheet();
        }
    }

    override public void Remove(){
        if(CharacterOptions.instance.sp_max > 0){
            CharacterOptions.instance.sp_max -= val;
            if(CharacterOptions.instance.sp_current > CharacterOptions.instance.sp_max) CharacterOptions.instance.sp_current = CharacterOptions.instance.sp_max;
            CharacterOptions.instance.UpdateSheet();
        }
    }
}
