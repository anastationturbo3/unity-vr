using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Artifact_Ability : Artifact {
    public string ability;

    override public void Gain(){
        CharacterOptions.instance.abilities.Add(ability);
        CharacterOptions.instance.UpdateSheet();
    }

    override public void Remove(){
        CharacterOptions.instance.abilities.Remove(ability);
        CharacterOptions.instance.UpdateSheet();
    }
}
