using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Artifact_HP : Artifact {
    public int val;

    override public void Gain(){
        CharacterOptions.instance.hp_max += val;
        CharacterOptions.instance.UpdateSheet();
    }

    override public void Remove(){
        CharacterOptions.instance.hp_max -= val;
        if(CharacterOptions.instance.hp_current > CharacterOptions.instance.hp_max) CharacterOptions.instance.hp_current = CharacterOptions.instance.hp_max;
        CharacterOptions.instance.UpdateSheet();
    }
}
