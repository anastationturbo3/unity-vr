using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridMaker : MonoBehaviour {
    public GameObject GridCell;
    public List<List<GameObject>> Grid;
    public int rows;
    public int cols;

    void Awake(){
        Grid = new List<List<GameObject>>();
        for(int i=0; i<rows; i++){
            Grid.Add(new List<GameObject>());
            for(int j=0; j<cols; j++){
                Grid[i].Add(Instantiate(GridCell, new Vector3(transform.position.x + (i - (rows/2f) + 0.5f) * 0.15f, transform.position.y, transform.position.z + (j - (cols/2f) + 0.5f ) * 0.15f), Quaternion.identity));
                Grid[i][j].transform.SetParent(transform);
            }
        }
    }
}
