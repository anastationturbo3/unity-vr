using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class TeleporterChairManager : MonoBehaviour {
    private PhotonView photonView;

    public TeleporterChairManager instance;
    public List<GameObject> ChairTeleporters;
    public GameObject CurrentChair;

    void Awake(){
        photonView = GetComponent<PhotonView>();
    }

    public void VerifyTakenChairs(){
        photonView.RPC("VerifyTakenChairsRPC", RpcTarget.All);
    }

    public void LeaveChair(){
        if(CurrentChair != null){
            photonView.RPC("SetChairMeshRPC", RpcTarget.All, CurrentChair.name, true);
            CurrentChair = null;
        }
    }

    public void ChangeChair(GameObject NewChair){
        if(CurrentChair != null){
            photonView.RPC("SetChairMeshRPC", RpcTarget.All, CurrentChair.name, true);
            CurrentChair = null;
        }

        photonView.RPC("SetChairMeshRPC", RpcTarget.All, NewChair.name, false);
        CurrentChair = NewChair;
    }

    [PunRPC]
    public void SetChairMeshRPC(string ChairName, bool ChairEnabled){
        foreach(GameObject Chair in ChairTeleporters)
            if(Chair.name.Equals(ChairName))
                Chair.GetComponent<MeshCollider>().enabled = ChairEnabled;
    }

    [PunRPC]
    public void ResetChairsRPC(){
        foreach(GameObject Chair in ChairTeleporters)
            Chair.GetComponent<MeshCollider>().enabled = true;
    }

    [PunRPC]
    public void VerifyTakenChairsRPC(){
        if(CurrentChair != null)
            photonView.RPC("SetChairMeshRPC", RpcTarget.All, CurrentChair.name, false);
    }
}
