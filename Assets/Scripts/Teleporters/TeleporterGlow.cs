using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class TeleporterGlow : MonoBehaviour {

    public float m_MaxAlphaIntensity = 2f;
    public float m_MinAlphaIntensity = 0f;
    public float m_FadeSpeed = 2.2f;
    public MeshRenderer[] combined_Meshes;

    private float m_CurrentTime = 0f;
    private bool m_Highlighted = false;
    //private MeshRenderer m_MeshRenderer;
    private MaterialPropertyBlock m_Block;
    private int m_AlphaIntensityID;

    void Start(){
        //m_MeshRenderer = GetComponent<MeshRenderer>();
        //m_MeshRenderer = GetComponent<MeshRenderer>().mesh.CombineMeshes(combined_Meshes);

        m_AlphaIntensityID = Shader.PropertyToID("AlphaIntensity");
        m_Block = new MaterialPropertyBlock();
        m_Block.SetFloat(m_AlphaIntensityID, m_CurrentTime);
        m_CurrentTime = 0;
        foreach(MeshRenderer m_MeshRenderer in combined_Meshes) m_MeshRenderer.SetPropertyBlock(m_Block);
    }

    void FixedUpdate(){
        if (m_Highlighted) m_CurrentTime += Time.deltaTime * m_FadeSpeed;
        else m_CurrentTime -= Time.deltaTime * m_FadeSpeed;
        
        if (m_CurrentTime > m_MaxAlphaIntensity) m_CurrentTime = m_MaxAlphaIntensity;
        else if (m_CurrentTime < m_MinAlphaIntensity) m_CurrentTime = m_MinAlphaIntensity;

        foreach(MeshRenderer m_MeshRenderer in combined_Meshes){
            m_MeshRenderer.GetPropertyBlock(m_Block);
            m_Block.SetFloat(m_AlphaIntensityID, m_CurrentTime);
            m_MeshRenderer.SetPropertyBlock(m_Block);
        }
    }

    public void StartHighlight(){
        m_Highlighted = true;
    }

    public void StopHighlight(){
        m_Highlighted = false;
    }
}
