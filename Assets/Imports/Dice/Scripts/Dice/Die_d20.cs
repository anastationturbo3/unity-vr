using UnityEngine;
using System.Collections;

// Die subclass to expose the D20 side hitVectors
public class Die_d20 : Die {
		
    override protected Vector3 HitVector(int side)
    {
        switch (side)
        {
            case 1: return new Vector3(-0.8F, 0.2F, -0.6F);
            case 2: return new Vector3(0.8F, -0.2F, 0.6F);
            case 3: return new Vector3(-0.3F, -0.2F, 0.9F);
            case 4: return new Vector3(-1F, 0.2F, 0F);
            case 5: return new Vector3(1F, 0.2F, 0F);
            case 6: return new Vector3(0.8F, -0.2F, -0.6F);
            case 7: return new Vector3(0.3F, 0.2F, 0.9F);
            case 8: return new Vector3(0.3F, 0.2F, -0.9F);
            case 9: return new Vector3(-0.3F, -0.2F, -0.9F);
            case 10: return new Vector3(0.2F, 0.8F, -0.6F);
            case 11: return new Vector3(-0.8F, 0.2F, 0.6F);
            case 12: return new Vector3(-0.5F, 0.8F, 0.3F);
            case 13: return new Vector3(0.5F, -0.8F, 0.4F);
            case 14: return new Vector3(-0.2F, -0.8F, 0.6F);
            case 15: return new Vector3(0.5F, -0.8F, -0.4F);
            case 16: return new Vector3(-0.5F, 0.8F, -0.4F);
            case 17: return new Vector3(0.6F, 0.8F, 0F);
            case 18: return new Vector3(0.2F, 0.8F, 0.6F);
            case 19: return new Vector3(-0.2F, -0.8F, -0.6F);
            case 20: return new Vector3(-0.6F, -0.8F, 0F);
        }
        return Vector3.zero;
    }
		
}
