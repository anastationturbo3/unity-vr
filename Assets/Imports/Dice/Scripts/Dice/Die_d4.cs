using UnityEngine;
using System.Collections;

// Die subclass to expose the D4 side hitVectors
public class Die_d4 : Die {
		
    override protected Vector3 HitVector(int side)
    {
        switch (side)
        {
            case 1: return new Vector3(0.8F, -0.4F, 0.5F);
            case 2: return new Vector3(-0.8F, -0.4F, 0.5F);
            case 3: return new Vector3(0F, -0.4F, -0.9F);
            case 4: return new Vector3(0F, 1F, 0F);
        }
        return Vector3.zero;
    }
		
}
