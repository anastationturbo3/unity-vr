using UnityEngine;
using System.Collections;

// Die subclass to expose the D12 side hitVectors
public class Die_d12 : Die {
		
    override protected Vector3 HitVector(int side)
    {
        switch (side)
        {
            case 1: return new Vector3(-0.8F, 0.5F, 0.3F);
            case 2: return new Vector3(-0.9F, -0.4F, -0.3F);
            case 3: return new Vector3(-0.5F, 0.5F, -0.7F);
            case 4: return new Vector3(0.8F, 0.4F, 0.3F);
            case 5: return new Vector3(0.5F, 0.5F, -0.7F);
            case 6: return new Vector3(0F, 0.5F, 0.9F);
            case 7: return new Vector3(0F, -0.5F, -0.9F);
            case 8: return new Vector3(0F, 1F, 0F);
            case 9: return new Vector3(0F, -1F, 0F);
            case 10: return new Vector3(-0.5F, -0.4F, 0.7F);
            case 11: return new Vector3(0.5F, -0.5F, 0.7F);
            case 12: return new Vector3(0.9F, -0.4F, -0.3F);
        }
        return Vector3.zero;
    }
		
}
