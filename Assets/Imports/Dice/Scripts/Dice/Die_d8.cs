using UnityEngine;
using System.Collections;

// Die subclass to expose the D8 side hitVectors
public class Die_d8 : Die {
		
    override protected Vector3 HitVector(int side)
    {
        switch (side)
        {
            case 1: return new Vector3(-0.6F, 0.5F, -0.6F);
            case 2: return new Vector3(-0.6F, -0.5F, 0.6F);
            case 3: return new Vector3(-0.6F, 0.5F, 0.6F);
            case 4: return new Vector3(-0.6F, -0.5F, -0.6F);
            case 5: return new Vector3(0.6F, 0.5F, 0.6F);
            case 6: return new Vector3(0.6F, -0.5F, -0.6F);
            case 7: return new Vector3(0.6F, 0.5F, -0.6F);
            case 8: return new Vector3(0.6F, -0.5F, 0.6F);
        }
        return Vector3.zero;
    }
		
}
